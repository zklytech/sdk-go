package tool

import (
	"fmt"
	"regexp"
	"strings"
)

// ReplaceVariables 接受模板字符串和可变参数，按照顺序替换 ${} 中的变量
func ReplaceVariables(template string, values ...string) (string, error) {
	// 匹配 ${...} 的正则表达式
	re := regexp.MustCompile(`\$\{([^}]+)}`)

	// 找到所有匹配的变量
	matches := re.FindAllStringSubmatch(template, -1)
	// 检查传入的参数数量是否与变量数量相同
	if len(values) != len(matches) {
		return template, nil
		//return "", fmt.Errorf("参数数量不匹配，模板中 %d 个变量，传入了 %d 个参数", len(matches), len(values))
	}

	// 创建替换数组
	replacements := make([]string, 0, len(matches)*2)

	// 将变量与传入的参数值对应
	for i, match := range matches {
		varName := match[1]
		replacements = append(replacements, "${"+varName+"}", values[i])
	}

	// 使用 strings.Replacer 替换所有变量
	replacer := strings.NewReplacer(replacements...)
	return replacer.Replace(template), nil
}

// ReplaceVariablesMap 接受一个模板字符串和变量映射，将模板中的变量替换成相应的值
func ReplaceVariablesMap(template string, vars map[string]string) (string, error) {
	// 匹配 ${...} 的正则表达式
	re := regexp.MustCompile(`\$\{([^}]+)}`)

	// 使用正则表达式查找所有匹配的变量
	matches := re.FindAllStringSubmatch(template, -1)

	// 检查所有匹配的变量是否都有对应的值
	for _, match := range matches {
		varName := match[1]
		if _, ok := vars[varName]; !ok {
			return "", fmt.Errorf("变量 %s 未找到", varName)
		}
	}

	// 使用 strings.Replacer 替换所有变量
	replacements := make([]string, 0, len(matches)*2)
	for _, match := range matches {
		varName := match[1]
		replacements = append(replacements, "${"+varName+"}", vars[varName])
	}

	replacer := strings.NewReplacer(replacements...)
	return replacer.Replace(template), nil
}

// ReverseArray 数组反转
func ReverseArray[T any](arr []T) []T {
	left := 0
	right := len(arr) - 1

	for left < right {
		// 交换 arr[left] 和 arr[right]
		arr[left], arr[right] = arr[right], arr[left]
		left++
		right--
	}

	return arr
}
