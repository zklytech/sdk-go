package tool

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func ReturnJson(Context *gin.Context, httpCode int, dataCode int, msg interface{}, data interface{}) {
	Context.Header("Content-Type", "application/json; charset=utf-8")
	Context.JSON(httpCode, gin.H{
		"errcode": dataCode,
		"msg":     msg,
		"data":    data,
	})
}

func AuthFail(c *gin.Context, code int, msg string, data interface{}) {
	ReturnJson(c, http.StatusUnauthorized, code, msg, data)
}
