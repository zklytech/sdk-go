package page_service

type GetForm struct {
	AppId    string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId string `json:"sub_app_id" form:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId string `json:"module_id" form:"module_id"`
	PageId   string `json:"page_id" form:"page_id" validate:"required" message:"page_id required"`
}
type SetForm struct {
	AppId    string      `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId string      `json:"sub_app_id" form:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId string      `json:"module_id" form:"module_id"`
	Data     interface{} `json:"data" form:"data" validate:"required" message:"data required"`
	PageId   string      `json:"page_id" form:"page_id" validate:"required" message:"page_id required"`
}
type DeleteForm struct {
	AppId    string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId string `json:"sub_app_id" form:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId string `json:"module_id" form:"module_id"`
	PageId   string `json:"page_id" form:"page_id" validate:"required" message:"page_id required"`
}

type GetGroupForm struct {
	AppId    string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId string `json:"sub_app_id" form:"sub_app_id"`
	ModuleId string `json:"module_id" form:"module_id"`
}
type GroupData struct {
	GroupId string `json:"group_id" form:"group_id" validate:"required" message:"group_id required"`
}
