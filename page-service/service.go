package page_service

import (
	"github.com/gin-gonic/gin"
	"net/url"
)

func DeletePage(ctx *gin.Context, form DeleteForm) (err error) {
	_, err = BasePost[any](ctx, "/api/v1/page/delete", form, map[string]string{})
	return
}
func GetPage(ctx *gin.Context, form GetForm) (data any, err error) {
	data, err = BasePost[any](ctx, "/api/v1/page/get", form, map[string]string{})
	return
}
func SetPage(ctx *gin.Context, form SetForm) (err error) {

	_, err = BasePost[any](ctx, "/api/v1/page/set", form, map[string]string{})
	return
}

func GetGroupId(ctx *gin.Context, form GetGroupForm) (groupId string, err error) {
	params := url.Values{}
	params.Add("app_id", form.AppId)
	res, err := BaseGet[GroupData](ctx, "/api/service/getGroupId", params, map[string]string{})
	if err != nil {
		return "", err
	}
	groupId = res.GroupId
	return
}
