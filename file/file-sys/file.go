package file_sys

import (
	"github.com/gin-gonic/gin"
	"net/url"
)

func List(ctx *gin.Context, form FileListForm) (list ListData, err error) {
	list, err = BasePost[ListData](ctx, "/api/v1/file/list", form, map[string]string{})
	return
}

func Info(ctx *gin.Context, form FileInfo) (data FileData, err error) {
	params := url.Values{}
	params.Add("file_id", form.FileId)
	data, err = BaseGet[FileData](ctx, "/api/v1/file/info", params, map[string]string{})
	return
}
func MoveFile(ctx *gin.Context, form MoveFileForm) (err error) {
	_, err = BasePost[any](ctx, "/api/v1/file/moveFile", form, map[string]string{})
	return
}
func CopyFile(ctx *gin.Context, form CopyFileForm) (err error) {
	_, err = BasePost[any](ctx, "/api/v1/file/copyFile", form, map[string]string{})
	return
}
func DeleteFile(ctx *gin.Context, form DeleteForm) (err error) {
	_, err = BasePost[any](ctx, "/api/v1/file/deleteFile", form, map[string]string{})
	return
}

func UpdateFileName(ctx *gin.Context, form UpdateFileNameForm) (err error) {
	_, err = BasePost[any](ctx, "/api/v1/file/updateFileName", form, map[string]string{})
	return
}

func CreateDirectory(ctx *gin.Context, form CreateDirectoryForm) (err error) {
	_, err = BasePost[any](ctx, "/api/v1/file/createDirectory", form, map[string]string{})
	return
}

func InitRootDirectory(ctx *gin.Context, form InitRootDirectoryForm) (err error) {
	_, err = BasePost[any](ctx, "/api/v1/file/initRootDirectory", form, map[string]string{})
	return
}

func GetDirectoryIdByName(ctx *gin.Context, form GetDirectoryIdByNameForm) (data GetDirectoryIdByNameData, err error) {
	params := url.Values{}
	params.Add("domain_id", form.DomainId)
	params.Add("directory_id", form.DirectoryId)
	params.Add("name", form.Name)
	data, err = BaseGet[GetDirectoryIdByNameData](ctx, "/api/v1/file/getDirectoryIdByName", params, map[string]string{})
	return
}

func GetChildIds(ctx *gin.Context, form GetChildIdsForm) (ids ChildIds, err error) {
	params := url.Values{}
	params.Add("domain_id", form.DomainId)
	params.Add("file_id", form.FileId)
	ids, err = BaseGet[ChildIds](ctx, "/api/v1/file/getChildIds", params, map[string]string{})
	return
}

func GetDirectoryIdByPath(ctx *gin.Context, form GetDirectoryIdByPathForm) (id string, err error) {
	params := url.Values{}
	params.Add("domain_id", form.DomainId)
	params.Add("path", form.Path)
	id, err = BaseGet[string](ctx, "/api/v1/file/getDirectoryIdByPath", params, map[string]string{})
	return
}

func GetCredential(ctx *gin.Context, form GetCredentialForm) (data CredentialData, err error) {
	p := url.Values{}
	p.Add("domain_id", form.DomainId)
	p.Add("app_id", form.AppId)
	p.Add("root", form.Root)
	data, err = BaseGet[CredentialData](ctx, "/api/permission/getCredential", p, map[string]string{})
	return
}

// 续期token
func RenewCredential(ctx *gin.Context, form RenewCredentialForm) (err error) {
	_, err = BasePost[any](ctx, "/api/permission/renewCredential", form, map[string]string{})
	return
}

// 增减文件元数据引用数
func SetFileReferIncrease(ctx *gin.Context, form SetFileReferIncreaseForm) (err error) {
	_, err = BasePost[any](ctx, "/api/v1/file/setFileReferIncrease", form, map[string]string{})
	return
}

func Upload(ctx *gin.Context, form UploadForm) (data UploadResponse, err error) {
	p := map[string]string{
		"domain_id":    form.DomainId,
		"path":         form.Path,
		"directory_id": form.DirectoryId,
		"owner":        form.Owner,
		"token":        form.Token,
	}
	data, err = BasePostForm[UploadResponse](ctx, "/api/v1/file/upload", p, form.File, map[string]string{})
	return
}

func Init(ctx *gin.Context, form InitForm) (err error) {
	_, err = BasePost[any](ctx, "/api/service/init", form, map[string]string{})
	return
}
func Uninstall(ctx *gin.Context, form UninstallForm) (err error) {
	_, err = BasePost[any](ctx, "/api/service/uninstall", form, map[string]string{})
	return
}

func Download(ctx *gin.Context, form DownloadFile) (resp []byte, err error) {
	resp, err = GetFile(ctx, "/api/v1/file/download", map[string]string{
		"domain_id": form.DomainId,
		"file_id":   form.FileId,
		"token":     form.Token,
	}, map[string]string{})
	return
}

func CheckS3Config(ctx *gin.Context, form CheckS3ConfigForm) (err error) {
	_, err = BasePost[any](ctx, "/api/v1/file/checkS3Config", form, map[string]string{})
	return
}
