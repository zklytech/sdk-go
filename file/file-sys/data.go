package file_sys

import (
	"mime/multipart"
	"time"
)

type CreateDirectoryForm struct {
	DomainId    string `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	Name        string `json:"name" form:"name" validate:"required" message:"name required"`
	DirectoryId string `json:"directory_id" form:"directory_id"`
	Owner       string `json:"owner" form:"owner"`
}
type InitRootDirectoryForm struct {
	DomainId string `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	Name     string `json:"name" form:"name" validate:"required" message:"name required"`
	Owner    string `json:"owner" form:"owner"`
}

type FileListForm struct {
	DomainId    string `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	Name        string `json:"name" form:"name"`
	DirectoryId string `json:"directory_id" form:"directory_id"`
	Sort        string `json:"sort" form:"sort" validate:"in:desc,asc" message:"sort is asc or desc"`
	SortField   string `json:"sort_field" form:"sort_field" validate:"in:size,name,update_time" message:"sort_field is size update_time or name"`
	Owner       string `json:"owner" form:"owner"`
	IsDelete    bool   `json:"is_delete" form:"is_delete"`
	Page        int    `json:"page" form:"page"`
	PageSize    int    `json:"page_size" form:"page_size"`
	IsDirectory *bool  `json:"is_directory" form:"is_directory"`
}
type FileInfo struct {
	FileId string `json:"file_id" form:"file_id" validate:"required" message:"file_id required"`
}

type DeleteForm struct {
	DomainId string `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	FileId   string `json:"file_id" form:"file_id" validate:"required" message:"file_id required"`
	Owner    string `json:"owner" form:"owner"`
}
type DeleteMult struct {
	DomainId string `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	Owner    string `json:"owner" form:"owner"`
}

type MoveFileForm struct {
	DomainId          string `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	FileId            string `json:"file_id" form:"file_id" validate:"required" message:"file_id required"`
	TargetDirectoryId string `json:"target_directory_id" form:"target_directory_id" validate:"required" message:"target_directory_id required"`
}

type CopyFileForm struct {
	DomainId          string `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	FileId            string `json:"file_id" form:"file_id" validate:"required" message:"file_id required"`
	TargetDirectoryId string `json:"target_directory_id" form:"target_directory_id" validate:"required" message:"target_directory_id required"`
}

type UpdateFileNameForm struct {
	DomainId string `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	FileId   string `json:"file_id" form:"file_id" validate:"required" message:"file_id required"`
	Name     string `json:"name" form:"name" validate:"required" message:"name required"`
}

type GetDirectoryIdByNameForm struct {
	DomainId    string `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	DirectoryId string `json:"directory_id" form:"directory_id"`
	Name        string `json:"name" form:"name" validate:"required" message:"name required"`
}
type GetDirectoryIdByNameData struct {
	DirectoryId string `json:"directory_id" form:"directory_id"`
}

type ComputeDirectorySizeForm struct {
	DomainId    string `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	DirectoryId string `json:"directory_id" form:"directory_id"`
}

type GetChildIdsForm struct {
	DomainId string `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	FileId   string `json:"file_id" form:"file_id" validate:"required" message:"file_id required"`
}
type ChildIds []string

type FileData struct {
	CreateTime time.Time `json:"create_time"`
	UpdateTime time.Time ` json:"update_time"`

	FileId           string `json:"file_id"`
	Size             int64  `json:"size"`
	Mime             string `json:"mime"`
	Name             string `json:"name"`              // 文件名称
	Public           bool   `json:"public"`            // 是否公有
	DomainId         string `json:"domain_id"`         // 域id
	IsDelete         bool   `json:"is_delete"`         // 是否删除
	Owner            string `json:"owner"`             // 所有者
	Permission       string `json:"permission"`        // 权限
	DirectoryId      string `json:"directory_id"`      // 目录id
	IsDirectory      bool   `json:"is_directory"`      // 是否是文件夹
	FileMetaId       string `json:"file_meta_id"`      // 文件元数据id
	Ext              string `json:"ext"`               // 文件扩展名
	AccessPermission string `json:"access_permission"` // 访问权限
}

type ListData struct {
	List  []FileData `json:"list"`
	Total int        `json:"total"`
}
type GetDirectoryIdByPathForm struct {
	DomainId string `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	Path     string `json:"path" form:"path" validate:"required" message:"path required"`
}

type GetCredentialForm struct {
	AppId    string `json:"app_id"`
	DomainId string `json:"domain_id"`
	Root     string `json:"root"`
}

type RenewCredentialForm struct {
	RefreshToken string `json:"refresh_token"`
}

type CredentialData struct {
	AppId           string `json:"app_id"`
	DomainId        string `json:"domain_id"`
	Token           string `json:"token"`
	RootDirectoryId string `json:"root_directory_id"`
	Expire          int    `json:"expire"`
	RefreshToken    string `json:"refresh_token"`
}

type SetFileReferIncreaseForm struct {
	FileMetaId string `json:"file_meta_id" form:"file_meta_id" validate:"required" message:"file_meta_id required"`
	Number     int64  `json:"number" form:"number" validate:"required" message:"number required"`
}

type UploadForm struct {
	DomainId    string                `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	Path        string                `json:"path" form:"path" validate:"regexp:^([^/]+/)*[^/]+/?$" message:"path not continuous /"`
	File        *multipart.FileHeader `json:"file" form:"file" validate:"required" message:"file required"`
	DirectoryId string                `json:"directory_id" form:"directory_id" validate:"required" message:"directory_id required"`
	Owner       string                `json:"owner" form:"owner"`
	Token       string                `json:"token" form:"token"`
}

type UploadResponse struct {
	FileId      string `json:"file_id"`
	FileName    string `json:"file_name"`
	Size        int64  `json:"size"`
	ContentType string `json:"content_type"`
}

type InitForm struct {
	DomainId string `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	StoreId  string `json:"store_id" form:"store_id"`
	Bucket   string `json:"bucket" form:"bucket"`
}
type UninstallForm struct {
	AppId    string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	DomainId string `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	StoreId  string `json:"store_id" form:"store_id"`
}

type DownloadFile struct {
	DomainId string `json:"domain_id" form:"domain_id"`
	FileId   string `json:"file_id" form:"file_id" validate:"required" message:"file_id required"`
	Token    string `json:"token" form:"token"` // 认证token
}

type CheckS3ConfigForm struct {
	EndPoint        string `json:"end_point" form:"end_point" validate:"required" message:"endpoint required"`
	AccessKeyId     string `json:"access_key_id" form:"access_key_id" validate:"required" message:"access_key_id required"`
	SecretAccessKey string `json:"secret_access_key" form:"secret_access_key" validate:"required" message:"secret_access_key required"`
	Region          string `json:"region" form:"region" validate:"required" message:"region required"`
	SSL             bool   `json:"ssl" form:"ssl" validate:"required" message:"ssl required"`
	Bucket          string `json:"bucket" form:"bucket" validate:"required" message:"bucket required"`
	Version         string `json:"version" form:"version" validate:"required" message:"version required"`
	Platform        string `json:"platform" form:"platform" validate:"required" message:"platform required"`
}
