package file_service

import "time"

// 设置文件权限
type SetPermissionForm struct {
	AppId            string   `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId         string   `json:"sub_app_id" form:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId         string   `json:"module_id" form:"module_id" validate:"required" message:"module_id required"`
	FileIds          []string `form:"file_ids" json:"file_ids" validate:"required" message:"filed_ids required"`
	AccessPermission string   `form:"access_permission" json:"access_permission" validate:"access_permission required"`
}

type CheckPermissionMethod string

const (
	UploadPermission   CheckPermissionMethod = "upload"
	DownloadPermission CheckPermissionMethod = "download"
)

type CheckPermissionForm struct {
	DomainId string                `json:"domain_id"`
	FileId   string                `json:"file_id"`
	Token    string                `json:"token"`
	Method   CheckPermissionMethod `json:"method"`
}

type DirData struct {
	FileId      string    `json:"file_id"`
	FileName    string    `json:"file_name"`
	Alias       string    `json:"alias"`
	FileMetaId  string    `json:"file_meta_id"`
	IsDir       bool      `json:"is_dir"`
	DirectoryId string    `json:"directory_id"`
	StoreId     string    `json:"store_id"`
	Children    []DirData `json:"children"`
}

type CheckPermissionRes struct {
	Check bool `json:"check"`
	DirData
}

type GetCredentialForm struct {
	AppId    string `json:"app_id"`
	SubAppId string `json:"sub_app_id"`
	ModuleId string `json:"module_id"`
}

type RenewCredentialForm struct {
	RefreshToken string `json:"refresh_token"`
}

type CredentialData struct {
	AppId           string `json:"app_id"`
	SubAppId        string `json:"sub_app_id"`
	ModuleId        string `json:"module_id"`
	DomainId        string `json:"domain_id"`
	Token           string `json:"token"`
	RootDirectoryId string `json:"root_directory_id"`
	Expire          int    `json:"expire"`
	RefreshToken    string `json:"refresh_token"`
}

// path 上传之前 先设置path ，接口返回最后上传的目录信息
type SetPathForm struct {
	DomainId    string `json:"domain_id" validate:"required" message:"domain_id required"`
	ModuleId    string `json:"module_id"`
	Path        string `json:"path"` // path  directory_id 必填其一
	Owner       string `json:"owner"`
	DirectoryId string `json:"directory_id"`
}
type FileData struct {
	FileId           string `json:"file_id"`
	Size             int64  `json:"size"`
	Mime             string `json:"mime"`
	Name             string `json:"name"`              // 文件名称
	DomainId         string `json:"domain_id"`         // 域id
	IsDelete         bool   `json:"is_delete"`         // 是否删除
	Owner            string `json:"owner"`             // 所有者
	AccessPermission string `json:"access_permission"` // 访问权限,public,private
	Permission       string `json:"permission"`        // 权限
	DirectoryId      string `json:"directory_id"`      // 目录id
	IsDirectory      bool   `json:"is_directory"`      // 是否是文件夹
	FileMetaId       string `json:"file_meta_id"`      // 文件元数据id
	Ext              string `json:"ext"`               // 文件扩展名
	StoreId          string `json:"store_id"`          // 存储id
	Path             string `json:"path"`              // 位置
	ModuleId         string `json:"module_id"`
}

// 设置文件上传信息，文件系统上传文件完成之后回调此接口
type SetUploadForm struct {
	Data []FileData `json:"data"`
}
type DeleteMultiForm struct {
	AppId         string   `json:"app_id" validate:"required" message:"app_id required"`
	SubAppId      string   `json:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId      string   `json:"module_id" validate:"required" message:"module_id required"`
	FileIds       []string `json:"file_ids" validate:"required" message:"file_ids required"`
	DeleteForever bool     `json:"delete_forever" message:"delete_forever"`
}

type DeleteForm struct {
	AppId         string `json:"app_id" validate:"required" message:"app_id required"`
	SubAppId      string `json:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId      string `json:"module_id" validate:"required" message:"module_id required"`
	FileId        string `json:"file_id" validate:"required" message:"file_id required"`
	DeleteForever bool   `json:"delete_forever"`
}

type CheckShareForm struct {
	Sid    string `json:"sid" form:"sid" validate:"required" message:"sid required"`
	Pwd    string `json:"pwd" form:"pwd" validate:"required|len:4" message:"required:pwd required|len:pwd len 4"`
	FileId string `json:"file_id" form:"file_id"`
}

type CheckShareResp struct {
	Limit       int        `json:"limit"`        // 下载次数
	Expire      time.Time  `json:"expire"`       // 过期时间
	Type        string     `json:"type"`         // 返回内容 多个文件list,单个download
	FileMetaId  string     `json:"file_meta_id"` // 单个文件元id
	FileId      string     `json:"file_id"`      // 单个文件/目录时的文件名称
	FileName    string     `json:"file_name"`    // 单个文件/目录 文件名称
	IsDirectory bool       `json:"is_directory"` // 单个文件是否是文件夹
	StoreId     string     `json:"store_id"`
	DirData     DirData    `json:"dir_data"`
	List        []FileData `json:"list"` // 多个文件列表
	DomainId    string     `json:"domain_id"`
	AppId       string     `json:"app_id"`
	SubAppId    string     `json:"sub_app_id"`
	ModuleId    string     `json:"module_id"`
}

type S3Config struct {
	EndPoint        string `json:"end_point" form:"end_point" validate:"required" message:"endpoint required"`
	AccessKeyId     string `json:"access_key_id" form:"access_key_id" validate:"required" message:"access_key_id required"`
	SecretAccessKey string `json:"secret_access_key" form:"secret_access_key" validate:"required" message:"secret_access_key required"`
	Region          string `json:"region" form:"region" validate:"required" message:"region required"`
	SSL             bool   `json:"ssl" form:"ssl" validate:"required" message:"ssl required"`
	Bucket          string `json:"bucket" form:"bucket" validate:"required" message:"bucket required"`
	Version         string `json:"version" form:"version" validate:"required" message:"version required"`
	Platform        string `json:"platform" form:"platform" validate:"required" message:"platform required"`
}

type SpaceData struct {
	StoreId          string   `json:"store_id"`
	Title            string   `json:"title"`
	Name             string   `json:"name"`
	Config           S3Config `json:"config"`
	MountDirectoryId string   `json:"mount_directory_id"`
	Capacity         int64    `json:"capacity"` // 容量
	UseCap           int64    `json:"use"`      // 已使用
}

type GetSpaceStoreByDomainForm struct {
	DomainId string `json:"domain_id" form:"domain_id" validate:"required" message:"domain_id required"`
	StoreId  string `json:"store_id" form:"store_id" validate:"required" message:"store_id required"`
}
type GetFileInfoForm struct {
	AppId    string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId string `json:"sub_app_id" form:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId string `json:"module_id" form:"module_id" validate:"required" message:"module_id required"`
	FileId   string `json:"file_id" form:"file_id" validate:"required" message:"file_id required"`
}

type ServiceRegisterForm struct {
	AppId        string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId     string `json:"sub_app_id" form:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId     string `json:"module_id" form:"module_id" validate:"required" message:"module_id required"`
	ServiceName  string `json:"service_name" form:"service_name" validate:"required" message:"service_name required"`
	ServiceTitle string `json:"service_title" form:"service_title" validate:"required" message:"service_name required"`
}
type UnRegisterForm struct {
	AppId       string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId    string `json:"sub_app_id" form:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId    string `json:"module_id" form:"module_id" validate:"required" message:"module_id required"`
	ServiceName string `json:"service_name" form:"service_name" validate:"required" message:"service_name required"`
}
