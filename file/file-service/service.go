package file_service

import (
	"net/url"

	"github.com/gin-gonic/gin"
)

// CheckPermission 检查权限
func CheckPermission(ctx *gin.Context, p CheckPermissionForm) (data CheckPermissionRes, err error) {
	data, err = BasePost[CheckPermissionRes](ctx, "/api/v1/permission/checkPermission", p, map[string]string{})
	return
}

func SetPath(ctx *gin.Context, p SetPathForm) (data FileData, err error) {
	data, err = BasePost[FileData](ctx, "/api/v1/file/setPath", p, map[string]string{})
	return
}

func SetUploadData(ctx *gin.Context, p SetUploadForm) (err error) {
	_, err = BasePost[any](ctx, "/api/v1/file/setUploadData", p, map[string]string{})
	return
}

func GetCredential(ctx *gin.Context, p GetCredentialForm) (data CredentialData, err error) {
	data, err = BasePost[CredentialData](ctx, "/api/v1/permission/getCredential", p, map[string]string{})
	return
}

func RenewCredential(ctx *gin.Context, p RenewCredentialForm) (data CredentialData, err error) {
	data, err = BasePost[CredentialData](ctx, "/api/v1/permission/renewCredential", p, map[string]string{})
	return
}

func DeleteMult(ctx *gin.Context, p DeleteMultiForm) (err error) {
	_, err = BasePost[any](ctx, "/api/v1/file/deleteMult", p, map[string]string{})
	return
}

func Delete(ctx *gin.Context, p DeleteForm) (err error) {
	_, err = BasePost[any](ctx, "/api/v1/file/delete", p, map[string]string{})
	return
}
func CheckShare(ctx *gin.Context, p CheckShareForm) (res CheckShareResp, err error) {
	res, err = BasePost[CheckShareResp](ctx, "/api/v1/share/check", p, map[string]string{})
	return
}

func GetSpaceStoreByDomain(ctx *gin.Context, p GetSpaceStoreByDomainForm) (res SpaceData, err error) {
	params := url.Values{}
	params.Add("domain_id", p.DomainId)
	params.Add("store_id", p.StoreId)
	res, err = BaseGet[SpaceData](ctx, "/api/v1/space/getSpaceStoreByDomain", params, map[string]string{})
	return
}

func GetFileInfo(ctx *gin.Context, p GetFileInfoForm) (res FileData, err error) {
	params := url.Values{}
	params.Add("app_id", p.AppId)
	params.Add("file_id", p.FileId)
	res, err = BaseGet[FileData](ctx, "/api/v1/file/getFileInfo", params, map[string]string{})
	return
}

func ServiceRegister(ctx *gin.Context, p ServiceRegisterForm) (err error) {
	_, err = BasePost[any](ctx, "/api/v1/file/register", p, map[string]string{})
	return
}

func Unregister(ctx *gin.Context, p UnRegisterForm) (err error) {
	_, err = BasePost[any](ctx, "/api/v1/file/unregister", p, map[string]string{})
	return
}
