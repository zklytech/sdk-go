package file_service

import (
	"bytes"
	"encoding/json"
	"errors"
	"gitee.com/zklytech/sdk-go/common"
	file_sys "gitee.com/zklytech/sdk-go/file/file-sys"
	"github.com/gin-gonic/gin"
	"io"
	"mime/multipart"
	"net/http"
	"strconv"
)

type UploadForm struct {
	AppId    string    `json:"app_id"`
	File     io.Reader `json:"file"`
	Path     string    `json:"path"`
	FileName string    `json:"file_name"`
	Owner    string    `json:"owner"`
}

type DownloadForm struct {
	AppId  string `json:"app_id"`
	FileId string `json:"file_id"`
}

func Upload(ctx *gin.Context, form UploadForm) (res file_sys.UploadResponse, err error) {

	credential, err := GetCredential(ctx, GetCredentialForm{AppId: form.AppId})
	if err != nil {
		err = errors.New("get credential err:" + err.Error())
		return
	}

	// 创建一个新的 multipart writer
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	// 创建文件部分
	part, err := writer.CreateFormFile("file", form.FileName)
	if err != nil {
		return
	}

	// 将文件内容写入到 multipart writer
	if _, e := io.Copy(part, form.File); e != nil {
		return
	}
	// 关闭 writer 以完成 multipart 构建
	err = writer.WriteField("domain_id", credential.DomainId)
	if err != nil {
		return
	}
	err = writer.WriteField("path", form.Path)
	if err != nil {
		return
	}
	err = writer.WriteField("token", credential.Token)
	if err != nil {
		return
	}
	err = writer.WriteField("owner", form.Owner)
	if err != nil {
		return
	}
	err = writer.Close()
	if err != nil {
		return
	}

	client := common.NewHttpClient()

	resBody, code, err := client.Post(ctx, file_sys.BaseUrl+"/api/v1/file/upload", body.Bytes(), map[string]string{"Content-Type": writer.FormDataContentType()})
	if err != nil {
		err = errors.New("upload err:" + err.Error())
		return
	}
	if code != http.StatusOK {
		err = errors.New("request upload  err:" + strconv.Itoa(code))
		return
	}
	var resData CommonResData[file_sys.UploadResponse]
	err = json.Unmarshal(resBody, &resData)
	if err != nil {
		err = errors.New("unmarshal err :" + err.Error())
		return
	}
	if resData.Errcode != 0 {
		err = errors.New(resData.Msg)
		return
	}
	res = resData.Data
	return
}

func Download(ctx *gin.Context, form DownloadForm) (resp []byte, err error) {
	credential, err := GetCredential(ctx, GetCredentialForm{AppId: form.AppId})
	if err != nil {
		err = errors.New("get credential err:" + err.Error())
		return
	}
	resp, err = file_sys.Download(ctx, file_sys.DownloadFile{
		DomainId: credential.DomainId,
		FileId:   form.FileId,
		Token:    credential.Token,
	})
	return
}
