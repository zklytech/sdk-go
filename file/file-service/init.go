package file_service

import (
	"encoding/json"
	"errors"
	"gitee.com/zklytech/sdk-go/common"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/url"
	"strconv"
)

const (
	baseUrl = "http://fileservice-service.infra:8080"
)

type CommonResData[T any] struct {
	Errcode int    `json:"errcode"`
	Msg     string `json:"msg"`
	Data    T      `json:"data"`
}

func BaseGet[T any](ctx *gin.Context, api string, params url.Values, header map[string]string) (data T, err error) {

	loginToken := ctx.GetHeader("Logintoken")
	authorization := ctx.GetHeader("Authorization")
	if header == nil {
		header = make(map[string]string)
	}
	header["Authorization"] = authorization
	header["Logintoken"] = loginToken
	client := common.NewHttpClient()
	res, httpCode, err := client.GetWithHeader(ctx, baseUrl+api, params, header)
	if err != nil {
		err = errors.New("request fail: " + err.Error())
		return
	}
	if httpCode != http.StatusOK {
		err = errors.New("request fail: " + strconv.Itoa(httpCode))
		return
	}
	var commonRes CommonResData[T]

	err = json.Unmarshal(res, &commonRes)
	if err != nil {
		err = errors.New("unmarshal fail: " + err.Error())
		return
	}
	if commonRes.Errcode != 0 {
		err = errors.New("service err " + commonRes.Msg)
		return
	}
	return commonRes.Data, nil

}

func BasePost[T any](ctx *gin.Context, api string, data any, header map[string]string) (resData T, err error) {
	client := common.NewHttpClient()
	bodyJosn, err := json.Marshal(data)
	if err != nil {
		err = errors.New("marshal fail: " + err.Error())
		return
	}

	loginToken := ctx.GetHeader("Logintoken")
	authorization := ctx.GetHeader("Authorization")
	if header == nil {
		header = make(map[string]string)
	}
	header["Authorization"] = authorization
	header["Logintoken"] = loginToken

	res, httpCode, err := client.PostJson(ctx, baseUrl+api, bodyJosn, header)
	if err != nil {
		err = errors.New("request fail: " + err.Error())
		return
	}
	if httpCode != http.StatusOK {
		err = errors.New("request fail: " + strconv.Itoa(httpCode))
		return
	}
	var commonRes CommonResData[T]
	err = json.Unmarshal(res, &commonRes)
	if err != nil {
		err = errors.New("unmarshal fail: " + err.Error())
		return
	}
	if commonRes.Errcode != 0 {
		err = errors.New("service err " + commonRes.Msg)
		return
	}
	return commonRes.Data, nil
}
