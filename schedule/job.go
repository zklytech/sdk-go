package schedule

import (
	"fmt"
	"log"
	"sort"
	"sync"
	"time"
)

// 定时任务参数类型
type ScheduleType int

const (
	Daily ScheduleType = iota
	Weekly
	Monthly
	Fixed
	Hour
	Min
	Quarter
	Year
	OddMonth
	EvenMonth
)

// 定时任务参数结构体
type Schedule struct {
	Type           ScheduleType
	Interval       int            // 时间间隔，例如：每隔几天，每隔几周，每隔几个月
	Time           time.Time      // 每天的几时几分
	Weekdays       []time.Weekday // 每周的哪几天
	Days           []int          // 每月的哪几天
	Months         []int          // 每年的哪几个月
	MonthNumber    []int          // 每季度的第几个月
	FixedTime      *time.Time     // 固定时间
	ValidTimeRange *TimeRange     // 有效时间区间
}

// 时间区间结构体
type TimeRange struct {
	Start time.Time
	End   time.Time
}

// Scheduler 定时任务调度器
type Scheduler struct {
	schedules     []Schedule
	task          func()
	running       bool
	stopCh        chan struct{}
	mu            sync.Mutex
	taskRunning   bool
	taskRunningMu sync.Mutex
}

// NewScheduler 创建一个新的定时任务调度器
func NewScheduler(schedules []Schedule, task func()) *Scheduler {
	return &Scheduler{
		schedules: schedules,
		task:      task,
		stopCh:    make(chan struct{}),
	}
}

// Start 启动定时任务
func (s *Scheduler) Start() {
	s.mu.Lock()
	defer s.mu.Unlock()
	if s.running {
		return
	}
	s.running = true
	go s.run()
}

// Stop 停止定时任务
func (s *Scheduler) Stop() {
	s.mu.Lock()
	defer s.mu.Unlock()
	if !s.running {
		return
	}
	s.running = false
	close(s.stopCh)
	s.stopCh = make(chan struct{})
}

// IsRunning 返回任务是否正在运行
func (s *Scheduler) IsRunning() bool {
	s.mu.Lock()
	defer s.mu.Unlock()
	return s.running
}

// IsTaskRunning 返回任务函数是否正在执行
func (s *Scheduler) IsTaskRunning() bool {
	s.taskRunningMu.Lock()
	defer s.taskRunningMu.Unlock()
	return s.taskRunning
}

func (s *Scheduler) run() {
	for {
		nextRun := s.calculateNextRunTime(time.Now())
		if nextRun.IsZero() {
			log.Println("Next run is zero")
			s.Stop()
			log.Println("Next run is stopped")
			time.Sleep(1 * time.Second)
			return
		}

		sleepDuration := nextRun.Sub(time.Now())
		log.Println("sleep", sleepDuration)
		if sleepDuration.Seconds() < 0 {
			log.Println("sleep time too small")
			// 防止一直死锁执行
			s.Stop()
		}
		select {
		case <-time.After(sleepDuration):
			log.Println("execute task")
			s.executeTask()
			log.Println("execute task finished")
		case <-s.stopCh:
			return
		}
	}
}
func minTime(times []time.Time) time.Time {
	if len(times) == 0 {
		return time.Time{}
	}

	min := times[0]
	for _, t := range times {
		if t.Before(min) {
			min = t
		}
	}
	return min
}

func (s *Scheduler) executeTask() {
	s.taskRunningMu.Lock()
	s.taskRunning = true
	s.taskRunningMu.Unlock()

	// 执行任务
	s.task()

	s.taskRunningMu.Lock()
	s.taskRunning = false
	s.taskRunningMu.Unlock()
}
func isDayInCurrentMonth(day int) bool {
	now := time.Now()
	year, month, _ := now.Date()
	daysInMonth := time.Date(year, month+1, 0, 0, 0, 0, 0, time.Local).Day()
	return day >= 1 && day <= daysInMonth
}

// IsDateInMonth 检查给定的日期是否存在于指定的月份和年份 不存在则返回月份的最后一天
func IsDateInMonth(year int, month int, day int) (bool, int) {
	// 检查月份是否合法
	if month < 1 || month > 12 {
		return false, 1
	}

	// 获取指定月份的第一天和下一个月的第一天
	firstOfMonth := time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.Local)
	firstOfNextMonth := firstOfMonth.AddDate(0, 1, 0)

	// 计算指定月份的天数
	daysInMonth := firstOfNextMonth.Sub(firstOfMonth).Hours() / 24
	if day > 0 && day <= int(daysInMonth) {
		return true, day
	}
	// 检查给定的天数是否在合法范围内
	return false, int(daysInMonth)
}

// GetQuarterlyNextTime 返回距离当前时间最近的指定季度、月份、日期、时间，带有间隔
func GetQuarterlyNextTime(now time.Time, quarterMonths []int, days []int, hour int, minute int, interval int) time.Time {
	//now := time.Now()
	sort.Ints(quarterMonths)
	sort.Ints(days)
	currentYear, currentMonth, currentDay := now.Date()
	currentHour, currentMinute, _ := now.Clock()
	newYear := currentYear
	// 计算下一个符合条件的时间
	for {
		for _, quarterMonth := range quarterMonths {
			for i := 0; i < 4; i += interval { // 按照interval跳跃季度
				month := time.Month(i*3 + quarterMonth)
				for _, day := range days {

					_, day = IsDateInMonth(newYear, int(month), day)
					// 如果当前月份、日期、时间已经过去，查找下一个
					if (newYear >= currentYear) || (month > currentMonth) || (month == currentMonth && day > currentDay) || (month == currentMonth && day == currentDay && (hour > currentHour || (hour == currentHour && minute > currentMinute))) {
						nextTime := time.Date(newYear, month, day, hour, minute, 0, 0, now.Location())
						if nextTime.After(now) {
							return nextTime
						}
					}
				}
			}
		}
		// 增加一个完整的间隔（interval * 3 个月）
		newYear, currentMonth = increaseQuarter(newYear, currentMonth, interval)
	}
}

// increaseQuarter 增加指定数量的季度（interval * 3 个月）
func increaseQuarter(year int, month time.Month, interval int) (int, time.Month) {
	month += time.Month(interval * 3)
	if month > 12 {
		year += int(month-1) / 12
		month = month % 12
		if month == 0 {
			month = 12
		}
	}
	return year, month
}

func GetYearNext(now time.Time, schedule Schedule) time.Time {
	var candidateTime time.Time
	// 每隔几年几月几日几点几分
	sort.Ints(schedule.Months)
	sort.Ints(schedule.Days)
	for {
		for _, month := range schedule.Months {
			for _, day := range schedule.Days {
				// 如果指定的日期不存在，跳过（例如2月30日）
				_, newDay := IsDateInMonth(now.Year(), month, day)
				candidateTime = time.Date(now.Year(), time.Month(month), newDay, schedule.Time.Hour(), schedule.Time.Minute(), 0, 0, now.Location())
				if candidateTime.After(now) {
					candidateTime = time.Date(candidateTime.Year(), candidateTime.Month(), newDay, schedule.Time.Hour(), schedule.Time.Minute(), 0, 0, now.Location())
					return candidateTime
				}
			}
		}
		// 跨年之后取第一天
		newDay := candidateTime.Day()
		candidateTime = candidateTime.AddDate(schedule.Interval, 0, 0)
		_, newDay = IsDateInMonth(now.Year(), schedule.Months[0], schedule.Days[0])

		candidateTime = time.Date(candidateTime.Year(), time.Month(schedule.Months[0]), newDay, schedule.Time.Hour(), schedule.Time.Minute(), 0, 0, now.Location())
		if candidateTime.After(now) {
			return candidateTime
		}

	}
}

func GetOddEvenMonthNext(now time.Time, schedule Schedule) (candidateTime time.Time) {
	sort.Ints(schedule.Days)
	switch schedule.Type {
	// 单月
	case OddMonth:
		nextMonth := now.Month()
		if nextMonth%2 == 0 {
			nextMonth++
		}
		nextYear := now.Year()
		if nextMonth > 12 {
			nextMonth = 1
			nextYear++
		}
		for {
			for _, day := range schedule.Days {
				_, newDay := IsDateInMonth(nextYear, int(nextMonth), day)
				nextTime := time.Date(nextYear, nextMonth, newDay, schedule.Time.Hour(), schedule.Time.Minute(), 0, 0, now.Location())
				if nextTime.After(now) {
					return nextTime
				}
			}
			nextMonth = nextMonth + 2
		}
	// 双月
	case EvenMonth:
		nextMonth := now.Month()
		if nextMonth%2 != 0 {
			nextMonth++
		}
		nextYear := now.Year()
		if nextMonth > 12 {
			nextMonth = 2
			nextYear++
		}
		for {
			for _, day := range schedule.Days {
				_, newDay := IsDateInMonth(nextYear, int(nextMonth), day)
				nextTime := time.Date(nextYear, nextMonth, newDay, schedule.Time.Hour(), schedule.Time.Minute(), 0, 0, now.Location())
				if nextTime.After(now) {
					return nextTime
				}
			}
			nextMonth = nextMonth + 2
		}
	default:
		// Handle other schedule types
		return time.Time{}
	}
}

func GetMonthNext(now time.Time, schedule Schedule) (candidateTime time.Time) {
	nextYear := now.Year()
	nextMonth := now.Month()
	for {
		for _, day := range schedule.Days {
			_, newDay := IsDateInMonth(nextYear, int(nextMonth), day)
			nextTime := time.Date(nextYear, nextMonth, newDay, schedule.Time.Hour(), schedule.Time.Minute(), 0, 0, now.Location())
			if nextTime.After(now) {
				return nextTime
			}
		}
		nextMonth = nextMonth + time.Month(schedule.Interval)
		if nextMonth > 12 {
			nextYear = nextYear + 1
			nextMonth = nextMonth % 12
		}
	}
}

func GetWeekNext(now time.Time, schedule Schedule) (candidateTime time.Time) {
	sort.Slice(schedule.Weekdays, func(i, j int) bool {
		return int(schedule.Weekdays[i]) < int(schedule.Weekdays[j])
	})
	for {
		var dayOffset int
		for _, weekday := range schedule.Weekdays {
			dayOffset = (int(weekday) - int(now.Weekday()) + 7) % 7
			candidateTime = now.AddDate(0, 0, dayOffset)
			candidateTime = time.Date(candidateTime.Year(), candidateTime.Month(), candidateTime.Day(), schedule.Time.Hour(), schedule.Time.Minute(), 0, 0, candidateTime.Location())
			if candidateTime.After(now) {
				return candidateTime
			}
		}
		if candidateTime.Before(now) {
			candidateTime = candidateTime.AddDate(0, 0, 7*schedule.Interval)
			return
		}
	}
}

func (s *Scheduler) calculateNextRunTime(now time.Time) time.Time {
	log.Println("now ", now)
	var nextRun time.Time
	for _, schedule := range s.schedules {
		if schedule.ValidTimeRange != nil {
			if now.Before(schedule.ValidTimeRange.Start) || now.After(schedule.ValidTimeRange.End) {
				continue
			}
		}

		var candidateTime time.Time

		switch schedule.Type {
		case Daily:
			candidateTime = now.AddDate(0, 0, 0)
			candidateTime = time.Date(candidateTime.Year(), candidateTime.Month(), candidateTime.Day(), schedule.Time.Hour(), schedule.Time.Minute(), 0, 0, candidateTime.Location())
			if candidateTime.Before(now) {
				candidateTime = candidateTime.AddDate(0, 0, schedule.Interval)
			}
			log.Println("day", candidateTime)
			break
		case Weekly:
			candidateTime = GetWeekNext(now, schedule)
			log.Println("weekday", candidateTime)
			break
		case Monthly:
			candidateTime = GetMonthNext(now, schedule)
			log.Println("month", candidateTime)
			break
		case OddMonth:
			// 单月 1,3,5..
			candidateTime = GetOddEvenMonthNext(now, schedule)
			log.Println("OddMonth", candidateTime)
			break
		case EvenMonth:
			// 双月 2,4,6...
			candidateTime = GetOddEvenMonthNext(now, schedule)
			log.Println("EvenMonth", candidateTime)
			break
		case Fixed:
			candidateTime = *schedule.FixedTime
			if candidateTime.Before(now) {
				continue
			}
			log.Println("fix", candidateTime)
			break
		case Hour:
			// 每多少小时的多少分钟
			candidateTime = now.Add(time.Hour * time.Duration(schedule.Interval))
			candidateTime = time.Date(candidateTime.Year(), candidateTime.Month(), candidateTime.Day(), candidateTime.Hour(), schedule.Time.Minute(), 0, 0, time.Local)
			// 时间落后就增加
			if candidateTime.Before(now) || candidateTime.Equal(now) {
				candidateTime = candidateTime.Add(time.Hour * time.Duration(schedule.Interval))
			}
			log.Println("hour", candidateTime)
			break
		case Min:
			// 每隔多少分钟执行
			candidateTime = now.Add(time.Minute * time.Duration(schedule.Interval))
			log.Println("minute", candidateTime)
			break
		case Quarter:
			// 每隔几季度 第几个月，第几号，几时几分执行
			candidateTime = GetQuarterlyNextTime(now, schedule.Months, schedule.Days, schedule.Time.Hour(), schedule.Time.Minute(), schedule.Interval)
			log.Println("quarter", candidateTime)
			break
		case Year:
			// 每隔几年几月几日几点几分
			candidateTime = GetYearNext(now, schedule)
			log.Println("year", candidateTime)
			break

		}
		if nextRun.IsZero() || candidateTime.Before(nextRun) {
			nextRun = candidateTime
		}
	}
	log.Println("next", nextRun)
	return nextRun
}

// 辅助函数：返回时间指针
func TimePtr(t time.Time) *time.Time {
	return &t
}

// convertToTime 函数将 "HH:MM" 格式的字符串转换为 time.Time 类型
func ConvertToTime(timeStr string) (time.Time, error) {
	// 定义一个布局来解析 "02:03" 格式的时间字符串
	// 使用当日的日期来创建完整的时间
	fullTimeStr := fmt.Sprintf("%s %s", time.Now().Format(time.DateOnly), timeStr)
	return time.Parse(time.RFC3339, fullTimeStr)
}
