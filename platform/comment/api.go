package comment

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"gitee.com/zklytech/sdk-go/common"
	"github.com/gin-gonic/gin"
)

var client *common.HttpClient

const baseUrl = "http://platform.infra:8090/api"

func init() {
	client = common.NewHttpClient()
}

func CreateComment(ctx *gin.Context, form CreateCommentReq) (result Comment, err error) {
	var u = baseUrl + "/comment/create"
	bdata, _ := json.Marshal(form)
	res, status, err := client.PostJson(ctx, u, bdata, map[string]string{})
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = errors.New("网络请求错误：" + strconv.Itoa(status))
		return
	}

	var j Result[Comment]
	json.Unmarshal(res, &j)
	if j.ErrCode != 0 {
		err = errors.New(j.Msg)
		return
	}

	result = j.Data

	return
}

func QueryComment(ctx *gin.Context, form QueryCommentReq) (result PageResult[Comment], err error) {
	var u = baseUrl + "/comment/query"
	bdata, _ := json.Marshal(form)
	res, status, err := client.PostJson(ctx, u, bdata, map[string]string{})
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = errors.New("网络请求错误：" + strconv.Itoa(status))
		return
	}

	var j Result[PageResult[Comment]]
	json.Unmarshal(res, &j)
	if j.ErrCode != 0 {
		err = errors.New(j.Msg)
		return
	}

	result = j.Data
	return
}

func QueryCommentTotal(ctx *gin.Context, form QueryCommentReq) (result int64, err error) {
	var u = baseUrl + "/comment/query/total"
	bdata, _ := json.Marshal(form)
	res, status, err := client.PostJson(ctx, u, bdata, map[string]string{})
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = errors.New("网络请求错误：" + strconv.Itoa(status))
		return
	}

	var j Result[int64]
	json.Unmarshal(res, &j)
	if j.ErrCode != 0 {
		err = errors.New(j.Msg)
		return
	}

	result = j.Data
	return
}

func Good(ctx *gin.Context, form GoodComment) (err error) {
	var u = baseUrl + "/comment/good"
	bdata, _ := json.Marshal(form)
	res, status, err := client.PostJson(ctx, u, bdata, map[string]string{})
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = errors.New("网络请求错误：" + strconv.Itoa(status))
		return
	}

	var j Result[any]
	json.Unmarshal(res, &j)
	if j.ErrCode != 0 {
		err = errors.New(j.Msg)
		return
	}
	return
}
