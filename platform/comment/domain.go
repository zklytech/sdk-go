package comment

import "time"

type Result[T any] struct {
	ErrCode int    `json:"errcode"`
	Msg     string `json:"msg"`
	Data    T      `json:"data"`
}

type PageResult[T any] struct {
	Page     int   `json:"page"`
	PageSize int   `json:"pageSize"`
	List     []T   `json:"list"`
	Total    int64 `json:"total"`
}

type QueryCommentReq struct {
	Page      int    `json:"page"`
	PageSize  int    `json:"page_size"`
	OrderBy   string `json:"order_by"`
	AscOrDesc string `json:"asc_or_desc"`
	Source    string `json:"source" validate:"required" message:"评论来源不能为空"`
	TargetId  string `json:"target_id" validate:"required" message:"目标ID不能为空"`
}

type CreateCommentReq struct {
	Source     string   `json:"source" validate:"required" message:"评论来源不能为空"`
	TargetId   string   `json:"target_id" validate:"required" message:"目标ID不能为空"`
	Content    string   `json:"content" validate:"required" message:"评论内容不能为空"`
	ImageList  []string `json:"image_list"`
	Additional string   `json:"additional"`
}

type GoodComment struct {
	Source    string `json:"source" validate:"required" message:"评论来源不能为空"`
	TargetId  string `json:"target_id" validate:"required" message:"目标ID不能为空"`
	CommentId int    `json:"id" validate:"required" message:"评论ID不能为空"`
}

type Comment struct {
	Id         int       `json:"id"`
	CreateTime time.Time `json:"create_time"`
	UpdateTime time.Time `json:"update_time"`
	Source     string    `json:"source"`
	TargetId   string    `json:"target_id"`
	Additional string    `json:"additional"`
	GoodCount  int       `json:"good_count"`
	Content    string    `json:"content"`
	ImageList  []string  `json:"image_list"`
	UserId     string    `json:"user_id"`
	Account    string    `json:"account"`
	UserName   string    `json:"user_name"`
	Avatar     string    `json:"avatar"`
}
