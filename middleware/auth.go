package authorization

import (
	"encoding/json"
	"fmt"

	"gitee.com/zklytech/sdk-go/common"
	"github.com/gin-gonic/gin"
)

type HeaderParams struct {
	AppId         string `header:"App-Id"`
	Authorization string `header:"Logintoken"`
}

type TokenResult struct {
	ErrCode int      `json:"errcode"`
	Msg     string   `json:"msg"`
	Data    UserInfo `json:"data"`
}

type UserInfo struct {
	AppId     string         `json:"app_id"`
	ID        string         `json:"id"`
	Account   string         `json:"account"`
	CacheData map[string]any `json:"cache_data"`
}

const baseUrl = "http://auth-service.infra:8091/api/token/check"

var client common.HttpClient

func init() {
	client = *common.NewHttpClient()
}

func CheckAuth() gin.HandlerFunc {
	return func(ctx *gin.Context) {

		fmt.Println(ctx.Request.URL.Path)

		headerParams := HeaderParams{}
		//  推荐使用 ShouldBindHeader 方式获取头参数
		if err := ctx.ShouldBindHeader(&headerParams); err != nil {
			ctx.Next()
			return
		}

		// 获取token
		token := headerParams.Authorization
		if len(token) == 0 {
			ctx.Next()
			return
		}
		params := map[string]any{
			"app_id": headerParams.AppId,
			"token":  headerParams.Authorization,
		}
		bdata, _ := json.Marshal(params)

		// token验证
		resp, _, err := client.PostJson(ctx, baseUrl, bdata, map[string]string{})
		if err != nil {
			ctx.Next()
			return
		}

		var j TokenResult
		json.Unmarshal(resp, &j)

		if j.ErrCode != 0 {
			fmt.Println("解析token失败")
			return
		}
		ctx.Set("app_user_info", j.Data)
		ctx.Next()
	}
}
