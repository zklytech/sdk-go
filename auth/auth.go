package auth

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

type Claims struct {
	ID      int64
	UserId  string
	Account string
	jwt.StandardClaims
}

func ParseToken(token string, key string) (*Claims, error) {
	tokenClaims, err := jwt.ParseWithClaims(token, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(key), nil
	})
	if err != nil {
		return nil, err
	}

	if tokenClaims != nil {
		if claims, ok := tokenClaims.Claims.(*Claims); ok && tokenClaims.Valid {
			return claims, nil
		}
	}

	return nil, err
}

func GenerateToken(id int, account string, userId string, key string) (string, error) {
	nowTime := time.Now()
	expiredTime := nowTime.Add(7 * 24 * time.Hour)
	claims := Claims{
		ID:      int64(id),
		Account: account,
		UserId:  userId,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiredTime.Unix(),
			Issuer:    account,
		},
	}
	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(key))
	return token, err
}
