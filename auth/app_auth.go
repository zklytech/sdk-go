package auth

import (
	"context"
	"fmt"

	app_manager "gitee.com/zklytech/sdk-go/app-manager"
	"gitee.com/zklytech/sdk-go/tool"
	"github.com/gin-gonic/gin"
	"github.com/redis/go-redis/v9"
)

type HeaderParams struct {
	Authorization string `header:"Authorization"`
	AppId         string `header:"App-Id"`
	Runtime       string `header:"Runtime"`
}

var Redis *redis.Client

func AppAuth(key string) gin.HandlerFunc {
	return func(ctx *gin.Context) {

		fmt.Println(ctx.Request.URL.Path)

		headerParams := HeaderParams{}
		//  推荐使用 ShouldBindHeader 方式获取头参数
		if err := ctx.ShouldBindHeader(&headerParams); err != nil {
			tool.AuthFail(ctx, 3001, "auth error", gin.H{})
			ctx.Abort()
			return
		}

		if headerParams.Runtime == "prod" {
			ctx.Next()
			return
		}

		// token验证
		token := headerParams.Authorization
		// 如果没有token则放行，token验证交由AuthCheck处理
		if len(token) == 0 {
			ctx.Next()
			return
		}
		userInfo, err := ParseToken(token, key)
		if err != nil {
			ctx.Next()
			return
		}

		if !AppCacheCheck(userInfo.UserId) {
			RefreshUserApp(ctx, userInfo.UserId)
		}

		if len(headerParams.AppId) > 0 && !AppCheck(userInfo.UserId, headerParams.AppId) {
			tool.AuthFail(ctx, 3001, "没有此App权限", gin.H{})
			ctx.Abort()
			return
		}

		ctx.Next()
	}
}

func AppCacheCheck(userId string) bool {
	key := "account:application:" + userId
	ex, err := Redis.Exists(context.TODO(), key).Result()
	if err != nil {
		return false
	}

	return ex > 0
}

func AppCheck(userId string, appId string) bool {
	key := "account:application:" + userId
	ex, _ := Redis.SIsMember(context.TODO(), key, appId).Result()

	return ex
}

func ClearUserApp(ctx *gin.Context, userId string) {
	key := "account:application:" + userId
	Redis.Del(context.TODO(), key)
}

func RefreshUserApp(ctx *gin.Context, userId string) {
	key := "account:application:" + userId
	appIds, err := app_manager.QueryUserApp(ctx, userId)
	if err != nil {
		return
	}
	Redis.SAdd(context.TODO(), key, appIds)
}
