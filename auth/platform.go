package auth

import (
	"fmt"

	"gitee.com/zklytech/sdk-go/tool"
	"github.com/gin-gonic/gin"
)

func CheckAuth(ignoreUrl []string, key string) gin.HandlerFunc {
	return func(ctx *gin.Context) {

		fmt.Println(ctx.Request.URL.Path)
		fmt.Println(ignoreUrl)

		var ignore = false
		for _, url := range ignoreUrl {
			if url == ctx.Request.URL.Path {
				ignore = true
			}
		}

		if ignore {
			ctx.Next()
			return
		}

		headerParams := HeaderParams{}
		//  推荐使用 ShouldBindHeader 方式获取头参数
		if err := ctx.ShouldBindHeader(&headerParams); err != nil {
			tool.AuthFail(ctx, 3001, "auth error", gin.H{})
			ctx.Abort()
			return
		}

		// 运行态不校验配置态认证信息
		if headerParams.Runtime == "prod" {
			ctx.Next()
			return
		}
		// 获取token
		token := headerParams.Authorization
		if len(token) == 0 {
			tool.AuthFail(ctx, 3001, "auth error", gin.H{})
			ctx.Abort()
			return
		}
		// token验证
		userInfo, err := ParseToken(token, key)
		if err != nil {
			tool.AuthFail(ctx, 3001, "auth error", gin.H{})
			ctx.Abort()
			return
		}

		ctx.Set("user_info", userInfo)

		ctx.Next()
	}
}
