package timer_service

import (
	"errors"
	"github.com/gin-gonic/gin"
)

func Create(ctx *gin.Context, form CreateTimerTaskForm) (data TaskData, err error) {
	data, err = basePostRequest[TaskData](ctx, "/v1/timer/create", form)
	if err != nil {
		err = errors.New("create timer task fail " + err.Error())
		return
	}
	return
}

func Update(ctx *gin.Context, form UpdateTimerTaskForm) (err error) {
	_, err = basePostRequest[any](ctx, "/v1/timer/update", form)
	if err != nil {
		err = errors.New("update timer task fail " + err.Error())
		return
	}
	return
}

func Delete(ctx *gin.Context, form DeleteTimerTaskForm) (err error) {
	_, err = basePostRequest[any](ctx, "/v1/timer/delete", form)
	if err != nil {
		err = errors.New("delete timer task fail " + err.Error())
		return
	}
	return
}

func Start(ctx *gin.Context, form StartTimerForm) (err error) {
	_, err = basePostRequest[any](ctx, "/v1/timer/start", form)
	if err != nil {
		err = errors.New("start timer task fail " + err.Error())
		return
	}
	return
}

func Stop(ctx *gin.Context, form StopTimerForm) (err error) {
	_, err = basePostRequest[any](ctx, "/v1/timer/stop", form)
	if err != nil {
		err = errors.New("stop timer task fail " + err.Error())
		return
	}
	return
}
