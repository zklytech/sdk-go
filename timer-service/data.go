package timer_service

type CreateTimerTaskForm struct {
	Name        string         `json:"name" form:"name" validate:"required" message:"name required"`
	Title       string         `json:"title" form:"title" validate:"required" message:"title required"`
	CallbackUrl string         `json:"callback_url" form:"callback_url" validate:"required" message:"callback_url required"`
	Data        map[string]any `json:"data" form:"data" validate:"required" message:"data required"`
	Type        string         `json:"type" form:"type" validate:"required" message:"type required"`
	Format      string         `json:"format" form:"format" validate:"required|in:cron,hour,min,second,time" message:"required: format required|in: format must be cron,hour,min,second,time"`
	Value       string         `json:"value" form:"value" validate:"required" message:"value required"`
	RetryNumber int            `json:"retry_number" form:"retry_number" validate:"required|max:10" message:"required:retry_number required|max: retry_number must be less than 10"`
	Interval    int            `json:"interval" form:"interval" validate:"required|min:1" message:"required:interval required|min: interval must be greater than 0"`
	Start       string         `json:"start" form:"start" validate:"required" message:"start required"`
	End         string         `json:"end" form:"end" validate:"required" message:"end required"`
	TenantId    string         `json:"tenant_id" form:"tenant_id" validate:"required" message:"tenant_id required"`
	IsStart     bool           `json:"is_start" form:"is_start"`
}

type TaskData struct {
	TaskId      string         `json:"task_id" form:"task_id"`
	Name        string         `json:"name" form:"name"`
	Title       string         `json:"title" form:"title" `
	CallbackUrl string         `json:"callback_url" form:"callback_url" `
	Data        map[string]any `json:"data" form:"data" `
	Type        string         `json:"type" form:"type" `
	Format      string         `json:"format" form:"format" `
	Value       string         `json:"value" form:"value" `
	RetryNumber int            `json:"retry_number" form:"retry_number" `
	Interval    int            `json:"interval" form:"interval" `
	Start       string         `json:"start" form:"start" `
	End         string         `json:"end" form:"end" `
	TenantId    string         `json:"tenant_id" form:"tenant_id" `
}

type UpdateTimerTaskForm struct {
	Id          string         `json:"id" form:"id" validate:"required" message:"id required"`
	Name        string         `json:"name" form:"name" `
	Title       string         `json:"title" form:"title" `
	CallbackUrl string         `json:"callback_url" form:"callback_url" `
	Status      string         `json:"status" form:"status" `
	Data        map[string]any `json:"data" form:"data" `
	Type        string         `json:"type" form:"type" `
	Format      string         `json:"format" form:"format" `
	Value       string         `json:"value" form:"value" `
	RetryNumber int            `json:"retry_number" form:"retry_number" `
	Interval    int            `json:"interval" form:"interval" `
	Start       string         `json:"start" form:"start" `
	End         string         `json:"end" form:"end" `
	TenantId    string         `json:"tenant_id" form:"tenant_id" `
	IsStart     bool           `json:"is_start"`
}

type DeleteTimerTaskForm struct {
	Id string `json:"id" form:"id" validate:"required" message:"id required"`
}

type StartTimerForm struct {
	Id string `json:"id" form:"id" validate:"required" message:"id required"`
}
type StopTimerForm struct {
	Id string `json:"id" form:"id" validate:"required" message:"id required"`
}
