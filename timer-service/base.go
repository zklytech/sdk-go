package timer_service

import (
	"encoding/json"
	"errors"
	"gitee.com/zklytech/sdk-go/common"
	"github.com/gin-gonic/gin"
	"strconv"
)

const baseUrl = "http://scheduler-service.infra:8080/api"

type ResCommonData[T any] struct {
	Errcode int    `json:"errcode"`
	Msg     string `json:"msg"`
	Data    T      `json:"data"`
}

func basePostRequest[T any](ctx *gin.Context, url string, form interface{}) (data T, err error) {
	client := common.NewHttpClient()
	body, err := json.Marshal(form)
	if err != nil {
		err = errors.New("marshal err " + err.Error())
		return
	}
	res, statusCode, err := client.PostJson(ctx, baseUrl+url, body, map[string]string{})
	if err != nil {
		err = errors.New("request err " + err.Error())
		return
	}
	if statusCode != 200 {
		err = errors.New("request fail httpcode:" + strconv.Itoa(statusCode) + " " + string(res))
		return
	}
	var resData ResCommonData[T]
	err = json.Unmarshal(res, &resData)
	if err != nil {
		err = errors.New("unmarshal err " + err.Error())
		return
	}
	if resData.Errcode != 0 {
		err = errors.New("ide service err " + resData.Msg)
	}
	data = resData.Data
	return
}
