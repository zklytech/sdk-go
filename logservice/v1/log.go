package logservice

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/zklytech/sdk-go/common"
	"gitee.com/zklytech/sdk-go/logservice/v1/level"
	"github.com/gin-gonic/gin"
	"github.com/gookit/validate"
	"log"
	"os"
	"strings"
	"time"
)

const Host = "http://log-service-service.infra:8080/api/v1"

//const Host = "http://192.168.100.57/ingress/log-service/api/v1"

func ValidateStruct(model any) (err error) {
	v := validate.Struct(model)
	if v.Validate() {
		return
	} else {
		errMap := v.Errors.All()
		var msgList []string
		for _, e := range errMap {
			for _, msg := range e {
				msgList = append(msgList, msg)
			}
		}
		err = errors.New(strings.Join(msgList, ","))
		return
	}
}

type Content struct {
	Title string `json:"title" form:"title"`
	Value any    `json:"value" form:"value"`
}

type CreateForm struct {
	TenantId   string `json:"tenant_id" form:"tenant_id" validate:"required" message:"tenant_id required"`
	Content    string `json:"content" form:"content"`
	Title      string `json:"title" form:"title"`
	Level      string `json:"level" form:"level" validate:"required|in:fatal,error,warn,info,debug,trace" message:"required:level required|in:level is fatal,error,warn,info,debug,trace"`
	Operator   string `json:"operator" form:"operator"`
	Source     string `json:"source" form:"source"`
	ReportTime string `json:"report_time" form:"report_time"`
	EventId    string `json:"event_id"`
	RuntimeId  string `json:"runtime_id"`

	TriggerTitle string   `json:"trigger_title" form:"trigger_title"`
	TriggerName  string   `json:"trigger_name" form:"trigger_name"`
	TraceId      string   `json:"trace_id" form:"trace_id"`
	Tag          []string `json:"tag" form:"tag"`
	Service      string   `json:"service" form:"service"`
	Category     string   `json:"category" form:"category"`
	Operation    string   `json:"operation" form:"operation"`
	Value        any      `json:"value" form:"value"`
}

type Res[T any] struct {
	ErrCode int    `json:"errcode"`
	Msg     string `json:"msg"`
	Data    T
}
type CommonForm struct {
	TriggerTitle string   `json:"trigger_title" form:"trigger_title"`
	TriggerName  string   `json:"trigger_name" form:"trigger_name"`
	Tag          []string `json:"tag" form:"tag"`
	Category     string   `json:"category" form:"category"`
	Operation    string   `json:"operation" form:"operation"`
	Operator     string   `json:"operator"`
	Source       string   `json:"source"`
	EventId      string   `json:"event_id"`
	RuntimeId    string   `json:"runtime_id"`
}

func Fatal(ctx *gin.Context, tenantId, title, content string, Value any, c CommonForm) {
	err := Create(ctx, CreateForm{
		TenantId:     tenantId,
		Title:        title,
		Value:        Value,
		Content:      content,
		Level:        level.Fatal,
		Operator:     c.Operator,
		Source:       c.Source,
		TriggerTitle: c.TriggerTitle,
		TriggerName:  c.TriggerName,
		Tag:          c.Tag,
		Category:     c.Category,
		Operation:    c.Operation,
		EventId:      c.EventId,
		RuntimeId:    c.RuntimeId,
	})
	if err != nil {
		log.Println("log err " + err.Error())
	}

}
func Error(ctx *gin.Context, tenantId, title, content string, Value any, c CommonForm) {
	err := Create(ctx, CreateForm{
		TenantId:     tenantId,
		Title:        title,
		Value:        Value,
		Content:      content,
		Level:        level.Error,
		Operator:     c.Operator,
		Source:       c.Source,
		TriggerTitle: c.TriggerTitle,
		TriggerName:  c.TriggerName,
		Tag:          c.Tag,
		Category:     c.Category,
		Operation:    c.Operation,
		EventId:      c.EventId,
		RuntimeId:    c.RuntimeId,
	})
	if err != nil {
		log.Println("log err " + err.Error())
	}
}

func Info(ctx *gin.Context, tenantId, title, content string, Value any, c CommonForm) {

	err := Create(ctx, CreateForm{
		TenantId:     tenantId,
		Title:        title,
		Value:        Value,
		Content:      content,
		Level:        level.Info,
		Operator:     c.Operator,
		Source:       c.Source,
		TriggerTitle: c.TriggerTitle,
		TriggerName:  c.TriggerName,
		Tag:          c.Tag,
		Category:     c.Category,
		Operation:    c.Operation,
		EventId:      c.EventId,
		RuntimeId:    c.RuntimeId,
	})
	if err != nil {
		log.Println("log err " + err.Error())
	}
}
func Warn(ctx *gin.Context, tenantId, title, content string, Value any, c CommonForm) {

	err := Create(ctx, CreateForm{
		TenantId:     tenantId,
		Title:        title,
		Content:      content,
		Level:        level.Warn,
		Operator:     c.Operator,
		Source:       c.Source,
		TriggerTitle: c.TriggerTitle,
		TriggerName:  c.TriggerName,
		Tag:          c.Tag,
		Category:     c.Category,
		Operation:    c.Operation,
		EventId:      c.EventId,
		RuntimeId:    c.RuntimeId,
	})
	if err != nil {
		log.Println("log err " + err.Error())
	}
}

func Debug(ctx *gin.Context, tenantId, title, content string, Value any, c CommonForm) {

	err := Create(ctx, CreateForm{
		TenantId:     tenantId,
		Title:        title,
		Value:        Value,
		Content:      content,
		Level:        level.Debug,
		Operator:     c.Operator,
		Source:       c.Source,
		TriggerTitle: c.TriggerTitle,
		TriggerName:  c.TriggerName,
		Tag:          c.Tag,
		Category:     c.Category,
		Operation:    c.Operation,
		EventId:      c.EventId,
		RuntimeId:    c.RuntimeId,
	})
	if err != nil {
		log.Println("log err " + err.Error())
	}

}

func Trace(ctx *gin.Context, tenantId, title, content string, Value any, c CommonForm) {
	err := Create(ctx, CreateForm{
		TenantId:     tenantId,
		Title:        title,
		Value:        Value,
		Content:      content,
		Level:        level.Trace,
		Operator:     c.Operator,
		Source:       c.Source,
		TriggerTitle: c.TriggerTitle,
		TriggerName:  c.TriggerName,
		Tag:          c.Tag,
		Category:     c.Category,
		Operation:    c.Operation,
		EventId:      c.EventId,
		RuntimeId:    c.RuntimeId,
	})
	if err != nil {
		log.Println("log err " + err.Error())
	}
}

func Create(ctx *gin.Context, c CreateForm) (err error) {
	client := common.NewHttpClient()
	eventId := c.EventId
	if len(c.EventId) == 0 {
		eventId = ctx.GetHeader("Event-Id")
	}
	traceId := ctx.GetHeader("Trace-Id")
	err = ValidateStruct(c)
	if err != nil {
		log.Println("validate err " + err.Error())
	}
	if len(c.TenantId) == 0 {
		c.TenantId = ctx.GetHeader("App-Id")
	}
	c.EventId = eventId
	c.TraceId = traceId
	c.ReportTime = time.Now().Format(time.RFC3339)
	c.Service = os.Getenv("HOSTNAME")
	js, err := json.Marshal(c)
	if err != nil {
		err = errors.New("marshal err " + err.Error())
		return
	}
	res, code, err := client.PostJson(ctx, Host+"/log/create", js, map[string]string{})

	if err != nil {
		err = errors.New("log service request err " + err.Error())
		return
	}
	if code != 200 {
		err = errors.New(fmt.Sprintf("server err httpCode %v", code))
		return
	}
	var r Res[interface{}]
	err = json.Unmarshal(res, &r)
	if err != nil {
		err = errors.New("unmarshal res err " + err.Error())
		return
	}
	if r.ErrCode != 0 {
		err = errors.New("service err " + r.Msg)
		return
	}

	return
}
