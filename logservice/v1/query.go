package logservice

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/zklytech/sdk-go/common"
	"github.com/gin-gonic/gin"
)

type DataContent struct {
	Content         any      `json:"content"`
	Title           string   `json:"title"`
	CreateTime      string   `json:"create_time"`
	CreateTimeStamp int64    `json:"create_time_stamp"`
	EventID         string   `json:"event_id"`
	Level           string   `json:"level"`
	LogID           string   `json:"log_id"`
	Operator        string   `json:"operator"`
	ReportTime      string   `json:"report_time"`
	ReportTimeStamp int64    `json:"report_time_stamp"`
	TenantID        string   `json:"tenant_id"`
	TriggerTitle    string   `json:"trigger_title"`
	TriggerName     string   `json:"trigger_name"`
	TraceId         string   `json:"trace_id"`
	Tag             []string `json:"tag"`
	Service         string   `json:"service"`
	Category        string   `json:"category"`
	Operation       string   `json:"operation"`
	Value           any      `json:"value"`
}

type Data struct {
	Total int           `json:"total"`
	List  []DataContent `json:"list"`
}

type Pagination struct {
	Page     int64 `json:"page" form:"page"`
	PageSize int64 `json:"page_size" form:"page"`
}

type Sort struct {
	Field string `json:"field" form:"field"`
	Order string `json:"order" form:"order" validate:"in:asc,desc" message:"order is asc desc"`
}

type TimeRange struct {
	TimeField string `json:"time_field" form:"time_field" validate:"in:create_time,report_time" message:"time_field is create_time or report_time"`
	Start     string `json:"start" form:"start"`
	End       string `json:"end" form:"end"`
}

type QueryForm struct {
	Pagination Pagination             `json:"pagination"`
	Sort       Sort                   `json:"sort"`
	TimeRange  TimeRange              `json:"time_range"`
	Keyword    string                 `json:"keyword" form:"keyword"`
	TenantId   string                 `json:"tenant_id" form:"tenant_id" validate:"required" message:"tenant_id required"`
	Level      string                 `json:"level" form:"level"`
	Operator   string                 `json:"operator" form:"operator"`
	EventId    string                 `json:"event_id" form:"event_id"`
	Filter     map[string]interface{} `json:"filter" form:"filter"`
	Source     string                 `json:"source" form:"source"`

	TriggerTitle string   `json:"trigger_title" form:"trigger_title"`
	TriggerName  string   `json:"trigger_name" form:"trigger_name"`
	TraceId      string   `json:"trace_id" form:"trace_id"`
	Tag          []string `json:"tag" form:"tag"`
	Service      string   `json:"service" form:"service"`
	Category     string   `json:"category" form:"category"`
	Operation    string   `json:"operation" form:"operation"`
}

func Query(ctx *gin.Context, q QueryForm) (data Data, err error) {
	err = ValidateStruct(q)
	if err != nil {
		err = errors.New("log param err " + err.Error())
		return
	}
	js, err := json.Marshal(q)
	if err != nil {
		err = errors.New("marshal err " + err.Error())
		return
	}

	client := common.NewHttpClient()
	res, code, err := client.PostJson(ctx, Host+"/log/query", js, map[string]string{})

	if err != nil {
		err = errors.New("log service request err " + err.Error())
		return
	}
	if code != 200 {
		err = errors.New(fmt.Sprintf("server err httpCode %v", code))
		return
	}
	var r Res[Data]
	err = json.Unmarshal(res, &r)
	if err != nil {
		err = errors.New("unmarshal res err " + err.Error())
		return
	}
	if r.ErrCode != 0 {
		err = errors.New("service err " + r.Msg)
		return
	}
	data = r.Data
	return
}
