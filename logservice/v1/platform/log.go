package platform

import (
	"gitee.com/zklytech/sdk-go/logservice/v1"
	"gitee.com/zklytech/sdk-go/logservice/v1/level"
	"github.com/gin-gonic/gin"
	"log"
	"os"
	"time"
)

const Source = "platform"

type LogOption struct {
	Operator     string
	TriggerTitle string
	TriggerName  string
	Tag          []string
	Category     string
	Operation    string
}

func base(ctx *gin.Context, tenantId, title, content string, value any, level string, f LogOption) {
	err := logservice.Create(ctx, logservice.CreateForm{
		TenantId:     tenantId,
		Title:        title,
		Value:        value,
		Content:      content,
		Level:        level,
		Operator:     f.Operator,
		Source:       Source,
		ReportTime:   time.Now().Format(time.RFC3339),
		TriggerTitle: f.TriggerTitle,
		TriggerName:  f.TriggerName,
		Tag:          f.Tag,
		Category:     f.Category,
		Operation:    f.Operation,
		Service:      os.Getenv("HOSTNAME"),
	})
	if err != nil {
		log.Println("create log err " + err.Error())
	}

}

func Info(ctx *gin.Context, tenantId, title, content string, value any, f LogOption) {
	base(ctx, tenantId, title, content, value, level.Info, f)
}

func Trace(ctx *gin.Context, tenantId, title, content string, value any, f LogOption) {
	base(ctx, tenantId, title, content, value, level.Trace, f)
}

func Debug(ctx *gin.Context, tenantId, title, content string, value any, f LogOption) {
	base(ctx, tenantId, title, content, value, level.Debug, f)
}

func Error(ctx *gin.Context, tenantId, title, content string, value any, f LogOption) {
	base(ctx, tenantId, title, content, value, level.Error, f)
}

func Fatal(ctx *gin.Context, tenantId, title, content string, value any, f LogOption) {
	base(ctx, tenantId, title, content, value, level.Fatal, f)
}

func Warn(ctx *gin.Context, tenantId, title, content string, value any, f LogOption) {
	base(ctx, tenantId, title, content, value, level.Warn, f)
}

type QueryForm struct {
	Pagination   logservice.Pagination  `json:"pagination"`
	Sort         logservice.Sort        `json:"sort"`
	TimeRange    logservice.TimeRange   `json:"time_range"`
	Keyword      string                 `json:"keyword" form:"keyword"`
	TenantId     string                 `json:"tenant_id" form:"tenant_id" validate:"required" message:"tenant_id required"`
	Level        string                 `json:"level" form:"level"`
	Operator     string                 `json:"operator" form:"operator"`
	EventId      string                 `json:"event_id" form:"event_id"`
	Filter       map[string]interface{} `json:"filter" form:"fileter"`
	Source       string                 `json:"source" form:"source"`
	TriggerTitle string                 `json:"trigger_title" form:"trigger_title"`
	TriggerName  string                 `json:"trigger_name" form:"trigger_name"`
	TraceId      string                 `json:"trace_id" form:"trace_id"`
	Tag          []string               `json:"tag" form:"tag"`
	Service      string                 `json:"service" form:"service"`
	Category     string                 `json:"category" form:"category"`
	Operation    string                 `json:"operation" form:"operation"`
}

func Query(ctx *gin.Context, q QueryForm) (err error, data logservice.Data) {
	data, err = logservice.Query(ctx, logservice.QueryForm{
		Pagination:   q.Pagination,
		Sort:         q.Sort,
		TimeRange:    q.TimeRange,
		Keyword:      q.Keyword,
		TenantId:     q.TenantId,
		Level:        q.Level,
		Operator:     q.Operator,
		EventId:      q.EventId,
		Filter:       q.Filter,
		Source:       Source,
		TriggerTitle: q.TriggerTitle,
		TriggerName:  q.TriggerName,
		TraceId:      q.TraceId,
		Tag:          q.Tag,
		Service:      q.Service,
		Category:     q.Category,
		Operation:    q.Operation,
	})

	return

}
