package level

const Info string = "info"
const Fatal string = "fatal"
const Error string = "error"
const Warn string = "warn"
const Debug string = "debug"
const Trace string = "trace"
