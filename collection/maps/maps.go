package maps

import (
	"strconv"
)

func Keys[K comparable, V any](m map[K]V) []K {
	var result []K
	for k := range m {
		result = append(result, k)
	}
	return result
}

func Values[K comparable, V any](m map[K]V) []V {
	var result []V
	for _, v := range m {
		result = append(result, v)
	}
	return result
}

// FlattenMap 只能对json序列化来map[string]any类型进行平铺
// 解析过后的map m可以使用  a.b.c的方式获取最终的基础值,如果字段a是数组 a.1.c 的方式获取a字段下标为1的数组的c字段
// 对于非基础字段，直接访问原本的 有 a.b.c这样的key 则a.b 获取到b对应的map[string]any
func FlattenMap(obj map[string]any) map[string]any {
	r := make(map[string]interface{})
	for k, v := range obj {
		flattenAndInsert(r, k, v)
	}

	return r
}
func flattenAndInsert(result map[string]interface{}, prefix string, value interface{}) {
	//确保原本的key可以正常访问
	result[prefix] = value
	switch tv := value.(type) {
	case map[string]interface{}:
		for k, v := range tv {
			flattenAndInsert(result, joinKeys(prefix, k), v)
		}
	case []interface{}:
		//以下标为key继续遍历
		for index, element := range tv {
			arrayKey := strconv.Itoa(index)
			flattenAndInsert(result, joinKeys(prefix, arrayKey), element)
		}
	default:
	}
}
func joinKeys(prefix string, key string) string {
	if prefix == "" {
		return key
	}
	return prefix + "." + key
}
