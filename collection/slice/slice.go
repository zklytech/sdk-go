package slice

import "sort"

// 数组过滤
func Filter[T any](sli []T, test func(T, int) bool) []T {
	var result = make([]T, 0, len(sli))

	for idx, e := range sli {
		if test(e, idx) {
			result = append(result, e)
		}
	}

	return result
}

// 数组映射
func MapTo[O any, R any](sli []O, mapFunc func(O, int) R) []R {
	var result = make([]R, 0, len(sli))

	for idx, e := range sli {
		result = append(result, mapFunc(e, idx))
	}

	return result
}

// 数组任意匹配
func AnyMatch[T any](sli []T, test func(T, int) bool) bool {
	for idx, e := range sli {
		if test(e, idx) {
			return true
		}
	}
	return false
}

// 数组转Map
func ToMap[E any, K comparable, V any](sli []E, keyFun func(E) K, valFun func(E) V) map[K]V {
	var m = make(map[K]V)
	for _, e := range sli {
		m[keyFun(e)] = valFun(e)
	}
	return m
}

// 数组降维
func Reduce[E any](sli []E, accumulatorm func(E, E) E) E {
	var result E = sli[0]
	for i := 1; i < len(sli); i++ {
		result = accumulatorm(result, sli[i])
	}

	return result
}

// 遍历
func ForEach[E any](sli []E, consumer func(E, int)) {
	for idx, e := range sli {
		consumer(e, idx)
	}
}

// 快排
func Sort[E any](sli *[]E, comparer func(*E, *E) int) {
	sort.Slice(*sli, func(i, j int) bool {
		return comparer(&(*sli)[i], &(*sli)[j]) < 0
	})
	//qs[E](sli, comparer, 0, len(*sli)-1)
}

func qs[E any](sli *[]E, comparer func(*E, *E) int, start int, end int) {
	if start >= end {
		return
	}
	var l = start
	var r = end
	cen := (*sli)[l]
	for l < r {
		for comparer(&(*sli)[r], &cen) >= 0 && l < r {
			r--
		}
		if l < r {
			swap[E](sli, l, r)
		}
		for comparer(&(*sli)[l], &cen) <= 0 && l < r {
			l++
		}
		if l < r {
			swap[E](sli, l, r)
		}
	}

	qs[E](sli, comparer, l+1, end)
	qs[E](sli, comparer, start, l-1)
}

func swap[E any](sli *[]E, i int, j int) {
	tmp := (*sli)[i]
	(*sli)[i] = (*sli)[j]
	(*sli)[j] = tmp
}

func Intersection[T any](sli1 []T, sli2 []T, eq func(a, b T) bool) []T {
	result := []T{}
	for _, a := range sli1 {
		for _, b := range sli2 {
			if eq(a, b) && !AnyMatch[T](result, func(t T, i int) bool {
				return eq(t, a)
			}) {
				result = append(result, a)
				break
			}
		}
	}
	return result
}

func Diff[T any](sli1 []T, sli2 []T, eq func(a, b T) bool) []T {
	result := []T{}

	for _, a := range sli1 {
		if AnyMatch[T](sli2, func(t T, i int) bool {
			return eq(t, a)
		}) {
			continue
		} else {
			result = append(result, a)
		}
	}

	for _, b := range sli2 {
		if AnyMatch[T](sli1, func(t T, i int) bool {
			return eq(t, b)
		}) {
			continue
		} else {
			result = append(result, b)
		}
	}

	return result
}

func Sum[T any](sli1 []T, sli2 []T, eq func(a, b T) bool) []T {
	result := []T{}

	for _, a := range sli1 {
		if AnyMatch[T](result, func(t T, i int) bool {
			return eq(t, a)
		}) {
			continue
		} else {
			result = append(result, a)
		}
	}

	for _, a := range sli2 {
		if AnyMatch[T](result, func(t T, i int) bool {
			return eq(t, a)
		}) {
			continue
		} else {
			result = append(result, a)
		}
	}

	return result
}
