package app_manager

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"

	"gitee.com/zklytech/sdk-go/common"
	"github.com/gin-gonic/gin"
)

const base = "http://appmanager.infra:8090"

var client *common.HttpClient

func init() {
	client = common.NewHttpClient()
}

func QueryUserApp(ctx *gin.Context, userId string) (appIds []string, err error) {

	params := url.Values{}
	params.Add("user_id", userId)

	res, status, err := client.Get(ctx, base+"/api/app/user/query", params)
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = errors.New("请求错误，status code:" + strconv.Itoa(status))
		return
	}
	var j common.Result[[]string]
	json.Unmarshal(res, &j)
	if j.ErrCode != 0 {
		err = errors.New(j.Msg)
		return
	}

	appIds = j.Data
	return
}
