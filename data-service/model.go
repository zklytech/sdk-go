package data_service

import (
	"errors"

	"github.com/gin-gonic/gin"
)

type ModelInstance struct {
	AppId             string
	SubAppId          string
	ModuleId          string
	InstanceId        string
	ModelDbInstanceId string
	Collections       map[string]ModelCollection
}

type ModelCollection struct {
	AppId          string
	SubAppId       string
	ModuleId       string
	InstanceId     string
	CollectionId   string
	CollectionName string
	FieldList      []Field
	FuncList       []ModelFunc
	EventList      []ModelEvent
}

func GetModelInstance(ctx *gin.Context, appId, subAppId, moduleId string, instanceId string) (model *ModelInstance, err error) {
	modelDetail, err := GetModelInstanceDetail(ctx, appId, subAppId, moduleId, instanceId)
	if err != nil {
		return
	}

	model = &ModelInstance{
		AppId:             appId,
		SubAppId:          subAppId,
		ModuleId:          moduleId,
		InstanceId:        instanceId,
		ModelDbInstanceId: modelDetail.ModelDbInstanceId,
	}

	colList := map[string]ModelCollection{}
	for _, c := range modelDetail.CollectionList {
		colList[c.CollectionName] = ModelCollection{
			AppId:          appId,
			SubAppId:       subAppId,
			ModuleId:       moduleId,
			InstanceId:     instanceId,
			CollectionId:   c.CollectionID,
			CollectionName: c.CollectionName,
			FieldList:      c.FieldList,
			FuncList:       c.FuncList,
			EventList:      c.EventList,
		}
	}
	model.Collections = colList

	return
}

func (m *ModelInstance) Collection(name string) (col *ModelCollection, err error) {
	c, ex := m.Collections[name]
	if !ex {
		err = errors.New("集合不存在")
		return
	}
	col = &c
	return
}

func (m *ModelInstance) CollectionById(colId string) (col *ModelCollection, err error) {
	for _, c := range m.Collections {
		if c.CollectionId == colId {
			col = &c
			return
		}
	}
	err = errors.New("集合不存在")
	return
}

func (m *ModelInstance) DeleteInstance(ctx *gin.Context) (err error) {

	err = DeleteModelInstance(ctx, m.AppId, m.SubAppId, m.ModuleId, m.InstanceId)

	return
}

func (m *ModelInstance) CallMethod(ctx *gin.Context, method string, params map[string]any) (result any, err error) {
	result, err = Process[any](ctx, ProcessReq{
		AppId:        m.AppId,
		InstanceId:   m.InstanceId,
		FuncName:     method,
		ProcessLevel: "S0",
		Params:       params,
	})
	return
}

func (c *ModelCollection) UpdateCollection(ctx *gin.Context, req UpdateCollectionReq) (err error) {
	err = UpdateCollectionInfo(ctx, c.AppId, c.SubAppId, c.ModuleId, c.CollectionId, req)
	return
}

func (c *ModelCollection) Insert(ctx *gin.Context, data []any, txId string) (ids []string, err error) {
	ids, err = InsertData[any](ctx, InsertReq[any]{
		AppId:         c.AppId,
		SubAppId:      c.SubAppId,
		ModuleId:      c.ModuleId,
		InstanceId:    c.InstanceId,
		CollectionId:  c.CollectionId,
		FuncName:      "insert",
		ProcessLevel:  "S0",
		Values:        data,
		TransactionId: txId,
	})
	return
}

func (c *ModelCollection) Update(ctx *gin.Context, set map[string]any, query *QueryWrapper, txId string) (err error) {
	err = UpdateData(ctx, UpdateReq{
		AppId:        c.AppId,
		SubAppId:     c.SubAppId,
		ModuleId:     c.ModuleId,
		InstanceId:   c.InstanceId,
		CollectionId: c.CollectionId,
		FuncName:     "update",
		ProcessLevel: "S0",
		Set:          set,
		Config: QueryConfig{
			Condition: query.Filter,
			Limit:     query.Limit,
			Page:      query.Page,
			Order:     query.Order,
		},
		TransactionId: txId,
	})
	return
}

func (c *ModelCollection) Delete(ctx *gin.Context, query *QueryWrapper, txId string) (result int, err error) {
	result, err = DeleteData(ctx, DeleteReq{
		AppId:        c.AppId,
		SubAppId:     c.SubAppId,
		ModuleId:     c.ModuleId,
		InstanceId:   c.InstanceId,
		CollectionId: c.CollectionId,
		FuncName:     "delete",
		ProcessLevel: "S0",
		Config: QueryConfig{
			Condition: query.Filter,
			Limit:     query.Limit,
			Page:      query.Page,
			Order:     query.Order,
		},
		TransactionId: txId,
	})
	return
}

func (c *ModelCollection) Query(ctx *gin.Context, query *QueryWrapper) (result QueryResult, err error) {
	result, err = QueryData(ctx, QueryReq{
		AppId:        c.AppId,
		SubAppId:     c.SubAppId,
		ModuleId:     c.ModuleId,
		InstanceId:   c.InstanceId,
		CollectionId: c.CollectionId,
		FuncName:     "query",
		ProcessLevel: "S0",
		Config: QueryConfig{
			Condition: query.Filter,
			Limit:     query.Limit,
			Page:      query.Page,
			Order:     query.Order,
		},
	})
	return
}

func (c *ModelCollection) CallMethod(ctx *gin.Context, method string, params any) (result any, err error) {
	result, err = Process[any](ctx, ProcessReq{
		AppId:        c.AppId,
		SubAppId:     c.SubAppId,
		ModuleId:     c.ModuleId,
		InstanceId:   c.InstanceId,
		CollectionId: c.CollectionId,
		FuncName:     method,
		ProcessLevel: "S0",
		Params:       params,
	})
	return
}

func (c *ModelCollection) Count(ctx *gin.Context, query *QueryWrapper) (result int64, err error) {
	result, err = Process[int64](ctx, QueryReq{
		AppId:        c.AppId,
		SubAppId:     c.SubAppId,
		ModuleId:     c.ModuleId,
		InstanceId:   c.InstanceId,
		CollectionId: c.CollectionId,
		FuncName:     "count",
		ProcessLevel: "S0",
		Config: QueryConfig{
			Condition: query.Filter,
		},
	})
	return
}
