package data_service

import "time"

type Result[T any] struct {
	ErrCode int    `json:"errcode"`
	Msg     string `json:"msg"`
	Data    T      `json:"data"`
}

type ProcessReq struct {
	AppId         string `json:"app_id"`
	SubAppId      string `json:"sub_app_id"`
	ModuleId      string `json:"module_id"`
	InstanceId    string `json:"instance_id"`
	CollectionId  string `json:"collection_id"`
	FuncName      string `json:"func_name"`
	ProcessLevel  string `json:"process_level"`
	Params        any    `json:"params"`
	TransactionId string `json:"transaction_id"`
}

type InsertReq[T any] struct {
	AppId         string         `json:"app_id"`
	SubAppId      string         `json:"sub_app_id"`
	ModuleId      string         `json:"module_id"`
	InstanceId    string         `json:"instance_id"`
	CollectionId  string         `json:"collection_id"`
	FuncName      string         `json:"func_name"`
	ProcessLevel  string         `json:"process_level"`
	Params        map[string]any `json:"params"`
	Values        []T            `json:"values"`
	TransactionId string         `json:"transaction_id"`
}

type UpdateReq struct {
	AppId         string         `json:"app_id"`
	SubAppId      string         `json:"sub_app_id"`
	ModuleId      string         `json:"module_id"`
	InstanceId    string         `json:"instance_id"`
	CollectionId  string         `json:"collection_id"`
	FuncName      string         `json:"func_name"`
	ProcessLevel  string         `json:"process_level"`
	Params        map[string]any `json:"params"`
	Set           map[string]any `json:"set"`
	Config        QueryConfig    `json:"config"`
	TransactionId string         `json:"transaction_id"`
}

type DeleteReq struct {
	AppId         string         `json:"app_id"`
	SubAppId      string         `json:"sub_app_id"`
	ModuleId      string         `json:"module_id"`
	InstanceId    string         `json:"instance_id"`
	CollectionId  string         `json:"collection_id"`
	FuncName      string         `json:"func_name"`
	ProcessLevel  string         `json:"process_level"`
	Params        map[string]any `json:"params"`
	Config        QueryConfig    `json:"config"`
	TransactionId string         `json:"transaction_id"`
}

type QueryReq struct {
	AppId        string         `json:"app_id"`
	SubAppId     string         `json:"sub_app_id"`
	ModuleId     string         `json:"module_id"`
	InstanceId   string         `json:"instance_id"`
	CollectionId string         `json:"collection_id"`
	FuncName     string         `json:"func_name"`
	ProcessLevel string         `json:"process_level"`
	Params       map[string]any `json:"params"`
	Config       QueryConfig    `json:"config"`
}

type QueryConfig struct {
	Condition      []QueryCondition `json:"condition"`
	Limit          int              `json:"limit"`
	Page           int              `json:"page"`
	Order          QueryOrder       `json:"order"`
	Select         []string         `json:"selectFields"`
	SecondaryOrder []QueryOrder     `json:"secondaryOrder"`
}

type QueryCondition struct {
	Relation string           `json:"relation"`
	IsGroup  bool             `json:"is_group"`
	Cond     Condition        `json:"condition"`
	List     []QueryCondition `json:"list"`
}

type Condition struct {
	FieldId     string `json:"fieldId"`
	FieldName   string `json:"fieldIdName"`
	FieldAttr   string `json:"fieldAttribute"`
	ValueType   string `json:"fieldValueType"`
	Symbol      string `json:"conditionSymbol"`
	SettingType string `json:"settingType"`
	Values      any    `json:"values"`
	VarInfo     struct {
		Pid       string `json:"pid"`
		Id        string `json:"id"`
		Name      string `json:"name"`
		Title     string `json:"title"`
		RuleKey   string `json:"rule_key"`
		Rule      string `json:"rule"`
		ValueType string `json:"value_type"`
		Type      string `json:"type"`
		Scope     string `json:"scope"`
	} `json:"varInfo"`
}

type QueryOrder struct {
	FieldId string `json:"fieldId"`
	Sort    string `json:"sort"`
}

type QueryResult struct {
	List  []map[string]any `json:"list"`
	Total int              `json:"total"`
}

type ModelInstanceDetail struct {
	ModelID           string             `json:"model_id"`
	Describe          string             `json:"describe"`
	InstanceID        string             `json:"instance_id"`
	InstanceName      string             `json:"instance_name"`
	VersionID         string             `json:"version_id"`
	VersionNumber     int                `json:"version_number"`
	CollectionList    []CollectionDetail `json:"collection_list"`
	ModelEvent        []ModelEvent       `json:"model_event"`
	ModelFunc         []ModelFunc        `json:"model_func"`
	ModelDbInstanceId string             `json:"model_db_instance_id"`
}

type CollectionDetail struct {
	ID              int          `json:"id"`
	CreateTime      time.Time    `json:"create_time"`
	UpdateTime      time.Time    `json:"update_time"`
	InstanceID      string       `json:"instance_id"`
	VersionID       string       `json:"version_id"`
	CollectionID    string       `json:"collection_id"`
	CollectionName  string       `json:"collection_name"`
	CollectionTitle string       `json:"collection_title"`
	Describe        string       `json:"describe"`
	SchemaName      string       `json:"schema_name"`
	Widgets         any          `json:"widgets"`
	FieldList       []Field      `json:"field_list"`
	EventList       []ModelEvent `json:"event_list"`
	FuncList        []ModelFunc  `json:"func_list"`
}

type ModelEvent struct {
	EventID    string `json:"event_id"`
	EventName  string `json:"event_name"`
	EventTitle string `json:"event_title"`
	InstanceID string `json:"instance_id"`
	VersionID  string `json:"version_id"`
}

type ModelFunc struct {
	VersionID  string `json:"version_id"`
	InstanceID string `json:"instance_id"`
	FuncID     string `json:"func_id"`
	FuncName   string `json:"func_name"`
	FuncTitle  string `json:"func_title"`
	Type       string `json:"type"`
	Namespace  string `json:"namespace"`
}

type Field struct {
	ID                 int       `json:"id"`
	CreateTime         time.Time `json:"create_time"`
	UpdateTime         time.Time `json:"update_time"`
	CollectionID       string    `json:"collection_id"`
	FieldID            string    `json:"field_id"`
	FieldName          string    `json:"field_name"`
	FieldType          string    `json:"field_type"`
	WidgetInfo         any       `json:"widget_info"`
	Level              string    `json:"level"`
	Describe           string    `json:"describe"`
	ValidatorType      string    `json:"validator_type"`
	ValidatorRegexp    string    `json:"validator_regexp"`
	ValidatorFuncName  string    `json:"validator_func_name"`
	ValidatorNamespace string    `json:"validator_namespace"`
	ValidatorMessage   string    `json:"validator_message"`
}

type UpdateCollectionReq struct {
	CollectionTitle string `json:"collection_title"`
	Describe        string `json:"describe"`
}

type UndoLog struct {
	AppId        string `json:"app_id"`
	SubAppId     string `json:"sub_app_id"`
	ModuleId     string `json:"module_id"`
	TxId         string `json:"tx_id"`
	Operation    string `json:"operation"`
	BeforeData   string `json:"before_data"`
	AfterData    string `json:"after_data"`
	InstanceId   string `json:"instance_id"`
	CollectionId string `json:"collection_id"`
}

type CreateInstanceResp struct {
	InstanceId     string                         `json:"biz_data_model_instance_id"`
	CollectionList []CreateInstanceCollectionResp `json:"biz_data_model_instance"`
}

type CreateInstanceCollectionResp struct {
	CollectionId    string `json:"data_model_id"`
	CollectionTitle string `json:"data_model_title"`
	CollectionName  string `json:"data_model_name"`
	CollectionType  string `json:"data_model_type"`
	ToPath          string `json:"to_path"`
}
