package data_service

import (
	"errors"
	"strings"

	"gitee.com/zklytech/sdk-go/collection/slice"
	authorization "gitee.com/zklytech/sdk-go/middleware"
	"gitee.com/zklytech/sdk-go/runtime/v1/runtime-sys/variable"
	"gitee.com/zklytech/sdk-go/tool"
	"github.com/gin-gonic/gin"
)

func FilterJudge(ctx *gin.Context, data map[string]any, conds []QueryCondition, params map[string]any, userInfo authorization.UserInfo) bool {

	var result = true
	for _, cond := range conds {
		if cond.IsGroup {
			if cond.Relation == "and" {
				result = result && FilterJudge(ctx, data, cond.List, params, userInfo)
			} else {
				result = result || FilterJudge(ctx, data, cond.List, params, userInfo)
			}
		} else {
			if cond.Relation == "and" {
				result = result && conditionJudge(ctx, data, cond.Cond, params, userInfo)
			} else {
				result = result || conditionJudge(ctx, data, cond.Cond, params, userInfo)
			}
		}
	}

	return result
}

func conditionJudge(ctx *gin.Context, data map[string]any, cond Condition, params map[string]any, userInfo authorization.UserInfo) bool {

	var leftValue any
	var rightValue = getRightValue(ctx, cond, params, userInfo)

	if cond.ValueType == "pointer" {
		leftValue = data[cond.FieldName].([]any)[0].(map[string]any)["id"]
	} else {
		leftValue = data[cond.FieldName]
	}

	switch cond.Symbol {
	case "eq":
		return leftValue == rightValue
	case "ne":
		return leftValue != rightValue
	case "gt":
		return leftValue.(float64) > rightValue.(float64)
	case "lt":
		return leftValue.(float64) < rightValue.(float64)
	case "ge":
		return leftValue.(float64) >= rightValue.(float64)
	case "le":
		return leftValue.(float64) <= rightValue.(float64)
	case "in":
		if cond.ValueType == "string" {
			return strings.Contains(rightValue.(string), leftValue.(string))
		} else {
			return slice.AnyMatch[any](rightValue.([]any), func(a any, i int) bool {
				return a == leftValue
			})
		}
	case "nin":
		return !slice.AnyMatch[any](rightValue.([]any), func(a any, i int) bool {
			return a == leftValue
		})
	case "null":
		_, ok := data[cond.FieldId]
		return !ok
	case "nnull":
		_, ok := data[cond.FieldId]
		return ok
	default:
		return false
	}
}

func getRightValue(ctx *gin.Context, cond Condition, params map[string]any, userInfo authorization.UserInfo) any {
	var value = cond.Values
	var settingType = cond.SettingType

	var val any

	if settingType == "relation" {
		val = params[value.(string)]
	} else if settingType == "var" {
		val, _ = getVarValue(ctx, cond, userInfo, params)
	} else {
		val = value
	}
	if cond.ValueType == "pointer" {
		va, ok := val.([]any)
		if !ok {
			return val
		}
		if cond.Symbol == "in" || cond.Symbol == "nin" {
			ids := []any{}
			for _, v := range va {
				ids = append(ids, v.(map[string]any)["id"])
			}
			val = ids
			return val
		} else {
			val = va[0].(map[string]any)["id"]
			return val
		}
	}
	return val
}

func getVarValue(ctx *gin.Context, cond Condition, userInfo authorization.UserInfo, params map[string]any) (val any, err error) {

	if cond.VarInfo.Scope == "page" || cond.VarInfo.Scope == "app" {
		v, ex := params[cond.VarInfo.Id]
		if !ex {
			err = errors.New("变量不存在")
			return
		}
		val = v
	} else {
		key, e := tool.ReplaceVariables(cond.VarInfo.Rule, userInfo.ID)
		if e != nil {
			err = e
			return
		}

		val, err = variable.Get(ctx, variable.GetForm{
			Key:   key,
			Scope: "service",
		}, map[string]string{})
	}

	return
}
