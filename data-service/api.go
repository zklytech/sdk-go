package data_service

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"

	"gitee.com/zklytech/sdk-go/common"
	"github.com/gin-gonic/gin"
)

const baseUrl = "http://data-service.infra:8088/api"

var client *common.HttpClient

func init() {
	client = common.NewHttpClient()
}

func GetModelInstanceByType(ctx *gin.Context, appId, subAppId, moduleId string, modelType string) (result []map[string]string, err error) {
	u := baseUrl + "/service/model/type"
	params := url.Values{}
	params.Add("app_id", appId)
	params.Add("model_type", modelType)
	params.Add("sub_app_id", subAppId)
	params.Add("module_id", moduleId)

	resp, status, err := client.Get(ctx, u, params)
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = errors.New("网络请求错误：" + strconv.Itoa(status))
		return
	}

	var j Result[[]map[string]string]
	json.Unmarshal(resp, &j)
	if j.ErrCode != 0 {
		err = errors.New(j.Msg)
		return
	}

	result = j.Data
	return
}

func GetModelInstanceDetail(ctx *gin.Context, appId, subAppId, moduleId string, instanceId string) (result ModelInstanceDetail, err error) {

	var u = baseUrl + "/model/instance/detail"
	params := url.Values{}
	params.Add("instance_id", instanceId)
	params.Add("app_id", appId)
	params.Add("sub_app_id", subAppId)
	params.Add("module_id", moduleId)
	resp, status, err := client.Get(ctx, u, params)
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = errors.New("网络请求错误：" + strconv.Itoa(status))
		return
	}

	var j Result[ModelInstanceDetail]
	json.Unmarshal(resp, &j)
	if j.ErrCode != 0 {
		err = errors.New(j.Msg)
		return
	}

	result = j.Data

	return
}

func DeleteModelInstance(ctx *gin.Context, appId, subAppId, moduleId, instanceId string) (err error) {
	var u = baseUrl + "/model/instance/delete"
	params := url.Values{}
	params.Add("app_id", appId)
	params.Add("sub_app_id", subAppId)
	params.Add("module_id", moduleId)
	params.Add("instance_id", instanceId)
	res, status, err := client.Delete(ctx, u, params)
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = errors.New("status code:" + strconv.Itoa(status))
		return
	}
	var j Result[any]
	json.Unmarshal(res, &j)
	if j.ErrCode != 0 {
		err = errors.New(j.Msg)
		return
	}

	return
}

func CopyModelInstance(ctx *gin.Context, appId, subAppId, moduleId, instanceId string) (result CreateInstanceResp, err error) {
	var u = baseUrl + "/model/instance/copy"
	params := map[string]any{
		"app_id":      appId,
		"sub_app_id":  subAppId,
		"module_id":   moduleId,
		"instance_id": instanceId,
	}
	bdata, _ := json.Marshal(params)
	res, status, err := client.PostJson(ctx, u, bdata, map[string]string{})
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = errors.New("status code:" + strconv.Itoa(status))
		return
	}
	var j Result[CreateInstanceResp]
	json.Unmarshal(res, &j)
	if j.ErrCode != 0 {
		err = errors.New(j.Msg)
		return
	}
	result = j.Data

	return
}

func GetCollectionDetail(ctx *gin.Context, appId, subAppId, moduleId, collectionId string) (result CollectionDetail, err error) {
	var u = baseUrl + "/model/collection/detail"
	params := url.Values{}
	params.Add("collection_id", collectionId)
	params.Add("app_id", appId)
	params.Add("sub_app_id", subAppId)
	params.Add("module_id", moduleId)
	resp, status, err := client.Get(ctx, u, params)
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = errors.New("网络请求错误：" + strconv.Itoa(status))
		return
	}

	var j Result[CollectionDetail]
	json.Unmarshal(resp, &j)
	if j.ErrCode != 0 {
		err = errors.New(j.Msg)
		return
	}

	result = j.Data
	return
}

func UpdateCollectionInfo(ctx *gin.Context, appId, subAppId, moduleId, collectionId string, req UpdateCollectionReq) (err error) {
	var u = baseUrl + "/model/instance/collection/update"
	params := map[string]any{
		"app_id":           appId,
		"sub_app_id":       subAppId,
		"module_id":        moduleId,
		"collection_id":    collectionId,
		"collection_title": req.CollectionTitle,
		"describe":         req.Describe,
	}
	bdata, _ := json.Marshal(params)
	resp, status, err := client.PostJson(ctx, u, bdata, map[string]string{})
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = errors.New("status code:" + strconv.Itoa(status))
		return
	}

	var j Result[any]
	json.Unmarshal(resp, &j)
	if j.ErrCode != 0 {
		err = errors.New(j.Msg)
		return
	}

	return
}

func Process[R any](ctx *gin.Context, params any) (result R, err error) {
	var url = baseUrl + "/v2/model/process"
	bdata, _ := json.Marshal(params)
	resp, status, err := client.PostJson(ctx, url, bdata, map[string]string{})
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = errors.New("网络请求错误：" + strconv.Itoa(status))
		return
	}

	var j Result[R]
	json.Unmarshal(resp, &j)

	if j.ErrCode != 0 {
		err = errors.New(j.Msg)
		return
	}
	result = j.Data

	return
}

func InsertData[T any](ctx *gin.Context, req InsertReq[T]) (result []string, err error) {
	result, err = Process[[]string](ctx, req)
	return
}

func UpdateData(ctx *gin.Context, req UpdateReq) (err error) {
	_, err = Process[any](ctx, req)
	return
}

func DeleteData(ctx *gin.Context, req DeleteReq) (count int, err error) {
	count, err = Process[int](ctx, req)
	return
}

func QueryData(ctx *gin.Context, req QueryReq) (result QueryResult, err error) {
	result, err = Process[QueryResult](ctx, req)
	return
}

func InsertUndoLog(ctx *gin.Context, log UndoLog) (err error) {
	var u = baseUrl + "/model/transaction/log/insert"
	bdata, _ := json.Marshal(log)
	res, status, err := client.PostJson(ctx, u, bdata, map[string]string{})
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = errors.New("网络请求错误：" + strconv.Itoa(status))
		return
	}

	var j Result[any]
	json.Unmarshal(res, &j)
	if j.ErrCode != 0 {
		err = errors.New(j.Msg)
		return
	}

	return
}
