package data_service

type QueryWrapper struct {
	Order  QueryOrder
	Filter []QueryCondition
	Limit  int
	Page   int
}

func NewQueryWrapper() *QueryWrapper {
	return &QueryWrapper{
		Filter: []QueryCondition{},
	}
}

func (q *QueryWrapper) Eq(relation, field string, val any, valueType string) *QueryWrapper {
	q.Filter = append(q.Filter, QueryCondition{
		Relation: relation,
		IsGroup:  false,
		Cond: Condition{
			FieldId:     field,
			FieldName:   field,
			FieldAttr:   "value",
			ValueType:   valueType,
			Symbol:      "eq",
			SettingType: "fixed",
			Values:      val,
		},
	})
	return q
}

func (q *QueryWrapper) Ne(relation, field string, val any, valueType string) *QueryWrapper {
	q.Filter = append(q.Filter, QueryCondition{
		Relation: relation,
		IsGroup:  false,
		Cond: Condition{
			FieldId:     field,
			FieldName:   field,
			FieldAttr:   "value",
			ValueType:   valueType,
			Symbol:      "ne",
			SettingType: "fixed",
			Values:      val,
		},
	})
	return q
}
func (q *QueryWrapper) Gt(relation, field string, val any) *QueryWrapper {
	q.Filter = append(q.Filter, QueryCondition{
		Relation: relation,
		IsGroup:  false,
		Cond: Condition{
			FieldId:     field,
			FieldName:   field,
			FieldAttr:   "value",
			ValueType:   "number",
			Symbol:      "gt",
			SettingType: "fixed",
			Values:      val,
		},
	})
	return q
}

func (q *QueryWrapper) Ge(relation, field string, val any) *QueryWrapper {
	q.Filter = append(q.Filter, QueryCondition{
		Relation: relation,
		IsGroup:  false,
		Cond: Condition{
			FieldId:     field,
			FieldName:   field,
			FieldAttr:   "value",
			ValueType:   "number",
			Symbol:      "ge",
			SettingType: "fixed",
			Values:      val,
		},
	})
	return q
}

func (q *QueryWrapper) Lt(relation, field string, val any) *QueryWrapper {
	q.Filter = append(q.Filter, QueryCondition{
		Relation: relation,
		IsGroup:  false,
		Cond: Condition{
			FieldId:     field,
			FieldName:   field,
			FieldAttr:   "value",
			ValueType:   "number",
			Symbol:      "lt",
			SettingType: "fixed",
			Values:      val,
		},
	})
	return q
}

func (q *QueryWrapper) Le(relation, field string, val any) *QueryWrapper {
	q.Filter = append(q.Filter, QueryCondition{
		Relation: relation,
		IsGroup:  false,
		Cond: Condition{
			FieldId:     field,
			FieldName:   field,
			FieldAttr:   "value",
			ValueType:   "number",
			Symbol:      "le",
			SettingType: "fixed",
			Values:      val,
		},
	})
	return q
}

func (q *QueryWrapper) In(relation, field string, val any) *QueryWrapper {
	q.Filter = append(q.Filter, QueryCondition{
		Relation: relation,
		IsGroup:  false,
		Cond: Condition{
			FieldId:     field,
			FieldName:   field,
			FieldAttr:   "value",
			ValueType:   "array",
			Symbol:      "in",
			SettingType: "fixed",
			Values:      val,
		},
	})
	return q
}

func (q *QueryWrapper) Nin(relation, field string, val any) *QueryWrapper {
	q.Filter = append(q.Filter, QueryCondition{
		Relation: relation,
		IsGroup:  false,
		Cond: Condition{
			FieldId:     field,
			FieldName:   field,
			FieldAttr:   "value",
			ValueType:   "array",
			Symbol:      "nin",
			SettingType: "fixed",
			Values:      val,
		},
	})
	return q
}

func (q *QueryWrapper) NotNull(relation, field string) *QueryWrapper {
	q.Filter = append(q.Filter, QueryCondition{
		Relation: relation,
		IsGroup:  false,
		Cond: Condition{
			FieldId:     field,
			FieldName:   field,
			FieldAttr:   "value",
			ValueType:   "string",
			Symbol:      "nnull",
			SettingType: "",
			Values:      "",
		},
	})
	return q
}

func (q *QueryWrapper) IsNull(relation, field string) *QueryWrapper {
	q.Filter = append(q.Filter, QueryCondition{
		Relation: relation,
		IsGroup:  false,
		Cond: Condition{
			FieldId:     field,
			FieldName:   field,
			FieldAttr:   "value",
			ValueType:   "string",
			Symbol:      "null",
			SettingType: "",
			Values:      "",
		},
	})
	return q
}

func (q *QueryWrapper) OrGroup(g *QueryWrapper) *QueryWrapper {
	q.Filter = append(q.Filter, QueryCondition{
		Relation: "or",
		IsGroup:  true,
		List:     q.Filter,
	})
	return q
}

func (q *QueryWrapper) AndGroup() *QueryWrapper {
	q.Filter = append(q.Filter, QueryCondition{
		Relation: "and",
		IsGroup:  true,
		List:     q.Filter,
	})
	return q
}

func (q *QueryWrapper) Lmt(val int) *QueryWrapper {
	q.Limit = val
	return q
}

func (q *QueryWrapper) Pg(val int) *QueryWrapper {
	q.Page = val
	return q
}

func (q *QueryWrapper) OrderByAsc(field string) *QueryWrapper {
	q.Order = QueryOrder{
		FieldId: field,
		Sort:    "asc",
	}
	return q
}

func (q *QueryWrapper) OrderByDesc(field string) *QueryWrapper {
	q.Order = QueryOrder{
		FieldId: field,
		Sort:    "desc",
	}
	return q
}
