package app_gateway

type Result[T any] struct {
	ErrCode int    `json:"errcode"`
	Msg     string `json:"msg"`
	Data    T      `json:"data"`
}

type CreateGatewayGroupReq struct {
	Name     string `json:"name"`
	AppId    string `json:"app_id"`
	SubAppId string `json:"sub_app_id"`
	ModuleId string `json:"module_id"`
	Pid      string `json:"parentId"` //父节点应当等于"0"
	RootId   string `json:"root_id"`  //根节点 当前节点为根节点则设置为自己
	Type     string `json:"type"`     //route:路由分组 auth:鉴权分组
	IsSystem bool   `json:"is_system"`
}

type CreateRouteReq struct {
	AppId    string        `json:"app_id"`
	SubAppId string        `json:"sub_app_id"`
	ModuleId string        `json:"module_id"`
	RootId   string        `json:"root_id"`
	Config   GetawayConfig `json:"config"`
}

type UpdateRouteReq struct {
	AppId            string        `json:"app_id"`
	SubAppId         string        `json:"sub_app_id"`
	ModuleId         string        `json:"module_id"`
	RootId           string        `json:"root_id"`
	TargetOrderIndex int           `json:"target_order_index"` //排到最前面则传输负数
	UPorDown         int           `json:"up_or_down"`
	Config           GetawayConfig `json:"config"`
}

type DeleteRouteReq struct {
	AppId    string   `json:"app_id" form:"app_id" validate:"required" message:"app_id不能为空"`
	SubAppId string   `json:"sub_app_id"`
	ModuleId string   `json:"module_id"`
	RouteIds []string `json:"route_ids" form:"route_ids" validate:"required" message:"route_ids不能为空"`
}

type GetawayConfig struct {
	Pid          string `json:"parentId"`
	RouteId      string `json:"route_id"`
	Name         string `json:"name"`
	Type         string `json:"type"`
	From         string `json:"from"`
	To           string `json:"to"`
	AuthOpt      string `json:"auth_opt"` // ignore / disable / auth
	CountLimit   int    `json:"count_limit"`
	TimeLimit    int    `json:"time_limit"`
	ModuleId     string `json:"module_id"`
	TargetModule string `json:"target_module"`
}

type TokenConfigAddAccess struct {
	AppId    string       `json:"app_id"`
	SubAppId string       `json:"sub_app_id"`
	ModuleId string       `json:"module_id"`
	TokenIds []string     `json:"token_ids"`
	Access   AccessConfig `json:"access"`
}

type AccessConfig struct {
	Type        string `json:"type"`
	ServiceName string `json:"service_name"`
	Method      string `json:"method"`
	Url         string `json:"url"`
	Content     string `json:"content"`
}

type TokenRemoveAccess struct {
	AppId    string   `json:"app_id"`
	SubAppId string   `json:"sub_app_id"`
	ModuleId string   `json:"module_id"`
	TokenIds []string `json:"token_ids"`
	Method   string   `json:"method"`
	Url      string   `json:"url"`
}
