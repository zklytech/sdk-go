package app_gateway

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"

	"gitee.com/zklytech/sdk-go/common"
	"github.com/gin-gonic/gin"
)

var client *common.HttpClient

const gatewayBaseUrl = "http://app-gateway.infra:8091"
const authBaseUrl = "http://auth-service.infra:8091"

func init() {
	client = common.NewHttpClient()
}

func CreateGroup(ctx *gin.Context, form CreateGatewayGroupReq) (groupId string, err error) {

	bdata, _ := json.Marshal(form)

	return processRes[string](client.PostJson(ctx, gatewayBaseUrl+"/api/group/create", bdata, map[string]string{}))
}

func DeleteGroup(ctx *gin.Context, appId, groupId string) (err error) {
	params := url.Values{}
	params.Add("app_id", appId)
	params.Add("id", groupId)

	_, err = processRes[any](client.Delete(ctx, gatewayBaseUrl+"/api/group/delete", params))

	return
}

func CreateRoute(ctx *gin.Context, form CreateRouteReq) (routeId string, err error) {
	bdata, _ := json.Marshal(form)

	return processRes[string](client.PostJson(ctx, gatewayBaseUrl+"/api/route/config/create", bdata, map[string]string{}))
}

func UpdateRoute(ctx *gin.Context, form UpdateRouteReq) (err error) {
	bdata, _ := json.Marshal(form)

	_, err = processRes[any](client.PostJson(ctx, gatewayBaseUrl+"/api/route/config/update", bdata, map[string]string{}))
	return
}

func DeleteRoute(ctx *gin.Context, form DeleteRouteReq) (err error) {
	bdata, _ := json.Marshal(form)

	_, err = processRes[any](client.PostJson(ctx, gatewayBaseUrl+"/api/route/config/delete", bdata, map[string]string{}))

	return
}

func AddAuthToRoute(ctx *gin.Context, form TokenConfigAddAccess) (err error) {

	bdata, _ := json.Marshal(form)

	_, err = processRes[any](client.PostJson(ctx, authBaseUrl+"/api/token/config/add_access", bdata, map[string]string{}))

	return
}

func RemoveAuthFromRoute(ctx *gin.Context, form TokenRemoveAccess) (err error) {

	bdata, _ := json.Marshal(form)

	_, err = processRes[any](client.PostJson(ctx, authBaseUrl+"/api/token/config/remove_access", bdata, map[string]string{}))

	return
}

func processRes[T any](res []byte, status int, e error) (result T, err error) {

	if e != nil {
		err = e
		return
	}

	if status != http.StatusOK {
		err = errors.New("网络请求错误：" + strconv.Itoa(status))
		return
	}

	var j Result[T]

	json.Unmarshal(res, &j)

	if j.ErrCode != 0 {
		err = errors.New(j.Msg)
		return
	}

	result = j.Data

	return
}
