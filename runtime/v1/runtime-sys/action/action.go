package action

import (
	common "gitee.com/zklytech/sdk-go/runtime/v1/runtime-sys"
	"github.com/gin-gonic/gin"
	"net/url"
)

func SaveAction(ctx *gin.Context, form SavaActionForm) (err error) {
	_, err = common.BasePost[any](ctx, "/api/v1/action/save", form, map[string]string{})
	return err
}

func DetailAction(ctx *gin.Context, form DetailActionForm) (data ActionData, err error) {
	params := url.Values{}
	params.Add("app_id", form.AppId)
	params.Add("index", form.Index)
	params.Add("action_class", form.ActionClass)
	data, err = common.BaseGet[ActionData](ctx, "/api/action/detail", params, map[string]string{})
	return
}
func ListAction(ctx *gin.Context, form ListActionForm) (list []ActionData, err error) {
	list, err = common.BasePost[[]ActionData](ctx, "/api/v1/action/list", form, map[string]string{})
	return
}

func PrefixIndexAction(ctx *gin.Context, form PrefixIndexActionForm) (list []ActionData, err error) {
	list, err = common.BasePost[[]ActionData](ctx, "/api/v1/action/prefixIndexAction", form, map[string]string{})
	return
}

func DeleteAction(ctx *gin.Context, form DeleteActionForm) (err error) {
	_, err = common.BasePost[any](ctx, "/api/v1/action/delete", form, map[string]string{})
	return err
}

func RunAction(ctx *gin.Context, form RunActionForm) (r RunOutput, err error) {
	r, err = common.BasePost[RunOutput](ctx, "/api/v1/action/run", form, map[string]string{})
	if err != nil {
		return
	}
	return
}

func SaveRunLog(ctx *gin.Context, form RunLogForm) (err error) {
	_, err = common.BasePost[any](ctx, "/api/v1/action/saveRunLog", form, map[string]string{})
	return err
}
