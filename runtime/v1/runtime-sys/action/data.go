package action

import "time"

type CreateSourceInfo struct {
	Name  string `json:"name"`
	Title string `json:"title"`
}
type CreateServiceInfo struct {
	Name  string `json:"name"`
	Title string `json:"title"`
}

type ActionData struct {
	Id                string            `json:"id"`                  // 工作流id
	Status            string            `json:"status"`              // 状态
	EventType         string            `json:"event_type"`          // event_type
	Start             string            `json:"start"`               // 工作流开始位置
	CreateServiceInfo CreateServiceInfo `json:"create_service_info"` // 创建服务信息
	CreateSourceInfo  CreateSourceInfo  `json:"create_source_info"`  // 创建源信息
	TriggerName       string            `json:"trigger_name"`        // 触发名称
	TriggerTitle      string            `json:"trigger_title"`       // 触发标题
	Title             string            `json:"title"`               // 标题
	ActionInfo        any               `json:"action_info"`         // 动作
	Workflow          string            `json:"workflow"`            // 动作生成的工作流
	Extend            map[string]any    `json:"extend"`              // 扩展字段
	ActionClass       string            `json:"action_class"`        // 动作分类,JobAction 定时任务，DataAction 数据动作，FlowAction 流程动作 决定在appconfig 中Runtime 目录下的存储位置
	CreateTime        string            `json:"create_time,omitempty"`
	UpdateTime        string            `json:"update_time,omitempty"`
}

type SavaActionForm struct {
	AppId    string     `json:"app_id"`
	SubAppId string     `json:"sub_app_id"`
	ModuleId string     `json:"module_id" form:"module_id"`
	Index    string     `json:"index"` // 索引值 工作流id值或者event_type的值, 页面动作分段执行存的是id
	Data     ActionData `json:"data"`
}

type DetailActionForm struct {
	AppId       string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId    string `json:"sub_app_id" form:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId    string `json:"module_id" form:"module_id"`
	Index       string `json:"index" form:"index" validate:"required" message:"index required"` // 索引值
	ActionClass string `json:"action_class" form:"action_class" validate:"required" message:"action_class required"`
}

type ListActionForm struct {
	AppId       string         `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId    string         `json:"sub_app_id" form:"sub_app_id" `
	ModuleId    string         `json:"module_id" form:"module_id"`
	ActionClass string         `json:"action_class" form:"action_class"` // 动作分类
	Filter      map[string]any `json:"filter"`
}
type PrefixIndexActionForm struct {
	AppId       string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId    string `json:"sub_app_id" form:"sub_app_id" `
	ModuleId    string `json:"module_id" form:"module_id"`
	Index       string `json:"index" form:"index" validate:"required" message:"index required"`
	ActionClass string `json:"action_class" form:"action_class" validate:"required" message:"action_class required"`
}

type DeleteActionForm struct {
	AppId       string `json:"app_id"`
	SubAppId    string `json:"sub_app_id"`
	ModuleId    string `json:"module_id" form:"module_id"`
	Index       string `json:"index"`
	ActionClass string `json:"action_class"`
}

type RunActionForm struct {
	AppId       string `json:"app_id"` // 应用id
	SubAppId    string `json:"sub_app_id"`
	ModuleId    string `json:"module_id" form:"module_id"`
	Id          string `json:"id"`           // 工作流id
	Start       string `json:"start"`        // 工作流开始位置
	EventType   string `json:"event_type"`   // 事件类型
	IndexField  string `json:"index_field"`  // 触发索引字段，id，event_type
	ActionClass string `json:"action_class"` // 动作类型
	Params      any    `json:"params"`       // 触发参数
	Async       bool   `json:"async"`        // 是否异步
}
type RunOutput struct {
	Data    any    `json:"Data"`
	ErrMsg  string `json:"ErrMsg"`
	Success bool   `json:"Success"`
}

type Source struct {
	Name  string `json:"name"`
	Title string `json:"title"`
}

type RunLogForm struct {
	AppId        string    `json:"app_id" form:"app_id" validate:"required" message:"app_id required"` // 应用id
	SubAppId     string    `json:"sub_app_id" form:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId     string    `json:"module_id" form:"module_id"`
	RuntimeId    string    `json:"runtime_id" form:"runtime_id" validate:"required" message:"runtime_id required"` // 运行id
	EventId      string    `json:"event_id" form:"event_id" validate:"required" message:"event_id required"`       // 事件id
	EventType    string    `json:"event_type" form:"event_type" validate:"required" message:"event_type required"` // 事件类型
	Action       string    `json:"action" form:"action" validate:"required" message:"action required"`             // 执行的动作
	ActionTitle  string    `json:"action_title"`                                                                   // 动作的title
	Duration     int64     `json:"duration"`                                                                       // 耗时
	Res          string    `json:"res"`                                                                            // 执行结果
	Success      bool      `json:"Success"`                                                                        //执行状态
	StartTime    time.Time `json:"start_time"`                                                                     // 开始时间
	EndTime      time.Time `json:"end_time"`                                                                       // 结束时间
	TraceId      string    `json:"trace_id"`                                                                       //  链路id
	TriggerName  string    `json:"trigger_name"`                                                                   // 触发名称
	TriggerTitle string    `json:"trigger_title"`                                                                  // 触发标题
	Id           string    `json:"id"`                                                                             // 工作流id
	Source       Source    `json:"source"`                                                                         // 工作流的创建源
}
