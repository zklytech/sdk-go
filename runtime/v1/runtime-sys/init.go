package runtime_sys

import (
	"encoding/json"
	"errors"
	"gitee.com/zklytech/sdk-go/common"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/url"
	"strconv"
)

const (
	runtimeUrl = "http://runtime-sys-service.infra:8080"
)

type CommonResData[T any] struct {
	Errcode int    `json:"errcode"`
	Msg     string `json:"msg"`
	Data    T      `json:"data"`
}

func BaseGet[T any](ctx *gin.Context, api string, params url.Values, header map[string]string) (data T, err error) {
	client := common.NewHttpClient()
	res, httpCode, err := client.GetWithHeader(ctx, runtimeUrl+api, params, header)
	if err != nil {
		err = errors.New("request fail: " + err.Error())
		return
	}
	if httpCode != http.StatusOK {
		err = errors.New("request fail: " + strconv.Itoa(httpCode))
		return
	}
	var commonRes CommonResData[T]

	err = json.Unmarshal(res, &commonRes)
	if err != nil {
		err = errors.New("unmarshal fail: " + err.Error())
		return
	}
	if commonRes.Errcode != 0 {
		err = errors.New("service err " + commonRes.Msg)
		return
	}
	return commonRes.Data, nil

}

func BasePost[T any](ctx *gin.Context, api string, data any, header map[string]string) (resData T, err error) {
	client := common.NewHttpClient()
	bodyJosn, err := json.Marshal(data)
	if err != nil {
		err = errors.New("marshal fail: " + err.Error())
		return
	}

	res, httpCode, err := client.PostJson(ctx, runtimeUrl+api, bodyJosn, header)
	if err != nil {
		err = errors.New("request fail: " + err.Error())
		return
	}
	if httpCode != http.StatusOK {
		err = errors.New("request fail: " + strconv.Itoa(httpCode))
		return
	}
	var commonRes CommonResData[T]
	err = json.Unmarshal(res, &commonRes)
	if err != nil {
		err = errors.New("unmarshal fail: " + err.Error())
		return
	}
	if commonRes.Errcode != 0 {
		err = errors.New("service err " + commonRes.Msg)
		return
	}
	return commonRes.Data, nil
}
