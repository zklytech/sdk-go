package variable

import (
	common "gitee.com/zklytech/sdk-go/runtime/v1/runtime-sys"
)
import (
	"github.com/gin-gonic/gin"
	"net/url"
)

func SetDesc(ctx *gin.Context, form SetDescForm) (err error) {
	_, err = common.BasePost[any](ctx, "/api/v1/variable/setDesc", form, map[string]string{})
	return
}
func GetDesc(ctx *gin.Context, form GetDescForm) (data map[string][]DescData, err error) {

	data, err = common.BasePost[map[string][]DescData](ctx, "/api/v1/variable/getDesc", form, map[string]string{})
	return
}

func GetAppAllPageDesc(ctx *gin.Context, form GetAppAllPageDescForm) (data []DescData, err error) {
	data, err = common.BasePost[[]DescData](ctx, "/api/v1/variable/getAppAllPageDesc", form, map[string]string{})
	return

}

func Set(ctx *gin.Context, form SetForm, headers map[string]string) (err error) {
	_, err = common.BasePost[any](ctx, "/api/v1/variable/set", form, headers)
	return
}
func Get(ctx *gin.Context, form GetForm, headers map[string]string) (data any, err error) {
	params := url.Values{}
	params.Add("key", form.Key)
	params.Add("scope", form.Scope)
	data, err = common.BaseGet[any](ctx, "/api/v1/variable/get", params, headers)
	return
}
func GetByAlias(ctx *gin.Context, form GetByAliasForm, headers map[string]string) (data any, err error) {
	params := url.Values{}
	params.Add("alias", form.Alias)
	data, err = common.BaseGet[any](ctx, "/api/v1/variable/getByAlias", params, headers)
	return
}

func Del(ctx *gin.Context, form DeleteForm, headers map[string]string) (err error) {
	_, err = common.BasePost[any](ctx, "/api/v1/variable/del", form, headers)
	return
}

func Watch(ctx *gin.Context, form WatchForm, headers map[string]string) (err error) {
	_, err = common.BasePost[any](ctx, "/api/v1/variable/watch", form, headers)
	return
}

func Unwatch(ctx *gin.Context, form UnwatchForm, headers map[string]string) (err error) {

	_, err = common.BasePost[any](ctx, "/api/v1/variable/unwatch", form, headers)
	return
}
func DelWithPrefix(ctx *gin.Context, form DelWithPrefixForm, headers map[string]string) (err error) {
	_, err = common.BasePost[any](ctx, "/api/v1/variable/delWithPrefix", form, headers)
	return
}

func SetAlias(ctx *gin.Context, form SetAliasForm, headers map[string]string) (err error) {
	_, err = common.BasePost[any](ctx, "/api/v1/variable/setAlias", form, headers)
	return
}

func DelAlias(ctx *gin.Context, form DeleteAliasForm, headers map[string]string) (err error) {
	_, err = common.BasePost[any](ctx, "/api/v1/variable/delAlias", form, headers)
	return
}
func WatchByAlias(ctx *gin.Context, form WatchByAliasForm, headers map[string]string) (err error) {
	_, err = common.BasePost[any](ctx, "/api/v1/variable/watchByAlias", form, headers)
	return
}

func UnwatchByAlias(ctx *gin.Context, form UnwatchByAliasForm, headers map[string]string) (err error) {
	_, err = common.BasePost[any](ctx, "/api/v1/variable/unwatchByAlias", form, headers)
	return
}

func GetRuntimeVar(ctx *gin.Context, form GetRuntimeVariableForm) (data any, err error) {
	data, err = common.BasePost[any](ctx, "/api/v1/variable/getRuntimeVar", form, map[string]string{})
	return
}
func SetRuntimeVar(ctx *gin.Context, form SetRuntimeVariableForm) (err error) {
	_, err = common.BasePost[any](ctx, "/api/v1/variable/setRuntimeVar", form, map[string]string{})
	return
}
