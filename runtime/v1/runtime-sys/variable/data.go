package variable

type SetForm struct {
	Key    string `json:"key" form:"key" validate:"required" message:"key required"`
	Alias  string `json:"alias" form:"alias"`
	Value  any    `json:"value" form:"value" validate:"required" message:"value required"`
	Scope  string `json:"scope" form:"scope" validate:"required|in:global,service" message:"required:scope required|in:must global,service"`
	Expire int64  `json:"expire" form:"expire" `
}

type GetForm struct {
	Key   string `json:"key" form:"key" validate:"required" message:"key required"`
	Scope string `json:"scope" form:"scope" validate:"required|in:global,service" message:"required:scope required|in:must global,service"`
}

type DeleteForm struct {
	Key   string `json:"key" form:"key" validate:"required" message:"key required"`
	Scope string `json:"scope" form:"scope" validate:"required|in:global,service" message:"required:scope required|in:must global,service"`
}

type GetByAliasForm struct {
	Alias string `json:"alias" form:"alias" validate:"required" message:"alias required"`
}

type SetAliasForm struct {
	Key   string `json:"key" form:"key" validate:"required" message:"key required"`
	Alias string `json:"alias" form:"alias" validate:"required" message:"alias required"`
}
type DeleteAliasForm struct {
	Alias      string `json:"alias" form:"alias" validate:"required" message:"alias required"`
	DeleteData bool   `json:"delete_data" form:"delete_data"`
}

type WatchForm struct {
	Key   string `json:"key" form:"key" validate:"required" message:"key required"`
	Scope string `json:"scope" form:"scope" validate:"required|in:global,service" message:"required:scope required|in:must global,service"`
	Url   string `json:"url" form:"url" validate:"required|isFullURL" message:"required:url required|isFullURL: url format error"`
}
type UnwatchForm struct {
	Key   string `json:"key" form:"key" validate:"required" message:"key required"`
	Scope string `json:"scope" form:"scope" validate:"required|in:global,service" message:"required:scope required|in:must global,service"`
}

type WatchByAliasForm struct {
	Alias string `json:"alias" form:"alias" validate:"required" message:"alias required"`
	Url   string `json:"url" form:"url" validate:"required|isFullURL" message:"required:url required|isFullURL: url format error"`
}
type UnwatchByAliasForm struct {
	Alias string `json:"alias" form:"alias" validate:"required" message:"alias required"`
}

type DelWithPrefixForm struct {
	Key   string `json:"key" form:"key" validate:"required" message:"key required"`
	Scope string `json:"scope" form:"scope" validate:"required|in:global,service" message:"required:scope required|in:must global,service"`
}

type Format struct {
	Id     string `json:"id"`
	Name   string `json:"name"`
	Title  string `json:"title"`
	Type   string `json:"type"`
	Format any    `json:"format"`
}

type DescData struct {
	DefaultKey   string `json:"default_key"` // 默认key
	RuleKey      string `json:"rule_key"`
	Rule         string `json:"rule"`
	Type         string `json:"type"`
	DefaultValue any    `json:"default_value"`
	//Format       string     `json:"format"` // 数据格式规则：如时间格式化 Y-m-d
	Pid      string     `json:"pid"`
	Id       string     `json:"id"`
	Title    string     `json:"title"`     //(服务变量第一级是服务标题)
	Name     string     `json:"name"`      // 服务变量第一级是服务名称
	DataType string     `json:"data_type"` // 数据类型,data-数据,class-分类
	Format   any        `json:"format"`
	Children []DescData `json:"children"`
	PageId   string     `json:"page_id"`
}

// 设置变量描述
type SetDescForm struct {
	AppId       string     `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId    string     `json:"sub_app_id" form:"sub_app_id"`
	ModuleId    string     `json:"module_id" form:"module_id"`
	Type        string     `json:"type" form:"type" validate:"required" message:"type required"` //app-app变量描述,service-服务变量描述,page-页面变量描述
	ServiceName string     `json:"service_name" form:"service_name"`
	PageId      string     `json:"page_id" form:"page_id" ` // 设置页面变量描述时必传
	Data        []DescData `json:"data" form:"data" validate:"required" message:"data required"`
}

type GetDescForm struct {
	AppId    string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId string `json:"sub_app_id" form:"sub_app_id"`
	ModuleId string `json:"module_id" form:"module_id"`
	PageId   string `json:"page_id" form:"page_id"` //获取页面变量描述时必传
}

type GetAppAllPageDescForm struct {
	AppId    string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId string `json:"sub_app_id" form:"sub_app_id"`
	ModuleId string `json:"module_id" form:"module_id"`
}

type SetRuntimeVariableForm struct {
	AppId     string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId  string `json:"sub_app_id" form:"sub_app_id"`
	ModuleId  string `json:"module_id" form:"module_id"`
	RuntimeId string `json:"runtime_id" form:"runtime_id" validate:"required" message:"app_id required"`
	Key       string `json:"key" form:"key" validate:"required" message:"key required"`
	Value     any    `json:"value" form:"value" validate:"required" message:"value required"`
}

type GetRuntimeVariableForm struct {
	AppId     string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId  string `json:"sub_app_id" form:"sub_app_id"`
	ModuleId  string `json:"module_id" form:"module_id"`
	RuntimeId string `json:"runtime_id" form:"runtime_id" validate:"required" message:"app_id required"`
	Key       string `json:"key" form:"key" validate:"required" message:"key required"`
}
