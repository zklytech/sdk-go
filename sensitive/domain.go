package sensitive

type SensitiveConfig struct {
	Method       string               `json:"method"`
	Rule         string               `json:"rule"`
	RuleCond     string               `json:"rule_cond"`
	RuleValue    []SensitiveRuleValue `json:"rule_value"`
	ReplaceRule  string               `json:"replace_rule"`
	ReplaceValue string               `json:"replace_value"`
	Enable       bool                 `json:"enable"`
}

type SensitiveRuleValue struct {
	Operator string `json:"operator"`
	Value    any    `json:"value"`
}
