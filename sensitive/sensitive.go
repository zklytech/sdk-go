package sensitive

import (
	"errors"
	"math/rand"
	"regexp"
	"strconv"
	"time"
)

var randomSeqMap = map[string]string{
	"alpha":  "QWERTYUIOPASDFGHJKLZXCVBNM",
	"number": "0123456789",
	"*#":     "*#",
}

var random *rand.Rand

// 正则缓存
var regCache map[string]*regexp.Regexp

func init() {
	random = rand.New(rand.NewSource(time.Now().UnixNano()))
	regCache = make(map[string]*regexp.Regexp)
}

func TransformData(origin []map[string]any, config map[string]SensitiveConfig) (result []map[string]any, err error) {

	for _, data := range origin {
		for f, c := range config {
			val, ok := data[f]
			if !ok {
				continue
			}
			if !c.Enable {
				continue
			}
			var newVal string
			switch c.Method {
			case "text":
				newVal, err = text(c, val)
			case "date":
				newVal, err = date(c, val)
			case "option":
				newVal, err = option(c, val)
			default:
				newVal, err = text(c, val)
			}
			if err != nil {
				return
			}
			data[f] = newVal
		}

		result = append(result, data)
	}

	return
}

func text(c SensitiveConfig, value any) (result string, err error) {
	str, ok := value.(string)
	if !ok {
		err = errors.New("字段类型错误，非文本类型")
		return
	}

	switch c.Rule {
	case "value":
		result, err = textValue(c, str)
	case "keep":
		result, err = TextKeep(c, str)
	case "sub":
		result, err = textReplace(c, str)
	case "regex":
		result, err = textRegex(c, str)
	}

	return
}

func date(c SensitiveConfig, value any) (result string, err error) {
	str, ok := value.(string)
	if !ok {
		err = errors.New("字段类型错误，非文本类型")
		return
	}

	switch c.Rule {
	case "value":
		result, err = dateValue(c, str)
	case "keep":
		result, err = TextKeep(c, str)
	case "sub":
		result, err = textReplace(c, str)
	case "regex":
		result, err = textRegex(c, str)
	}

	return
}

func option(c SensitiveConfig, value any) (result string, err error) {
	l, ok := value.([]any)
	if !ok {
		err = errors.New("选项类型错误")
		return
	}
	v, ok := l[0].(map[string]any)
	if !ok {
		err = errors.New("选项类型错误")
		return
	}

	str := v["values"].(string)

	switch c.Rule {
	case "value":
		result, err = textValue(c, str)
	case "keep":
		result, err = TextKeep(c, str)
	case "sub":
		result, err = textReplace(c, str)
	case "regex":
		result, err = textRegex(c, str)
	}
	return
}

func textValue(c SensitiveConfig, value string) (result string, err error) {
	result = value
	val, err := strconv.ParseFloat(value, 64)
	if err != nil {
		err = errors.New("数据类型错误，文本非纯数值")
		return
	}
	if c.RuleCond == "range" {
		for _, rc := range c.RuleValue {
			rv, e := convertValue[float64](rc.Value)
			if e != nil {
				err = e
				return
			}
			if !compare(rc.Operator, val, rv) {
				return
			}
		}
		result = ReplaceRange(c, value, []int{0, len(value) - 1})
	} else {
		rv, e := convertValue[float64](c.RuleValue[0].Value)
		if e != nil {
			err = e
			return
		}
		if compare(c.RuleCond, val, rv) {
			result = ReplaceRange(c, value, []int{0, len(value) - 1})
		}
	}

	return
}

func dateValue(c SensitiveConfig, value string) (result string, err error) {
	date, err := time.Parse(time.RFC3339, value)
	if err != nil {
		err = errors.New("日期格式错误")
		return
	}
	if c.RuleCond == "range" {
		for _, rc := range c.RuleValue {
			rv, e := convertValue[string](rc.Value)
			if e != nil {
				err = e
				return
			}
			rvd, e := time.Parse(time.RFC3339, rv)
			if e != nil {
				err = errors.New("条件日期格式错误")
				return
			}
			if !dateCompare(rc.Operator, date, rvd) {
				return
			}
		}
		result = ReplaceRange(c, value, []int{0, len(value) - 1})
	} else {
		rv, e := convertValue[string](c.RuleValue[0].Value)
		if e != nil {
			err = e
			return
		}
		rvd, e := time.Parse(time.RFC3339, rv)
		if e != nil {
			err = errors.New("条件日期格式错误")
			return
		}
		if dateCompare(c.RuleCond, date, rvd) {
			result = ReplaceRange(c, value, []int{0, len(value) - 1})
		}
	}
	return
}

func TextKeep(c SensitiveConfig, value string) (result string, err error) {
	lv, err := convertValue[float64](c.RuleValue[0].Value)
	if err != nil {
		return
	}
	l := int(lv)
	if c.RuleCond == "keep_header" {
		result = ReplaceRange(c, value, []int{int(l), len(value) - 1})
	} else if c.RuleCond == "keep_tail" {
		result = ReplaceRange(c, value, []int{0, len(value) - int(l) - 1})
	} else if c.RuleCond == "keep_range" {
		rv, e := convertValue[float64](c.RuleValue[1].Value)
		if e != nil {
			err = e
			return
		}
		r := int(rv)

		result = ReplaceRange(c, value, []int{0, int(l) - 1, r + 1, len(value) - 1})
	}
	return
}

func textReplace(c SensitiveConfig, value string) (result string, err error) {
	lv, err := convertValue[float64](c.RuleValue[0].Value)
	if err != nil {
		return
	}

	l := int(lv)
	if c.RuleCond == "sub_header" {
		result = ReplaceRange(c, value, []int{0, int(l) - 1})
	} else if c.RuleCond == "sub_tail" {
		result = ReplaceRange(c, value, []int{len(value) - int(l), len(value) - 1})
	} else if c.RuleCond == "sub_range" {
		rv, e := convertValue[float64](c.RuleValue[1].Value)
		if e != nil {
			err = e
			return
		}
		r := int(rv)
		result = ReplaceRange(c, value, []int{int(l) - 1, int(r) - 1})
	}
	return
}

func textRegex(c SensitiveConfig, value string) (result string, err error) {
	exp, err := convertValue[string](c.RuleValue[0].Value)
	var regex *regexp.Regexp
	var ok bool
	if regex, ok = regCache[exp]; !ok {
		regex, err = regexp.Compile(exp)
		if err != nil {
			err = errors.New("正则表达式格式错误")
			return
		}
		regCache[exp] = regex
	}

	rag := regex.FindAllStringIndex(value, -1)
	r := []int{}
	for _, r1 := range rag {
		// 正则匹配的返回值为左闭右开区间
		// relaceRange函数处理的为闭区间，需要转换一下
		r = append(r, r1[0], r1[1]-1)
	}
	if len(r) > 0 {
		result = ReplaceRange(c, value, r)
	} else {
		result = value
	}
	return
}

func compare(operator string, left float64, right float64) bool {
	switch operator {
	case "eq":
		return left == right
	case "gt":
		return left > right
	case "ge":
		return left >= right
	case "lt":
		return left < right
	case "le":
		return left <= right
	}

	return false
}

func dateCompare(operator string, left time.Time, right time.Time) bool {
	com := left.Compare(right)
	switch operator {
	case "eq":
		return com == 0
	case "gt":
		return com > 0
	case "ge":
		return com >= 0
	case "lt":
		return com < 0
	case "le":
		return com <= 0
	}

	return false
}

func ReplaceRange(c SensitiveConfig, value string, rg []int) (result string) {

	for idx, i := range rg {
		if idx%2 == 0 {
			last := -1
			if idx > 0 {
				last = rg[idx-1]
			}
			result += value[last+1 : i]
		} else if idx%2 != 0 {
			last := rg[idx-1]
			result += replace(c, value[last:i+1])
		}
	}
	result += value[rg[len(rg)-1]+1:]

	return
}

func replace(c SensitiveConfig, value string) (result string) {

	if c.ReplaceRule == "iterate" {
		rl := len(c.ReplaceValue)
		for idx := 0; idx < len(value); idx++ {
			s := idx % rl
			result += c.ReplaceValue[s : s+1]
		}
	} else if c.ReplaceRule == "whole" {
		result = c.ReplaceValue
	} else if c.ReplaceRule == "random" {
		seq := randomSeqMap[c.ReplaceValue]
		for idx := 0; idx < len(value); idx++ {
			p := random.Int() % len(seq)
			result += seq[p : p+1]
		}
	}
	return
}

func convertValue[T any](value any) (result T, err error) {
	result, ok := value.(T)
	if !ok {
		err = errors.New("数值类型错误")
	}

	return
}
