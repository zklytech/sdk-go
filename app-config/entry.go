package app_config

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"gitee.com/zklytech/sdk-go/common"

	"github.com/gin-gonic/gin"
)

var (
	baseUrl           = "http://app-config-service.infra:8080/api"
	registerUrl       = "/service/register"
	setConfigUrl      = "/setConfig"
	getConfigUrl      = "/getConfig"
	getListUrl        = "/getConfigList"
	deleteConfigUrl   = "/deleteConfig"
	deleteByPrefixUrl = "/deleteConfigByPathPrefix"
)

var client *common.HttpClient

func init() {
	client = common.NewHttpClient()
}

func ServiceRegister(ctx *gin.Context, req ServiceRegisterReq) (err error) {
	bdata, _ := json.Marshal(req)
	_, err = processRes[any](client.PostJson(ctx, baseUrl+registerUrl, bdata, map[string]string{}))
	return
}

func SetConfig(ctx *gin.Context, req ConfigSetReq) (err error) {
	bdata, _ := json.Marshal(req)
	_, err = processRes[any](client.PostJson(ctx, baseUrl+setConfigUrl, bdata, map[string]string{}))
	return
}

func GetConfig[T any](ctx *gin.Context, req ConfigGetReq) (result ConfigItem[T], err error) {
	bdata, _ := json.Marshal(req)

	result, err = processRes[ConfigItem[T]](client.PostJson(ctx, baseUrl+getConfigUrl, bdata, map[string]string{}))
	return
}

func GetConfigList[T any](ctx *gin.Context, req ConfigListReq) (result GetListResp[T], err error) {
	bdata, _ := json.Marshal(req)

	result, err = processRes[GetListResp[T]](client.PostJson(ctx, baseUrl+getListUrl, bdata, map[string]string{}))
	return
}

func DeleteConfig(ctx *gin.Context, req DeleteKeyForm) (err error) {
	bdata, _ := json.Marshal(req)
	_, err = processRes[any](client.PostJson(ctx, baseUrl+deleteConfigUrl, bdata, map[string]string{}))
	return
}

func DeleteConfigByPrefix(ctx *gin.Context, req DeletePrefixForm) (err error) {
	bdata, _ := json.Marshal(req)
	_, err = processRes[any](client.PostJson(ctx, baseUrl+deleteByPrefixUrl, bdata, map[string]string{}))
	return
}

func processRes[T any](res []byte, status int, e error) (result T, err error) {

	if e != nil {
		err = e
		return
	}

	if status != http.StatusOK {
		err = errors.New("网络请求错误：" + strconv.Itoa(status))
		return
	}

	var j Result[T]

	json.Unmarshal(res, &j)

	if j.ErrCode != 0 {
		err = errors.New(j.Msg)
		return
	}

	result = j.Data

	return
}
