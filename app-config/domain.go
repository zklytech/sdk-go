package app_config

import "time"

type Result[T any] struct {
	ErrCode int    `json:"errcode"`
	Msg     string `json:"msg"`
	Data    T      `json:"data"`
}

type DeleteKeyForm struct {
	AppId    string `json:"AppId" form:"AppId" validate:"required" message:"AppId 必须"`
	SubAppId string `json:"SubAppId" form:"SubAppId"`
	ModuleId string `json:"ModuleId" form:"ModuleId"`
	Type     string `json:"Type" form:"Type" validate:"required|enum:APP_CURRENT_OPT,APP_SERVICE,APP_CURRENT_CONFIG,APP_INFRA" message:"required:Type required|enum:Type invalid"`
	Path     string `json:"Path" form:"Path"`
}
type DeletePrefixForm struct {
	AppId    string         `json:"AppId" form:"AppId" validate:"required" message:"AppId 必须"`
	SubAppId string         `json:"SubAppId" form:"SubAppId"`
	ModuleId string         `json:"ModuleId" form:"ModuleId"`
	Type     string         `json:"Type" form:"Type" validate:"required|enum:APP_CURRENT_OPT,APP_SERVICE,APP_CURRENT_CONFIG,APP_INFRA" message:"required:Type required|enum:Type invalid"`
	Path     string         `json:"Path" form:"Path" validate:"required" message:"path required"`
	Filter   map[string]any `json:"filter" form:"filter"`
}

type ConfigGetReq struct {
	AppId    string         `json:"AppId" form:"AppId" validate:"required" message:"AppId 必须"`
	SubAppId string         `json:"SubAppId" form:"SubAppId"`
	ModuleId string         `json:"ModuleId" form:"ModuleId"`
	Type     string         `json:"Type" form:"Type"`
	Fields   string         `json:"Fields" form:"Fields"`
	Path     string         `json:"Path" form:"Path"`
	Filter   map[string]any `json:"Filter" form:"Filter"`
}

type SortValue struct {
	Sort  string `json:"sort" form:"sort"`
	Field string `json:"field" from:"field"`
}

type ConfigListReq struct {
	AppId    string         `json:"AppId" form:"AppId" validate:"required" message:"AppId 必须"`
	SubAppId string         `json:"SubAppId" form:"SubAppId"`
	ModuleId string         `json:"ModuleId" form:"ModuleId"`
	Type     string         `json:"Type" form:"Type"`
	Filter   map[string]any `json:"Filter" form:"Filter"`
	Sort     []SortValue    `json:"Sort" form:"Sort"`
	Page     int64          `json:"Page" form:"Page" default:"1"`
	PageSize int64          `json:"PageSize" form:"PageSize" default:"20"`
}

type ServiceRegisterReq struct {
	AppId       string                 `json:"AppId" form:"AppId" validate:"required" message:"AppId required"`
	SubAppId    string                 `json:"SubAppId" form:"SubAppId"`
	ModuleId    string                 `json:"ModuleId" form:"ModuleId"`
	ServiceName string                 `json:"ServiceName" form:"ServiceName" validate:"required" message:"ServiceName required"`
	Data        map[string]interface{} `json:"Data" form:"Data" validate:"required" message:"Data required"`
}

type ConfigSetReq struct {
	AppId    string      `json:"AppId" form:"AppId" validate:"required" message:"AppId required"`
	SubAppId string      `json:"SubAppId" form:"SubAppId"`
	ModuleId string      `json:"ModuleId" form:"ModuleId"`
	Data     interface{} `json:"Data" form:"Data" validate:"required" message:"Data required"`
	Path     string      `json:"Path" form:"Path"`
	Type     string      `json:"Type" form:"Type" validate:"required|enum:APP_CURRENT_OPT,APP_SERVICE,APP_CURRENT_CONFIG,APP_INFRA" message:"required:Type required|enum:Type invalid"`
}

type ConfigItem[T any] struct {
	Id         string    `json:"_id"`
	Data       T         `json:"data"`
	Path       string    `json:"path"`
	Type       string    `json:"type"`
	AppId      string    `json:"app_id"`
	ModuleId   string    `json:"module"`
	CreateTime time.Time `json:"create_time"`
	UpdateTime time.Time `json:"update_time"`
}

type CallResp[T any] struct {
	ErrCode int    `json:"errcode"`
	Msg     string `json:"msg"`
	Data    T      `json:"data"`
}

type GetListResp[T any] struct {
	List  []ConfigItem[T] `json:"list"`
	Total int64           `json:"total"`
}
