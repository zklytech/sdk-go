package mongo_sdk

// sdk接口定义

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strconv"

	"gitee.com/zklytech/sdk-go/common"
	"github.com/gin-gonic/gin"
)

const (
	applicationId = "Application-Id"
	modelId       = "Model-Id"
	instanceId    = "Instance-Id"
	bizInstanceId = "Biz-Instance-Id"
	collectionId  = "Collection-Id"
	transactionId = "Transaction-Id"
)

var client *common.HttpClient

func init() {
	client = common.NewHttpClient()
}

func CreateCollection(httpReq *http.Request, req CallSdkReq) (result CreateCollectionResp, err error) {
	resp, err := callSdkService[CreateCollectionResp](httpReq, &req, createCollection)
	if err != nil {
		return
	}
	result = resp.Result

	return
}

func DeleteCollection(httpReq *http.Request, req CallSdkReq) (err error) {
	_, err = callSdkService[any](httpReq, &req, deleteCollection)

	return
}

func CreateIndex(httpReq *http.Request, req CreateIndexReq) (err error) {
	resp, err := callSdkService[any](httpReq, &req, createIndex)
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
	}
	return
}

func DropIndex(httpReq *http.Request, req DropIndexReq) (err error) {
	resp, err := callSdkService[any](httpReq, &req, dropIndex)
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
	}
	return
}

func DbSize(httpReq *http.Request, req CallSdkReq) (result uint64, err error) {
	resp, err := callSdkService[uint64](httpReq, &req, dbSize)
	if err != nil {
		return
	}
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
		return
	}
	result = resp.Result

	return
}

func CopyData(httpReq *http.Request, req CopyDataReq) (err error) {
	resp, err := callSdkService[any](httpReq, &req, copyData)
	if err != nil {
		return
	}
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
		return
	}

	return
}

func FindOne[T any](httpReq *http.Request, query QueryWrapper[T]) (result T, err error) {
	resp, err := callSdkService[T](httpReq, &query, findOne)
	if err != nil {
		return
	}
	result = resp.Result

	return
}

func FindMany[T any](httpReq *http.Request, query QueryWrapper[T]) (result QueryResult[T], err error) {
	resp, err := callSdkService[QueryResult[T]](httpReq, &query, find)
	if err != nil {
		return
	}
	result = resp.Result

	return
}

func Count(httpReq *http.Request, query QueryWrapper[int64]) (result int64, err error) {
	resp, err := callSdkService[int64](httpReq, &query, count)
	if err != nil {
		return
	}

	result = resp.Result
	return
}

func InsertOne[T any](httpReq *http.Request, req InsertOneReq[T]) (result string, err error) {
	resp, err := callSdkService[string](httpReq, &req, insertOne)
	if err != nil {
		return
	}
	result = resp.Result

	return
}

func InsertMany[T any](httpReq *http.Request, req InsertManyReq[T]) (result []string, err error) {
	resp, err := callSdkService[[]string](httpReq, &req, insertMany)
	if err != nil {
		return
	}
	result = resp.Result

	return
}

func UpdateById[T any](httpReq *http.Request, req UpdateOneReq[T]) (result int, err error) {
	resp, err := callSdkService[int](httpReq, &req, updateById)
	if err != nil {
		return
	}
	result = resp.Result
	return
}

func UpdateMany[T any](httpReq *http.Request, req UpdateWrapper[T]) (result int, err error) {
	resp, err := callSdkService[int](httpReq, &req, updateMany)
	if err != nil {
		return
	}
	result = resp.Result
	return
}

func DeleteOne[T any](httpReq *http.Request, query QueryWrapper[T]) (result int, err error) {
	resp, err := callSdkService[int](httpReq, &query, deleteOne)
	if err != nil {
		return
	}
	result = resp.Result

	return
}

func DeleteMany[T any](httpReq *http.Request, query QueryWrapper[T]) (result int, err error) {
	resp, err := callSdkService[int](httpReq, &query, deleteMany)
	if err != nil {
		return
	}
	result = resp.Result

	return
}

func SaveHook(httpReq *http.Request, req SaveHookReq) (err error) {
	_, err = callSdkService[any](httpReq, &req, saveHook)

	return
}

func DeleteHook(httpReq *http.Request, req DeleteHookReq) (err error) {
	_, err = callSdkService[any](httpReq, &req, saveHook)

	return
}

// 创建一个模拟的Gin上下文的辅助函数
func createTestContext() *gin.Context {
	// 创建一个Gin引擎
	// 创建一个模拟的HTTP请求
	re := httptest.NewRequest("GET", "/example", nil)
	// 创建一个模拟的Gin上下文
	writer := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(writer)
	re.Header.Set("Trace-Id", "jobrestart")
	re.Header.Set("Event-Id", "jobrestart")
	context.Request = re
	return context
}

// 调用SDK sevice
func callSdkService[T any](httpReq *http.Request, req SdkServiceReq, m string) (resp CallSdkResp[T], err error) {
	// 序列化参数
	bData := req.ToJson()

	header := map[string]string{
		applicationId:   httpReq.Header.Get(applicationId),
		modelId:         httpReq.Header.Get(modelId),
		instanceId:      httpReq.Header.Get(instanceId),
		bizInstanceId:   httpReq.Header.Get(bizInstanceId),
		collectionId:    httpReq.Header.Get(collectionId),
		transactionId:   httpReq.Header.Get(transactionId),
		MethodHeaderKey: m,
		"Logintoken":    httpReq.Header.Get("Logintoken"),
	}

	ctx := createTestContext()

	buff, status, err := client.PostJson(ctx, "http://mongo-sdk-v1.mongo-sdk-klskoz.svc.cluster.local", bData, header)
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = errors.New("网络请求错误：" + strconv.Itoa(status))
		return
	}
	// 反序列化结果
	err = json.Unmarshal(buff, &resp)
	if err != nil {
		return
	}

	if resp.ErrCode != Success {
		err = errors.New(resp.ErrMessage)
	}
	return
}
