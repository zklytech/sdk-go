package mongo_sdk

import (
	"errors"
	"net/http"
	"reflect"
	"strings"

	"gitee.com/zklytech/sdk-go/common"
	data_service "gitee.com/zklytech/sdk-go/data-service"
	"gitee.com/zklytech/sdk-go/runtime/v1/runtime-sys/variable"
	"gitee.com/zklytech/sdk-go/tool"
)

var OperatorMapping = map[string]string{
	"eq":  "$eq",
	"ne":  "$ne",
	"in":  "$in",
	"nin": "$nin",
	"gt":  "$gt",
	"ge":  "$gte",
	"lt":  "$lt",
	"le":  "$lte",
}

type UserInfo struct {
	ID        string         `json:"id"`
	Account   string         `json:"account"`
	CacheData map[string]any `json:"cache_data"`
}

func BuildFilter(req *http.Request, conditions []data_service.QueryCondition, params map[string]any, userInfo UserInfo) map[string]any {

	var p = map[string]any{}
	var orList = []map[string]any{}
	var andList = []map[string]any{}
	for idx, cond := range conditions {
		var relation = cond.Relation
		if idx == 0 && len(conditions) > 1 && conditions[1].Relation == "or" {
			relation = "or"
		}
		if cond.IsGroup {
			pg := BuildFilter(req, cond.List, params, userInfo)
			if relation == "and" {
				andList = append(andList, pg)
			} else {
				orList = append(orList, pg)
			}
		} else {
			if cond.Cond.Symbol == "range" {
				if vals, ok := cond.Cond.Values.([]any); ok && len(vals) > 0 && vals[0] != nil {
					rangeList := []map[string]any{
						{
							cond.Cond.FieldId: map[string]any{
								"$gte": vals[0],
							},
						}}
					if len(vals) > 1 && vals[1] != nil {
						rangeList = append(rangeList, map[string]any{
							cond.Cond.FieldId: map[string]any{
								"$lt": vals[1],
							},
						})
					}
					rangeQ := map[string]any{
						"$and": rangeList,
					}
					if relation == "and" {
						andList = append(andList, rangeQ)
					} else {
						orList = append(orList, rangeQ)
					}
				}
			} else {
				f, v := createCond(req, cond.Cond, params, userInfo)
				// 没有值则不加入提交
				if len(v) < 1 {
					continue
				}
				if relation == "and" {
					andList = append(andList, map[string]any{
						f: v,
					})
				} else {
					orList = append(orList, map[string]any{
						f: v,
					})
				}
			}
		}
	}
	if len(andList) > 0 {
		p["$and"] = andList
	}
	if len(orList) > 0 {
		p["$or"] = orList
	}
	return p
}

func createCond(req *http.Request, cond data_service.Condition, params map[string]any, userInfo UserInfo) (string, map[string]any) {
	var fileName = cond.FieldId
	if cond.ValueType == "pointer" {
		fileName += ".id"
	}

	if cond.Symbol == "null" {
		return fileName, map[string]any{
			"$exists": false,
		}
	} else if cond.Symbol == "nnull" {
		return fileName, map[string]any{
			"$exists": true,
		}
	} else {
		val := getQueryValue(req, cond, params, userInfo)

		if val != nil {

			isArray := strings.Contains(reflect.TypeOf(val).String(), "[]")

			if cond.Symbol == "in" && !isArray && cond.ValueType == "pointer" {
				val = []any{val}
			}

			if cond.Symbol == "in" && cond.ValueType == "string" && !isArray {
				return fileName, map[string]any{
					"$regex": val,
				}
			}

			if cond.Symbol == "nin" && cond.ValueType == "string" && !isArray {
				return fileName, map[string]any{
					"$not": map[string]any{
						"$regex": val,
					},
				}
			}

			return fileName, map[string]any{
				OperatorMapping[cond.Symbol]: val,
			}
		} else {
			return fileName, map[string]any{}
		}
	}
}

func getQueryValue(req *http.Request, cond data_service.Condition, params map[string]any, userInfo UserInfo) any {
	var value = cond.Values
	var settingType = cond.SettingType

	var val any

	if settingType == "relation" {
		val = params[value.(string)]
	} else if settingType == "var" {
		val, _ = getVarValue(req, cond, userInfo, params)
	} else {
		val = value
	}
	if cond.ValueType == "pointer" {
		va, ok := val.([]any)
		if !ok {
			return val
		}
		if cond.Symbol == "in" || cond.Symbol == "nin" {
			ids := []any{}
			for _, v := range va {
				ids = append(ids, v.(map[string]any)["id"])
			}
			val = ids
			return val
		} else {
			val = va[0].(map[string]any)["id"]
			return val
		}
	}
	return val
}

func getVarValue(req *http.Request, cond data_service.Condition, userInfo UserInfo, params map[string]any) (val any, err error) {
	header := map[string]string{}

	if cond.VarInfo.Scope == "page" || cond.VarInfo.Scope == "app" {
		v, ex := params[cond.VarInfo.Id]
		if !ex {
			err = errors.New("变量不存在")
			return
		}
		val = v
	} else {
		c := common.CreateGinContext(header)

		key, e := tool.ReplaceVariables(cond.VarInfo.Rule, userInfo.ID)
		if e != nil {
			err = e
			return
		}

		val, err = variable.Get(c, variable.GetForm{
			Key:   key,
			Scope: "service",
		}, map[string]string{
			"App-Id":       req.Header.Get("Application-Id"),
			"Service-Name": cond.VarInfo.Scope,
		})
	}

	return
}
