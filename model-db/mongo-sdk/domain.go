package mongo_sdk

import (
	"encoding/json"

	modeldb "gitee.com/zklytech/sdk-go/model-db"
)

// sdk接口参数的数据结构定义

// 接口调用的数据结构定义
type CallSdkReq struct {
	Schema     string                   `json:"schema"`
	Collection string                   `json:"collection"`
	ColConfig  modeldb.CollectionConfig `json:"collection_config"`
}

// 接口返回的结果数据结构定义
type CallSdkResp[T any] struct {
	ErrCode    int    `json:"err_code"`
	ErrMessage string `json:"err_message"`
	Result     T      `json:"result"`
}

type CreateCollectionResp struct {
	CollectionName string `json:"collection_name"`
	ConnectStr     string `json:"connect_str"`
}

type SaveHookReq struct {
	Req  CallSdkReq
	Data []any `json:"data"`
}

type DeleteHookReq struct {
	Req  CallSdkReq
	Data []string `json:"data"`
}

type CreateIndexReq struct {
	Req   CallSdkReq
	Index []IndexDomain `json:"index_list"`
}

type DropIndexReq struct {
	Req   CallSdkReq
	Field []string `json:"field_list"`
}

type IndexDomain struct {
	Fields []string `json:"field"`
	Unique bool     `json:"unique"`
}

func (req *CreateIndexReq) ToJson() []byte {
	bdata, _ := json.Marshal(req)
	return bdata
}
func (req *DropIndexReq) ToJson() []byte {
	bdata, _ := json.Marshal(req)
	return bdata
}

type CopyDataReq struct {
	SourceInstance   string `json:"source_instance"`
	SourceCollection string `json:"source_collection"`
	TargetInstance   string `json:"target_instance"`
	TargetCollection string `json:"target_collection"`
}

func (req *CopyDataReq) ToJson() []byte {
	bdata, _ := json.Marshal(req)
	return bdata
}

func (req *CallSdkReq) ToJson() []byte {
	bdata, _ := json.Marshal(req)
	return bdata
}

func (req *SaveHookReq) ToJson() []byte {
	bdata, _ := json.Marshal(req)
	return bdata
}

func (req *DeleteHookReq) ToJson() []byte {
	bdata, _ := json.Marshal(req)
	return bdata
}

type SdkServiceReq interface {
	ToJson() []byte
}

// query

type QueryWrapper[T any] struct {
	Schema      string                   `json:"schema"`
	Collection  string                   `json:"collection"`
	Query       map[string]any           `json:"query"`
	SelectField []any                    `json:"select"`
	LimitCount  int64                    `json:"limit"`
	OffsetCount int64                    `json:"offset"`
	SortList    []SortRule               `json:"sort"`
	ColConfig   modeldb.CollectionConfig `json:"collection_config"`
}

type SortRule struct {
	Field     string `json:"field"`
	AscOrDesc int    `json:"asc_or_desc"`
}

type QueryResult[T any] struct {
	List  []T   `json:"list"`
	Total int64 `json:"total"`
}

func (query *QueryWrapper[T]) ToJson() []byte {
	bdata, _ := json.Marshal(query)
	return bdata
}

func NewQueryWrapper[T any](schema string, collection string) QueryWrapper[T] {
	return QueryWrapper[T]{
		Schema:     schema,
		Collection: collection,
		Query:      map[string]any{},
	}
}

func (query *QueryWrapper[T]) Eq(fieldName string, value any) *QueryWrapper[T] {
	query.Query[fieldName] = map[string]any{
		"$eq": value,
	}
	return query
}

func (query *QueryWrapper[T]) Ne(fieldName string, value any) *QueryWrapper[T] {
	query.Query[fieldName] = map[string]any{
		"$ne": value,
	}
	return query
}

func (query *QueryWrapper[T]) Gt(fieldName string, value any) *QueryWrapper[T] {
	query.Query[fieldName] = map[string]any{
		"$gt": value,
	}
	return query
}

func (query *QueryWrapper[T]) Lt(fieldName string, value any) *QueryWrapper[T] {
	query.Query[fieldName] = map[string]any{
		"$lt": value,
	}
	return query
}

func (query *QueryWrapper[T]) In(fieldName string, value []any) *QueryWrapper[T] {
	query.Query[fieldName] = map[string]any{
		"$in": value,
	}
	return query
}

func (query *QueryWrapper[T]) Search(fieldName string, value any) *QueryWrapper[T] {
	query.Query[fieldName] = map[string]any{
		"$text": map[string]any{
			"$search": value,
		},
	}
	return query
}

func (query *QueryWrapper[T]) Expr(fieldName string, value string) *QueryWrapper[T] {
	query.Query[fieldName] = map[string]any{
		"$expr": value,
	}
	return query
}

func (query *QueryWrapper[T]) Or(value map[string]any) *QueryWrapper[T] {
	if _, ok := query.Query["$or"]; !ok {
		query.Query["$or"] = []map[string]any{}
	}
	query.Query["$or"] = append(query.Query["$or"].([]map[string]any), value)
	return query
}

func (query *QueryWrapper[T]) Select(value []any) *QueryWrapper[T] {
	query.SelectField = value
	return query
}

func (query *QueryWrapper[T]) Offset(count int64) *QueryWrapper[T] {
	query.OffsetCount = count
	return query
}

func (query *QueryWrapper[T]) Limit(count int64) *QueryWrapper[T] {
	query.LimitCount = count
	return query
}

func (query *QueryWrapper[T]) Sort(field string, ascOrDesc int) *QueryWrapper[T] {
	query.SortList = append(query.SortList, SortRule{
		Field:     field,
		AscOrDesc: ascOrDesc,
	})
	return query
}

// insert
type InsertOneReq[T any] struct {
	CallSdkReq
	InsertOption string `json:"insert_option"` // abort / replace
	Data         T      `json:"data"`
}

type InsertManyReq[T any] struct {
	CallSdkReq
	InsertOption string `json:"insert_option"` // abort / replace / error
	Data         []T    `json:"data"`
}

func (req *InsertOneReq[T]) ToJson() []byte {
	bdata, _ := json.Marshal(req)
	return bdata
}

func (req *InsertManyReq[T]) ToJson() []byte {
	bdata, _ := json.Marshal(req)
	return bdata
}

func (req *InsertManyReq[T]) AbortWhenDuplicateKey() *InsertManyReq[T] {
	req.InsertOption = "abort"
	return req
}

func (req *InsertManyReq[T]) ReplaceWhenDuplicateKey() *InsertManyReq[T] {
	req.InsertOption = "replace"
	return req
}

func (req *InsertManyReq[T]) ErrorWhenDuplicateKey() *InsertManyReq[T] {
	req.InsertOption = "error"
	return req
}

// update
type UpdateOneReq[T any] struct {
	CallSdkReq
	Data T `json:"data"`
}

func (req *UpdateOneReq[T]) ToJson() []byte {
	bdata, _ := json.Marshal(req)
	return bdata
}

type UpdateWrapper[T any] struct {
	Schema     string                   `json:"schema"`
	Collection string                   `json:"collection"`
	Query      map[string]any           `json:"query"`
	UpdateSet  map[string]any           `json:"update_set"`
	ColConfig  modeldb.CollectionConfig `json:"collection_config"`
}

func (query *UpdateWrapper[T]) ToJson() []byte {
	bdata, _ := json.Marshal(query)
	return bdata
}

func NewUpdateWrapper[T any](schema string, collection string) UpdateWrapper[T] {
	return UpdateWrapper[T]{
		Schema:     schema,
		Collection: collection,
		Query:      map[string]any{},
		UpdateSet: map[string]any{
			"$set": map[string]any{},
		},
	}
}

func (query *UpdateWrapper[T]) Eq(fieldName string, value any) *UpdateWrapper[T] {
	query.Query[fieldName] = map[string]any{
		"$eq": value,
	}
	return query
}

func (query *UpdateWrapper[T]) Ne(fieldName string, value any) *UpdateWrapper[T] {
	query.Query[fieldName] = map[string]any{
		"$ne": value,
	}
	return query
}

func (query *UpdateWrapper[T]) Gt(fieldName string, value any) *UpdateWrapper[T] {
	query.Query[fieldName] = map[string]any{
		"$gt": value,
	}
	return query
}

func (query *UpdateWrapper[T]) Lt(fieldName string, value any) *UpdateWrapper[T] {
	query.Query[fieldName] = map[string]any{
		"$lt": value,
	}
	return query
}

func (query *UpdateWrapper[T]) In(fieldName string, value []any) *UpdateWrapper[T] {
	query.Query[fieldName] = map[string]any{
		"$in": value,
	}
	return query
}

func (query *UpdateWrapper[T]) Search(fieldName string, value any) *UpdateWrapper[T] {
	query.Query[fieldName] = map[string]any{
		"$text": map[string]any{
			"$search": value,
		},
	}
	return query
}

func (query *UpdateWrapper[T]) Expr(fieldName string, value string) *UpdateWrapper[T] {
	query.Query[fieldName] = map[string]any{
		"$expr": value,
	}
	return query
}

func (query *UpdateWrapper[T]) Or(value map[string]any) *UpdateWrapper[T] {
	if _, ok := query.Query["$or"]; !ok {
		query.Query["$or"] = []map[string]any{}
	}
	query.Query["$or"] = append(query.Query["$or"].([]map[string]any), value)
	return query
}

func (query *UpdateWrapper[T]) Set(fieldName string, value any) *UpdateWrapper[T] {
	query.UpdateSet["$set"].(map[string]any)[fieldName] = value
	return query
}

// delete
type DeleteWrapper struct {
	CallSdkReq
	Data []string `json:"data"`
}

func (req *DeleteWrapper) ToJson() []byte {
	bdata, _ := json.Marshal(req)
	return bdata
}
