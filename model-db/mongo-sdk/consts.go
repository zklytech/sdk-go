package mongo_sdk

// sdk接口使用的常量定义

const (
	Success         = 0
	Fail            = 1
	MethodHeaderKey = "Mongo-Method"
)

const (
	createCollection = "create_collection"
	deleteCollection = "delete_collection"
	createIndex      = "create_index"
	dropIndex        = "drop_index"
	insertOne        = "insert_one"
	insertMany       = "insert_many"
	findOne          = "find_one"
	find             = "find"
	updateById       = "update_by_id"
	updateMany       = "update_many"
	deleteOne        = "delete_one"
	deleteMany       = "delete_many"
	saveHook         = "save_hook"
	deleteHook       = "delete_hook"
	count            = "count"
	dbSize           = "db_size"
	copyData         = "copy_data"
)
