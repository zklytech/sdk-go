package modeldb

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"gitee.com/zklytech/sdk-go/common"
	"github.com/gin-gonic/gin"
)

var client *common.HttpClient

const baseUri = "http://model-db.infra:8082/api"

func init() {
	client = common.NewHttpClient()
}

type Result[T any] struct {
	Errcode int    `json:"errcode"`
	Msg     string `json:"msg"`
	Data    T      `json:"data"`
}

func GetHookUrl(c *gin.Context, modelId, hookName string) (url string, err error) {

	params := map[string]any{
		"model_db_id": modelId,
		"hook_name":   hookName,
	}

	bdata, _ := json.Marshal(params)
	res, statusCode, err := client.PostJson(c, baseUri+"/model/func/hook/url", bdata, map[string]string{})
	if err != nil {
		return
	}
	if statusCode != http.StatusOK {
		err = errors.New("请求失败，status code:" + strconv.Itoa(statusCode))
		return
	}

	var j Result[string]
	json.Unmarshal(res, &j)
	if j.Errcode != 0 {
		err = errors.New(j.Msg)
		return
	}

	url = j.Data

	return
}
