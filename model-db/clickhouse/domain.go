package clickhouse_sdk

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"

	modeldb "gitee.com/zklytech/sdk-go/model-db"
)

// 接口返回的结果数据结构定义
type CallSdkResp[T any] struct {
	ErrCode    int    `json:"err_code"`
	ErrMessage string `json:"err_message"`
	Result     T      `json:"result"`
}

type QueryResult[T any] struct {
	List  []T   `json:"list"`
	Total int64 `json:"total"`
}

type SdkServiceReq interface {
	ToJson() []byte
}

type TableDSL struct {
	DatabaseName string     `json:"database_name"`
	TableName    string     `json:"table_name"`
	IndexEngine  string     `json:"index_engine"`
	FieldList    []FieldDSL `json:"field_list"`
	IndexOrder   []string   `json:"index_order"`
	Cluster      bool       `json:"cluster"`
}

type FieldDSL struct {
	FieldName  string `json:"field_name"`
	FieldType  string `json:"field_type"`
	NotNull    bool   `json:"not_null"`
	Default    string `json:"default"`
	AutoIncre  bool   `json:"auto_incre"`
	PrimaryKey bool   `json:"primary_key"`
}

type CopyDataReq struct {
	SourceInstance   string `json:"source_instance"`
	SourceCollection string `json:"source_collection"`
	TargetInstance   string `json:"target_instance"`
	TargetCollection string `json:"target_collection"`
}

func (t CopyDataReq) ToJson() []byte {
	bdata, _ := json.Marshal(t)
	return bdata
}

func (t TableDSL) ToJson() []byte {
	bdata, _ := json.Marshal(t)
	return bdata
}

func (t TableDSL) BuildDDL() string {
	createStart := strings.ReplaceAll(fmt.Sprintf("create table `%s`.`%s` ", t.DatabaseName, t.TableName), "-", "_")

	if t.Cluster {
		createStart = createStart + " on cluster cluster "
	}
	createStart = createStart + " (\n"

	primaryKeys := []string{}
	fieldList := ""
	for idx, f := range t.FieldList {
		fieldList = fieldList + fmt.Sprintf("`%s` %s", f.FieldName, GetValueType(f.FieldType))
		if f.NotNull {
			fieldList = fieldList + " not null "
		}
		if !reflect.ValueOf(f.Default).IsZero() {
			fieldList = fieldList + fmt.Sprintf(" default %s ", f.Default)
		}
		if idx < len(t.FieldList)-1 {
			fieldList = fieldList + ","
		}

		fieldList = fieldList + "\n"
		if f.PrimaryKey {
			primaryKeys = append(primaryKeys, fmt.Sprintf("`%s`", f.FieldName))
		}
	}

	// // 如果有创建时间和更新时间，加上跳数索引，加快排序速度
	// index := ""
	// if hasCt {
	// 	index += "\nINDEX ct_order create_time TYPE minmax GRANULARITY 4\n"
	// }
	// if hasUt {
	// 	index += "INDEX ut_order update_time TYPE minmax GRANULARITY 4\n"
	// }

	createEnd := ")\n"

	engine := fmt.Sprintf("engine = %s\n", t.IndexEngine)

	engineOrder := "order by (" + strings.Join(t.IndexOrder, ",") + ")\n"

	primaryKey := "primary key (" + strings.Join(primaryKeys, ",") + ")\n"

	sql := createStart + fieldList + createEnd + engine
	if len(t.IndexOrder) > 0 {
		sql = sql + engineOrder
	}

	if len(primaryKeys) > 0 {
		sql = sql + primaryKey
	}

	return sql
}

func GetValueType(ovt string) (valueType string) {

	vMap := map[string]string{
		"number":    "Float64",
		"string":    "String",
		"pointer":   "String",
		"boolean":   "Bool",
		"json":      "String",
		"timestamp": "timestamp",
		"file":      "String",
		"array":     "String",
	}

	return vMap[ovt]
}

type AlertWrapper struct {
	DatabaseName string    `json:"database_name"`
	TableName    string    `json:"table_name"`
	DiffList     []DiffDSL `json:"diff_list"`
}

type DiffDSL struct {
	Type        string      `json:"type"`
	DiffType    string      `json:"diff_type"`
	DiffContent DiffContent `json:"diff_content"`
}

type DiffContent struct {
	FieldName  string         `json:"field_name"`
	FieldType  string         `json:"field_type"`
	ConfigInfo map[string]any `json:"config_info"`
}

func (t AlertWrapper) ToJson() []byte {
	bdata, _ := json.Marshal(t)
	return bdata
}

type QueryWrapper struct {
	Database  string                   `json:"database"`
	TableName string                   `json:"table_name"`
	Sel       []string                 `json:"select"`
	Wh        []QueryWhere             `json:"where"`
	Group     []string                 `json:"group_by"`
	Order     []QueryOrder             `json:"order"`
	Li        int64                    `json:"limit"`
	Off       int64                    `json:"offset"`
	ColConfig modeldb.CollectionConfig `json:"collection_config"`
}

type QueryWhere struct {
	Relation string       `json:"relation"`
	Exp      string       `json:"exp"`
	Arg      any          `json:"Arg"`
	IsGroup  bool         `json:"is_group"`
	Group    []QueryWhere `json:"group"`
}

type QueryOrder struct {
	FieldName string `json:"field_name"`
	Order     string `json:"order"`
}

func NewQueryWrapper(database string, table string) *QueryWrapper {
	return &QueryWrapper{
		Database:  database,
		TableName: table,
		Wh:        []QueryWhere{},
	}
}

func (q QueryWrapper) ToJson() []byte {
	bdata, _ := json.Marshal(q)
	return bdata
}

func (q *QueryWrapper) Select(field []string) *QueryWrapper {
	q.Sel = append(q.Sel, field...)
	return q
}

func (q *QueryWrapper) Where(relation string, exp string, arg any) *QueryWrapper {
	q.Wh = append(q.Wh, QueryWhere{
		Relation: relation,
		Exp:      exp,
		Arg:      arg,
		IsGroup:  false,
	})
	return q
}

func (q *QueryWrapper) WhereGroup(relation string, other *QueryWrapper) *QueryWrapper {
	q.Wh = append(q.Wh, QueryWhere{
		Relation: relation,
		IsGroup:  true,
		Group:    other.Wh,
	})
	return q
}

func (q *QueryWrapper) GroupBy(fieldName string) *QueryWrapper {
	q.Group = append(q.Group, fieldName)
	return q
}

func (q *QueryWrapper) OrderBy(fieldName string, order string) *QueryWrapper {
	q.Order = append(q.Order, QueryOrder{
		FieldName: fieldName,
		Order:     order,
	})
	return q
}

func (q *QueryWrapper) Limit(count int64) *QueryWrapper {
	q.Li = count
	return q
}

func (q *QueryWrapper) Offset(count int64) *QueryWrapper {
	q.Off = count
	return q
}

type UpdateWrapper struct {
	Database  string                   `json:"database"`
	TableName string                   `json:"table_name"`
	Sets      []UpdateSet              `json:"set"`
	Wh        []QueryWhere             `json:"where"`
	ColConfig modeldb.CollectionConfig `json:"collection_config"`
}

type UpdateSet struct {
	FieldName string `json:"field_name"`
	Value     any    `json:"value"`
}

func NewUpdateWrapper(database string, table string) *UpdateWrapper {
	return &UpdateWrapper{
		Database:  database,
		TableName: table,
		Wh:        []QueryWhere{},
	}
}

func (q UpdateWrapper) ToJson() []byte {
	bdata, _ := json.Marshal(q)
	return bdata
}

func (q *UpdateWrapper) Set(fieldName string, value any) *UpdateWrapper {
	q.Sets = append(q.Sets, UpdateSet{
		FieldName: fieldName,
		Value:     value,
	})
	return q
}

func (q *UpdateWrapper) Where(relation string, exp string, arg any) *UpdateWrapper {
	q.Wh = append(q.Wh, QueryWhere{
		Relation: relation,
		Exp:      exp,
		Arg:      arg,
		IsGroup:  false,
	})
	return q
}

func (q *UpdateWrapper) WhereGroup(relation string, other *UpdateWrapper) *UpdateWrapper {
	q.Wh = append(q.Wh, QueryWhere{
		Relation: relation,
		IsGroup:  true,
		Group:    other.Wh,
	})
	return q
}

type InsertWrapper struct {
	Database  string                   `json:"database"`
	TableName string                   `json:"table_name"`
	Data      []any                    `json:"data"`
	ColConfig modeldb.CollectionConfig `json:"collection_config"`
}

func (q InsertWrapper) ToJson() []byte {
	bdata, _ := json.Marshal(q)
	return bdata
}

type DropWrapper struct {
	Database  string `json:"database"`
	TableName string `json:"table_name"`
}

func (q DropWrapper) ToJson() []byte {
	bdata, _ := json.Marshal(q)
	return bdata
}

type SqlExecWrapper struct {
	Sql       string                   `json:"sql"`
	Params    []any                    `json:"params"`
	ColConfig modeldb.CollectionConfig `json:"collection_config"`
}

func (q SqlExecWrapper) ToJson() []byte {
	bdata, _ := json.Marshal(q)
	return bdata
}

type ViewDSL struct {
	Database  string `json:"database"`
	ViewName  string `json:"view_name"`
	Cluster   bool   `json:"cluster"`
	SelectSQL string `json:"select_sql"`
}

func (d ViewDSL) ToJson() []byte {
	bdata, _ := json.Marshal(d)
	return bdata
}

func (dsl *ViewDSL) BuildViewDDL() string {

	sql := "create or replace view "

	viewName := strings.ReplaceAll(fmt.Sprintf("%s.%s", dsl.Database, dsl.ViewName), "-", "_")

	sql += viewName

	if dsl.Cluster {
		sql += " on cluster cluster "
	}

	sql += " as "

	sql += dsl.SelectSQL

	return sql
}

type DropViewWrapper struct {
	Database string
	ViewName string
}

func (d DropViewWrapper) ToJson() []byte {
	bdata, _ := json.Marshal(d)
	return bdata
}
