package clickhouse_sdk

// sdk接口使用的常量定义

const (
	applicationId = "Application-Id"
	modelId       = "Model-Id"
	instanceId    = "Instance-Id"
	bizInstanceId = "Biz-Instance-Id"
	collectionId  = "Collection-Id"
	transactionId = "Transaction-Id"
)

const (
	Success         = 0
	Fail            = 1
	MethodHeaderKey = "ClickHouse-Method"
)
