package clickhouse_sdk

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strconv"

	"gitee.com/zklytech/sdk-go/common"
	"github.com/gin-gonic/gin"
)

var client *common.HttpClient

func init() {
	client = common.NewHttpClient()
}

// 创建一个模拟的Gin上下文的辅助函数
func createGinContext() *gin.Context {
	// 创建一个Gin引擎
	// 创建一个模拟的HTTP请求
	re := httptest.NewRequest("GET", "/example", nil)
	// 创建一个模拟的Gin上下文
	writer := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(writer)
	context.Request = re
	return context
}

func CreateTable(httpReq *http.Request, dsl TableDSL) (err error) {

	resp, err := callSdkService[any](httpReq, dsl, "create_table")
	if err != nil {
		return
	}
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
		return
	}

	return
}

func DbSize(httpReq *http.Request, dsl TableDSL) (result uint64, err error) {

	resp, err := callSdkService[uint64](httpReq, dsl, "db_size")
	if err != nil {
		return
	}
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
		return
	}
	result = resp.Result

	return
}

func AlertTable(httpReq *http.Request, dsl AlertWrapper) (err error) {

	resp, err := callSdkService[any](httpReq, dsl, "alert_table")
	if err != nil {
		return
	}
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
		return
	}

	return
}

func DropTable(httpReq *http.Request, dsl DropWrapper) (err error) {

	resp, err := callSdkService[any](httpReq, dsl, "drop_table")
	if err != nil {
		return
	}
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
		return
	}

	return
}

func CreateView(httpReq *http.Request, dsl ViewDSL) (err error) {

	resp, err := callSdkService[any](httpReq, dsl, "create_view")
	if err != nil {
		return
	}
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
		return
	}

	return
}

func DropView(httpReq *http.Request, dsl DropViewWrapper) (err error) {

	resp, err := callSdkService[any](httpReq, dsl, "drop_view")
	if err != nil {
		return
	}
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
		return
	}

	return
}

func CopyData(httpReq *http.Request, req CopyDataReq) (err error) {

	resp, err := callSdkService[any](httpReq, req, "copy_data")
	if err != nil {
		return
	}
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
		return
	}

	return
}

func Insert(httpReq *http.Request, req InsertWrapper) (result []string, err error) {
	resp, err := callSdkService[[]string](httpReq, req, "insert_many")
	if err != nil {
		return
	}
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
		return
	}

	result = resp.Result
	return
}

func Query[T any](httpReq *http.Request, req QueryWrapper) (result QueryResult[T], err error) {
	resp, err := callSdkService[QueryResult[T]](httpReq, req, "query")
	if err != nil {
		return
	}
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
		return
	}

	result = resp.Result
	return
}

func Update(httpReq *http.Request, req UpdateWrapper) (result int64, err error) {
	resp, err := callSdkService[int64](httpReq, req, "update_many")
	if err != nil {
		return
	}
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
		return
	}

	result = resp.Result
	return
}

func Count(httpReq *http.Request, req QueryWrapper) (result int64, err error) {
	resp, err := callSdkService[int64](httpReq, req, "count")
	if err != nil {
		return
	}
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
		return
	}

	result = resp.Result
	return
}

func Delete(httpReq *http.Request, req QueryWrapper) (result int64, err error) {
	resp, err := callSdkService[int64](httpReq, req, "delete_many")
	if err != nil {
		return
	}
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
		return
	}

	result = resp.Result
	return
}

func SqlExec(httpReq *http.Request, req SqlExecWrapper) (result []map[string]any, err error) {
	resp, err := callSdkService[[]map[string]any](httpReq, req, "sql")
	if err != nil {
		return
	}
	if resp.ErrCode != 0 {
		err = errors.New(resp.ErrMessage)
		return
	}

	result = resp.Result
	return
}

// 调用SDK sevice
func callSdkService[T any](httpReq *http.Request, req SdkServiceReq, m string) (resp CallSdkResp[T], err error) {
	// 序列化参数
	bData := req.ToJson()

	header := map[string]string{
		applicationId:   httpReq.Header.Get(applicationId),
		modelId:         httpReq.Header.Get(modelId),
		instanceId:      httpReq.Header.Get(instanceId),
		bizInstanceId:   httpReq.Header.Get(bizInstanceId),
		collectionId:    httpReq.Header.Get(collectionId),
		transactionId:   httpReq.Header.Get(transactionId),
		MethodHeaderKey: m,
		"Logintoken":    httpReq.Header.Get("Logintoken"),
	}

	ctx := createGinContext()

	buff, status, err := client.PostJson(ctx, "http://clickhouse-sdk-v1.ck-sdk-slhasy.svc.cluster.local", bData, header)
	if err != nil {
		return
	}
	if status != http.StatusOK {
		err = errors.New("网络请求错误：" + strconv.Itoa(status))
		return
	}
	// 反序列化结果
	err = json.Unmarshal(buff, &resp)
	if err != nil {
		return
	}
	return
}
