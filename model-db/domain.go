package modeldb

import "gitee.com/zklytech/sdk-go/sensitive"

type ModelDBDescribe struct {
	ModelDB ModelDB          `json:"model_db"`
	Schemas []SchemaDescribe `json:"schemas"`
}

type SchemaDescribe struct {
	Schema      ModelSchema          `json:"schema"`
	Collections []CollectionDescribe `json:"collections"`
}

type CollectionDescribe struct {
	Collection ModelCollection   `json:"collection"`
	Fields     []CollectionField `json:"fields"`
}

type ModelDB struct {
	ModelDBId   string `json:"model_db_id"`
	ModelDBName string `json:"model_db_name"`
	ModelDBType string `json:"model_db_type"`
	Libs        string `json:"libs"`
	Describe    string `json:"describe"`
	Status      string `json:"status"`
	Config      string `json:"config"`
}

type ModelSchema struct {
	InstanceId       string `json:"instance_id"`
	SchemaID         string `json:"schema_id"`
	SchemaName       string `json:"schema_name"`
	SchemaEntityName string `json:"schema_entity_name"`
	SchemaType       string `json:"schema_type"`
	StorageType      string `json:"storage_type"`
	Describe         string `json:"describe"`
}

type ModelCollection struct {
	SchemaId             string `json:"schema_id"`
	CollectionId         string `json:"collection_id"`
	CollectionName       string `json:"collection_name"`
	CollectionEntityName string `json:"collection_entity_name"`
	Rules                string `json:"rules"`
	ConfigInfo           string `json:"config_info"`
}

type CollectionField struct {
	CollectionId    string `json:"collection_id"`
	FieldId         string `json:"field_id"`
	FieldName       string `json:"field_name"`
	FieldType       string `json:"field_type"`
	FieldEntityName string `json:"field_entity_name"`
	ConfigInfo      string `json:"config_info"`
}

type CollectionConfig struct {
	LockOn           bool                                 `json:"lock_on"`
	LockType         string                               `json:"lock_type"`
	CacheOn          bool                                 `json:"cache_on"`
	CacheType        string                               `json:"cache_type"`
	CompatibleOption string                               `json:"compatible_option"`
	CURDOption       []string                             `json:"curd_option"`
	SkipCreateId     bool                                 `json:"skip_create_id"`
	PrivateConfig    map[string]sensitive.SensitiveConfig `json:"private_config"`
}
