package ide

import (
	"github.com/gin-gonic/gin"
)

func RegisterDataModelService(ctx *gin.Context, form RegisterModelServiceForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/registerDataModelService", form)
	return
}

func RegisterModelMenu(ctx *gin.Context, form RegisterMenuForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/registerModelMenu", form)
	return
}

func RegisterIde(ctx *gin.Context, form RegisterIdeForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/registerIde", form)
	return
}

func RegisterPlugin(ctx *gin.Context, form RegisterPluginForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/registerPlugin", form)

	return
}

func RegisterServiceIDE(ctx *gin.Context, form RegisterServiceIDEForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/serviceMarket/installService", form)

	return
}

func AddModelView(ctx *gin.Context, form AddModelViewForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/addModeView", form)
	return
}

func UpdateModelView(ctx *gin.Context, form UpdateModelViewForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/updateModelView", form)
	return
}

func DeleteModelView(ctx *gin.Context, form DeleteModelViewForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/deleteModelView", form)
	return
}

func UpdateTabBar(ctx *gin.Context, form UpdateTabBarForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/updateTabBar", form)
	return
}

func DeleteTabBar(ctx *gin.Context, form DeleteTabBarForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/deleteTabBar", form)
	return
}

func AddTabChild(ctx *gin.Context, form AddTabChildForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/addTabChild", form)
	return
}

func UpdateTabChild(ctx *gin.Context, form UpdateTabChildForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/updateTabChild", form)
	return
}

func DeleteTabChild(ctx *gin.Context, form DeleteTabChildForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/deleteTabChild", form)
	return
}

func UnregisterDataModelService(ctx *gin.Context, form UnregisterModelServiceForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/unregisterDataModelService", form)
	return
}

func UnregisterModelMenu(ctx *gin.Context, form UnRegisterMenuForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/unregisterMenu", form)
	return
}

func UnregisterPlugin(ctx *gin.Context, form UnregisterPluginForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/unregisterPlugin", form)
	return
}

func UnregisterServiceIDE(ctx *gin.Context, form UnregisterServiceIDEForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/serviceMarket/uninstallService", form)
	return
}
