package ide

import "github.com/gin-gonic/gin"

type UpdateDataModelParams struct {
	AppId       string                 `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId    string                 `json:"sub_app_id" form:"sub_app_id"`
	ModuleId    string                 `json:"module_id" form:"module_id"`
	ModelId     string                 `json:"model_id" form:"model_id" validate:"required" message:"model_id required"`
	UpdateType  string                 `json:"update_type" form:"update_type" validate:"required" message:"update_type required"`
	DataModelId string                 `json:"data_model_id" form:"update_model_id" validate:"required" message:"data_model_id required"`
	UpdatePrams map[string]interface{} `json:"update_params" form:"update_params" validate:"required" message:"update_params required"`
}

// 更新data
func UpdateDataModel(ctx *gin.Context, p UpdateDataModelParams) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/updateDataModel", p)
	return
}
func DeleteModelData(ctx *gin.Context, form DeleteModelForm) (err error) {
	err = baseWUDRequest(ctx, "/v1/data/deleteModel", form)
	return
}
