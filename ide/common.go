package ide

const baseUrl = "http://ideserver-service.infra:8080/api"

type ResCommonData[T any] struct {
	Errcode int    `json:"errcode"`
	Msg     string `json:"msg"`
	Data    T      `json:"data"`
}
type FuncConfig struct {
	Api string `json:"api"`
}
type RegisterServiceData struct {
	Version     string                `json:"version" form:"version" validate:"required" message:"data.version required"`
	Title       string                `json:"title" form:"title" validate:"required" message:"data.title required"`
	Func        map[string]FuncConfig `json:"func" form:"func"`
	ServiceName string                `json:"service_name" form:"service_name" validate:"required" message:"service_name required"`
}

type RegisterModelServiceForm struct {
	AppId    string              `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId string              `json:"sub_app_id" form:"sub_app_id"`
	ModuleId string              `json:"module_id" form:"module_id"`
	Data     RegisterServiceData `json:"data" form:"data"`
}

type Provider struct {
	ServiceName string `json:"service_name" form:"service_name" validate:"required" message:"service_name required"`
	Version     string `json:"version" form:"version" validate:"required" message:"version required"`
}
type Func struct {
	Name   string         `json:"name"`
	Params map[string]any `json:"params"`
}
type RegisterMenuData struct {
	MenuTitle string             `json:"menu_title" form:"menu_title"`
	MenuName  string             `json:"menu_name" form:"menu_name"`
	Sort      int                `json:"sort" form:"fort"`
	Provider  Provider           `json:"provider" form:"provider" `
	Func      Func               `json:"func" form:"func"`
	Children  []RegisterMenuData `json:"children" form:"children"`
	Class     string             `json:"class" form:"class"` // 分类
}
type RegisterMenuForm struct {
	AppId    string           `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId string           `json:"sub_app_id" form:"sub_app_id"`
	ModuleId string           `json:"module_id" form:"module_id"`
	Data     RegisterMenuData `json:"data" form:"data" validate:"required" message:"data required"`
}

type TabBar struct {
	Id       string   `json:"id"`
	Name     string   `json:"name"`
	Title    string   `json:"title"`
	Icon     string   `json:"icon"`
	Type     string   `json:"type"`
	Url      string   `json:"url"`
	Children []TabBar `json:"children"`
}
type RegisterIdeData struct {
	ModelType string   `json:"model_type" form:"model_type"`
	TabBars   []TabBar `json:"tab_bars" form:"model_type"`
}

type RegisterIdeForm struct {
	AppId    string          `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId string          `json:"sub_app_id" form:"sub_app_id"`
	ModuleId string          `json:"module_id" form:"module_id"`
	Data     RegisterIdeData `json:"data" form:"data" validate:"required" message:"data required"`
}

type RegisterPluginData struct {
	Name             string `json:"name"`
	Title            string `json:"title"`
	Icon             string `json:"icon"`
	Type             string `json:"type"`
	Url              string `json:"url"`
	AdapterModelType string `json:"adapter_model_type"`
	CfgPage          any    `json:"cfg_page"`
}
type RegisterPluginForm struct {
	AppId    string             `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId string             `json:"sub_app_id" form:"sub_app_id"`
	ModuleId string             `json:"module_id" form:"module_id"`
	Data     RegisterPluginData `json:"data" form:"data" validate:"required" message:"data required"`
}

type ModelChildren struct {
	Id        string `json:"id"`
	Name      string `json:"name"`
	Title     string `json:"title"`
	Type      string `json:"type"`
	TypeTitle string `json:"type_title"`
	DetailUrl string `json:"detail_url"`
}
type AddModelViewForm struct {
	AppId       string        `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId    string        `json:"sub_app_id" form:"sub_app_id"`
	ModuleId    string        `json:"module_id" form:"module_id"`
	ModelId     string        `json:"model_id" form:"model_id" validate:"required" message:"model_id required"`
	DataModelId string        `json:"data_model_id" form:"data_model_id" validate:"required" message:"data_model_id required"`
	Data        ModelChildren `json:"data" form:"data" validate:"required" message:"data required"`
}
type UpdateModelViewForm struct {
	AppId       string `json:"app_id" validate:"required" message:"app_id required"`
	SubAppId    string `json:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId    string `json:"module_id" form:"module_id"`
	ModelId     string `json:"model_id" validate:"required" message:"model_id required"`
	DataModelId string `json:"data_model_id" validate:"required" message:"data_model_id required"`
	ChildrenId  string `json:"children_id" validate:"required" message:"children_id required"`
	Data        ModelChildren
}

type DeleteModelViewForm struct {
	AppId       string `json:"app_id" validate:"required" message:"app_id required"`
	SubAppId    string `json:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId    string `json:"module_id" form:"module_id"`
	ModelId     string `json:"model_id" validate:"required" message:"model_id required"`
	DataModelId string `json:"data_model_id" validate:"required" message:"data_model_id required"`
	ChildrenId  string `json:"children_id" validate:"required" message:"children_id required"`
}

type AddTabBarForm struct {
	AppId       string `json:"app_id" validate:"required" message:"app_id required"`
	SubAppId    string `json:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId    string `json:"module_id" form:"module_id"`
	ModelId     string `json:"model_id" validate:"required" message:"model_id required"`
	DataModelId string `json:"data_model_id" validate:"required" message:"data_model_id required"`
	Data        TabBar `json:"data" validate:"required" message:"data required"`
}
type UpdateTabBarForm struct {
	AppId     string `json:"app_id" validate:"required" message:"app_id required"`
	SubAppId  string `json:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId  string `json:"module_id" form:"module_id"`
	ModelType string `json:"model_type" validate:"required" message:"model_type required"`
	TabName   string `json:"tab_name" validate:"required" message:"tab_name required"`
	Data      TabBar `json:"data" validate:"required" message:"data required"`
}
type DeleteTabBarForm struct {
	AppId     string `json:"app_id" validate:"required" message:"app_id required"`
	SubAppId  string `json:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId  string `json:"module_id" form:"module_id"`
	ModelType string `json:"model_type" validate:"required" message:"model_type required"`
	TabName   string `json:"tab_name" validate:"required" message:"tab_name required"`
}

type AddTabChildForm struct {
	AppId     string `json:"app_id" validate:"required" message:"app_id required"`
	SubAppId  string `json:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId  string `json:"module_id" form:"module_id"`
	ModelType string `json:"model_type" validate:"required" message:"model_type required"`
	TabName   string `json:"tab_name" validate:"required" message:"tab_name required"`
	Data      TabBar `json:"data" validate:"required" message:"data required"`
}

type UpdateTabChildForm struct {
	AppId        string `json:"app_id" validate:"required" message:"app_id required"`
	SubAppId     string `json:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId     string `json:"module_id" form:"module_id"`
	ModelType    string `json:"model_type" validate:"required" message:"model_type required"`
	TabName      string `json:"tab_name" validate:"required" message:"tab_name required"`
	ChildTabName string `json:"child_tab_name" validate:"required" message:"child_tab_name required"`
	Data         TabBar `json:"data"`
}

type DeleteTabChildForm struct {
	AppId        string `json:"app_id" validate:"required" message:"app_id required"`
	SubAppId     string `json:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId     string `json:"module_id" form:"module_id"`
	ModelType    string `json:"model_type" validate:"required" message:"model_type required"`
	TabName      string `json:"tab_name" validate:"required" message:"tab_name required"`
	ChildTabName string `json:"child_tab_name" validate:"required" message:"child_tab_name required"`
}

type UnregisterModelServiceForm struct {
	AppId       string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId    string `json:"sub_app_id" form:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId    string `json:"module_id" form:"module_id"`
	ServiceName string `json:"service_name" form:"service_name" validate:"required" message:"service_name required"`
}

type UnRegisterMenuForm struct {
	AppId    string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId string `json:"sub_app_id" form:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId string `json:"module_id" form:"module_id"`
	MenuName string `json:"menu_name" form:"menu_name" validate:"required" message:"menu_name required"`
}

type UnregisterPluginForm struct {
	AppId    string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId string `json:"sub_app_id" form:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId string `json:"module_id" form:"module_id"`
	Name     string `json:"name" form:"name" validate:"required" message:"name required"`
}
type DeleteModelForm struct {
	AppId            string   `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	SubAppId         string   `json:"sub_app_id" form:"sub_app_id" validate:"required" message:"sub_app_id required"`
	ModuleId         string   `json:"module_id" form:"module_id"`
	ModelInstanceIds []string `json:"model_instance_ids" form:"model_instance_ids" validate:"required" message:"model_instance_ids required"`
	DeleteForever    bool     `json:"delete_forever" form:"delete_forever"` // 是否永久删除
}

type RegisterServiceIDEForm struct {
	AppId    string                     `json:"app_id"`
	SubAppId string                     `json:"sub_app_id"`
	ModuleId string                     `json:"module_id"`
	Data     RegisterServiceIDEFormData `json:"data"`
}

type RegisterServiceIDEFormData struct {
	ServiceID string `json:"id"`
	Name      string `json:"name"`
	Title     string `json:"title"`
	Icon      string `json:"icon"`
	Type      string `json:"type"`
	Url       string `json:"url"`
}

type UnregisterServiceIDEForm struct {
	AppId     string `json:"app_id"`
	SubAppId  string `json:"sub_app_id"`
	ModuleId  string `json:"module_id" form:"module_id"`
	ServiceID string `json:"id"`
}

// app 下的子应用列表
type SubAppListForm struct {
	AppId string `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
}

type SubAppData struct {
	Id         string `json:"id" form:"id" validate:"required" message:"app_id required"`
	Title      string `json:"title" form:"title" validate:"required" message:"title required"`
	Icon       string `json:"icon" form:"icon" `
	ClassId    string `json:"class_id" form:"class_id"`
	Desc       string `json:"desc" form:"desc"`
	CreateTime string `json:"create_time" form:"create_time"`
	UpdateTime string `json:"update_time" form:"update_time"`
	Type       string `json:"type" form:"type"` // 应用类型 项目应用还是单独应用
}

type SaveSubAppForm struct {
	AppId string     `json:"app_id" form:"app_id" validate:"required" message:"app_id required"`
	Data  SubAppData `json:"data" form:"data" validate:"required" message:"data required"`
}
