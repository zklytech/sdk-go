package ide

import (
	"github.com/gin-gonic/gin"
	"net/url"
)

type SuAppListRes struct {
	List  []SubAppData `json:"list"`
	Total int          `json:"total"`
}

func GetSubAppList(ctx *gin.Context, form SubAppListForm) (res SuAppListRes, err error) {
	params := url.Values{}
	params.Add("app_id", form.AppId)
	res, err = BaseGet[SuAppListRes](ctx, "/api/v1/app/getSubAppList", params, map[string]string{})
	return
}

func SaveSubApp(ctx *gin.Context, form SaveSubAppForm) (err error) {
	err = baseWUDRequest(ctx, "/api/v1/app/saveSubApp", form)
	return
}
