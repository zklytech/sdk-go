package ide

import (
	"encoding/json"
	"errors"
	"gitee.com/zklytech/sdk-go/common"
	"github.com/gin-gonic/gin"
	"net/http"
	"net/url"
	"strconv"
)

func baseWUDRequest(ctx *gin.Context, url string, form interface{}) (err error) {
	client := common.NewHttpClient()
	body, err := json.Marshal(form)
	if err != nil {
		err = errors.New("marshal err " + err.Error())
		return
	}
	res, statusCode, err := client.PostJson(ctx, baseUrl+url, body, map[string]string{})
	if err != nil {
		err = errors.New("request err " + err.Error())
		return
	}
	if statusCode != 200 {
		err = errors.New("request fail httpcode:" + strconv.Itoa(statusCode) + " " + string(res))
		return
	}
	var resData ResCommonData[any]
	err = json.Unmarshal(res, &resData)
	if err != nil {
		err = errors.New("unmarshal err " + err.Error())
		return
	}
	if resData.Errcode != 0 {
		err = errors.New("ide service err " + resData.Msg)
	}
	return
}

func BaseGet[T any](ctx *gin.Context, api string, params url.Values, header map[string]string) (data T, err error) {
	client := common.NewHttpClient()
	res, httpCode, err := client.GetWithHeader(ctx, baseUrl+api, params, header)
	if err != nil {
		err = errors.New("request fail: " + err.Error())
		return
	}
	if httpCode != http.StatusOK {
		err = errors.New("request fail: " + strconv.Itoa(httpCode))
		return
	}
	var commonRes ResCommonData[T]

	err = json.Unmarshal(res, &commonRes)
	if err != nil {
		err = errors.New("unmarshal fail: " + err.Error())
		return
	}
	if commonRes.Errcode != 0 {
		err = errors.New("service err " + commonRes.Msg)
		return
	}
	return commonRes.Data, nil

}
