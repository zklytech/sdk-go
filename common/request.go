package common

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"net/url"

	"github.com/gin-gonic/gin"
)

type HttpClient struct {
	client *http.Client
}

// Http客户端
func NewHttpClient() *HttpClient {
	return &HttpClient{
		client: &http.Client{},
	}
}

// 有代理的Http客户端
func NewHttpClientWithProxy(proxyUrl *url.URL) *HttpClient {
	return &HttpClient{
		client: &http.Client{
			Transport: &http.Transport{
				Proxy: http.ProxyURL(proxyUrl),
			},
		},
	}
}

// Http GET Method
// 使用x-www-urlencoded编码
func (c *HttpClient) Get(ctx *gin.Context, url string, params url.Values) ([]byte, int, error) {
	return c.sendWithoutBody(ctx, "GET", url, params)
}
func (c *HttpClient) GetWithHeader(ctx *gin.Context, url string, params url.Values, headers map[string]string) ([]byte, int, error) {
	return c.sendWithoutBodyWithHeader(ctx, "GET", url, params, headers)
}

// Http DELETE Method
// 使用x-www-urlencoded编码
func (c *HttpClient) Delete(ctx *gin.Context, url string, params url.Values) ([]byte, int, error) {
	return c.sendWithoutBody(ctx, "DELETE", url, params)
}
func (c *HttpClient) DeleteWithHeader(ctx *gin.Context, url string, params url.Values, header map[string]string) ([]byte, int, error) {
	return c.sendWithoutBodyWithHeader(ctx, "DELETE", url, params, header)
}

// Http POST Method
func (c *HttpClient) Post(ctx *gin.Context, url string, data []byte, headers map[string]string) ([]byte, int, error) {
	return c.sendWithBody(ctx, "POST", url, data, headers)
}

// Http PUT Method
func (c *HttpClient) Put(ctx *gin.Context, url string, data []byte, headers map[string]string) ([]byte, int, error) {
	return c.sendWithBody(ctx, "PUT", url, data, headers)
}

// Http POST Method
// 发送Json
func (c *HttpClient) PostJson(ctx *gin.Context, url string, data []byte, headers map[string]string) ([]byte, int, error) {
	if headers == nil {
		headers = map[string]string{}
	}
	headers["Content-Type"] = "application/json"
	return c.Post(ctx, url, data, headers)
}

func (c *HttpClient) PutJson(ctx *gin.Context, url string, data []byte, headers map[string]string) ([]byte, int, error) {
	if headers == nil {
		headers = map[string]string{}
	}
	headers["Content-Type"] = "application/json"
	return c.Put(ctx, url, data, headers)
}

// Http POST Method
// 发送FormData
func (c *HttpClient) PostForm(ctx *gin.Context, url string, data map[string]string, headers map[string]string) ([]byte, int, error) {
	buff, form := buildForm(data)
	form.Close()
	if headers == nil {
		headers = map[string]string{}
	}
	headers["Content-Type"] = form.FormDataContentType()

	return c.Post(ctx, url, buff.Bytes(), headers)
}

// Http PUT Method
// 发送FormData
func (c *HttpClient) PutForm(ctx *gin.Context, url string, data map[string]string, headers map[string]string) ([]byte, int, error) {
	buff, form := buildForm(data)
	form.Close()
	if headers == nil {
		headers = map[string]string{}
	}
	headers["Content-Type"] = form.FormDataContentType()

	return c.Put(ctx, url, buff.Bytes(), headers)
}

// Http POST Method
// 发送FormData
func (c *HttpClient) PostFile(ctx *gin.Context, url string, data map[string]string, fileData map[string]*multipart.FileHeader, headers map[string]string) ([]byte, int, error) {
	buff, form, err := buildFileForm(data, fileData)
	if err != nil {
		return nil, 0, err
	}
	form.Close()
	if headers == nil {
		headers = map[string]string{}
	}
	headers["Content-Type"] = form.FormDataContentType()
	return c.Post(ctx, url, buff.Bytes(), headers)
}

// Http PUT Method
// 发送FormData
func (c *HttpClient) PutFile(ctx *gin.Context, url string, data map[string]string, fileData map[string]*multipart.FileHeader, headers map[string]string) ([]byte, int, error) {
	buff, form, err := buildFileForm(data, fileData)
	if err != nil {
		return nil, 0, err
	}
	form.Close()
	if headers == nil {
		headers = map[string]string{}
	}
	headers["Content-Type"] = form.FormDataContentType()
	return c.Put(ctx, url, buff.Bytes(), headers)
}

func (c *HttpClient) DownloadFile(ctx *gin.Context, requestUrl string, method string, data map[string]string, headers map[string]string) ([]byte, int, error) {
	client := &http.Client{}
	body, err := json.Marshal(data)
	if err != nil {
		return nil, 0, err
	}
	queryParams := url.Values{}
	for k, v := range data {
		queryParams.Add(k, v)
	}
	req, err := http.NewRequest(method, requestUrl+"?"+queryParams.Encode(), bytes.NewReader(body))
	if err != nil {
		return nil, 0, err
	}
	req.Header.Set("Content-Type", "application/json")
	for k, v := range headers {
		req.Header.Set(k, v)
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, 0, err
	}
	if resp.StatusCode != 200 {
		return nil, resp.StatusCode, fmt.Errorf("request err http status code: %d", resp.StatusCode)
	}
	defer resp.Body.Close()
	ContentDisposition := resp.Header.Get("Content-Disposition")
	contentType := resp.Header.Get("Content-Type")
	ctx.Header("Content-Disposition", ContentDisposition)
	ctx.Header("Content-Type", contentType)
	r, e := io.ReadAll(resp.Body)
	if e != nil {
		e = errors.New("read err " + e.Error())
		return nil, 0, e
	}
	return r, resp.StatusCode, nil

}

// form no close
func buildForm(data map[string]string) (buff *bytes.Buffer, form *multipart.Writer) {
	buff = new(bytes.Buffer)
	form = multipart.NewWriter(buff)
	for k, v := range data {
		form.WriteField(k, v)
	}
	return
}

// form no close
func buildFileForm(data map[string]string, fileData map[string]*multipart.FileHeader) (buff *bytes.Buffer, form *multipart.Writer, err error) {
	buff = new(bytes.Buffer)
	form = multipart.NewWriter(buff)
	for k, v := range data {
		form.WriteField(k, v)
	}

	for fk, fv := range fileData {
		fh, e := fv.Open()
		if e != nil {
			err = e
			fmt.Println("read file error:" + err.Error())
			return
		}

		formIo, e := form.CreateFormFile(fk, fv.Filename)
		if e != nil {
			err = e
			fmt.Println("creats stream:" + err.Error())
			return
		}
		io.Copy(formIo, fh)
	}
	return
}

func (c *HttpClient) sendWithoutBody(ctx *gin.Context, method string, url string, params url.Values) ([]byte, int, error) {

	fullURL := url + "?" + params.Encode()
	req, err := http.NewRequest(method, fullURL, nil)
	if err != nil {
		return nil, 0, nil
	}

	req.Header.Set("Event-Id", ctx.GetHeader("Event-Id"))
	req.Header.Set("Trace-Id", ctx.GetHeader("Trace-Id"))
	req.Header.Set("Authorization", ctx.GetHeader("Authorization"))
	req.Header.Set("Logintoken", ctx.GetHeader("Logintoken"))
	req.Header.Set("App-Id", ctx.GetHeader("App-Id"))
	req.Header.Set("Service-Name", ctx.GetHeader("Service-Name"))
	req.Header.Set("Sub-App-Id", ctx.GetHeader("Sub-App-Id"))
	req.Header.Set("Module-Id", ctx.GetHeader("Module-Id"))
	resp, err := c.client.Do(req)

	if err != nil {
		return nil, 0, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, 0, err
	}

	return body, resp.StatusCode, nil
}

func (c *HttpClient) sendWithoutBodyWithHeader(ctx *gin.Context, method string, url string, params url.Values, headers map[string]string) ([]byte, int, error) {

	fullURL := url + "?" + params.Encode()
	req, err := http.NewRequest(method, fullURL, nil)
	if err != nil {
		return nil, 0, nil
	}

	req.Header.Set("Event-Id", ctx.GetHeader("Event-Id"))
	req.Header.Set("Trace-Id", ctx.GetHeader("Trace-Id"))
	req.Header.Set("Authorization", ctx.GetHeader("Authorization"))
	req.Header.Set("Logintoken", ctx.GetHeader("Logintoken"))
	req.Header.Set("App-Id", ctx.GetHeader("App-Id"))
	req.Header.Set("Service-Name", ctx.GetHeader("Service-Name"))
	for key, value := range headers {
		req.Header.Set(key, value)
	}
	resp, err := c.client.Do(req)

	if err != nil {
		return nil, 0, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, 0, err
	}

	return body, resp.StatusCode, nil
}

func (c *HttpClient) sendWithBody(ctx *gin.Context, method string, url string, data []byte, headers map[string]string) ([]byte, int, error) {
	req, err := http.NewRequest(method, url, bytes.NewBuffer(data))
	if err != nil {
		return nil, 0, err
	}

	req.Header.Set("Event-Id", ctx.GetHeader("Event-Id"))
	req.Header.Set("Trace-Id", ctx.GetHeader("Trace-Id"))
	req.Header.Set("Authorization", ctx.GetHeader("Authorization"))
	req.Header.Set("Logintoken", ctx.GetHeader("Logintoken"))
	req.Header.Set("App-Id", ctx.GetHeader("App-Id"))
	req.Header.Set("Service-Name", ctx.GetHeader("Service-Name"))

	for key, value := range headers {
		req.Header.Set(key, value)
	}
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, 0, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, resp.StatusCode, err
	}

	return body, resp.StatusCode, nil
}

// 创建一个模拟的Gin上下文的辅助函数
func CreateGinContext(header map[string]string) *gin.Context {
	// 创建一个Gin引擎
	// 创建一个模拟的HTTP请求
	re := httptest.NewRequest("GET", "/example", nil)
	// 创建一个模拟的Gin上下文
	writer := httptest.NewRecorder()
	context, _ := gin.CreateTestContext(writer)
	for k, v := range header {
		re.Header.Set(k, v)
	}
	context.Request = re
	return context
}
