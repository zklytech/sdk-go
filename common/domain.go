package common

type Result[T any] struct {
	ErrCode int    `json:"errcode"`
	Msg     string `json:"msg"`
	Data    T      `json:"data"`
}
