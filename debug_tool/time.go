package debug_tool

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"time"
)

// Timer 结构体用于存储起始时间和标记
type Timer struct {
	start    time.Time
	lastTime time.Time
	label    string
	id       string
}

var logChannel = make(chan string, 500)

func init() {
	// 启动一个 goroutine 来处理日志输出
	go func() {
		for msg := range logChannel {
			fmt.Println(msg)
		}
	}()
}

// generateID 生成一个随机字符串
func generateID() string {
	b := make([]byte, 8)
	_, err := rand.Read(b)
	if err != nil {
		return "00000000"
	}
	return hex.EncodeToString(b)
}

// StartTimer 创建一个新的 Timer 并记录当前时间
func StartTimer(label string) *Timer {
	id := generateID()
	logChannel <- fmt.Sprintf("debug label %s, id %s", label, id)
	now := time.Now()
	return &Timer{
		start:    now,
		lastTime: now,
		label:    label,
		id:       id,
	}
}

// Add 记录当前时间并计算上一个标记到现在的耗时
func (t *Timer) Add(label string) {
	now := time.Now()
	elapsed := now.Sub(t.lastTime)
	logChannel <- fmt.Sprintf("debug %s -> %s: %v, id %s", t.label, label, elapsed, t.id)
	t.lastTime = now
	t.label = label
}

// EndTimer 计算并发送从开始到结束的总耗时到 channel
func (t *Timer) EndTimer() {
	elapsed := time.Since(t.start)
	logChannel <- fmt.Sprintf("debug %s: %v, id %s", t.label, elapsed, t.id)
}
